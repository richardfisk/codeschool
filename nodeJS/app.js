var http = require('http');
var fs = require('fs');

http.createServer(function(request, response) {
  var responseHeaders = {
  	'Content-Type': 'text/html'
  };
  response.writeHead(200, 'OK', responseHeaders);

  var readFile = fs.readFile('index.html', function(err, contents) {
    response.write(contents);
    response.end();
  });
}).listen(8085);