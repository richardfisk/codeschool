var fs = require('fs');
var file = fs.createReadStream('index.html');

file.pipe(process.stdout, { end: false });

file.on("end", function() {
  console.log('--File Complete--');
});

var fs = require('fs');

var file = fs.createReadStream("icon.png");
var newFile = fs.createWriteStream("icon-new.png");

file.on('data', function(chunk) {
  var isBufferGood = newFile.write(chunk);
  if(!isBufferGood) {
    file.pause();
  }
});

newFile.on("drain", function (){
  file.resume();
});

file.on('end', function() {
  newFile.end();
});
