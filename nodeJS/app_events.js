var http = require('http');
var fs = require('fs');
var events = require('events');
var EventEmitter = events.EventEmitter;

var server = http.createServer();

var logger = new EventEmitter();
logger.on("blah", function(blahMessage) {
	console.log(blahMessage);
});


server.on("request", function(request, response) {
  var responseHeaders = {
  	'Content-Type': 'text/html'
  };
  response.writeHead(200, 'OK', responseHeaders);

  var readFile = fs.readFile('index.html', function(err, contents) {
    response.write(contents);
    logger.emit("blah", "I am blah");
    response.end();
  });
})

server.on("close", function () {
	console.log("server was closed");
});

server.listen(8085);