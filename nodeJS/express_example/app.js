var express = require('express');
var ejs = require('ejs');
var app = express.createServer();

var quotes = {
  'einstein': 'Life is like riding a bicycle. To keep your balance you must keep moving',
  'berners-lee': 'The Web does not just connect machines, it connects people',
  'crockford': 'The good thing about reinventing the wheel is that you can get a round one',
  'hofstadter': 'Which statement seems more true: (1) I have a brain. (2) I am a brain.'
};

app.configure(function(){
    
    app.set('views',__dirname+'/views');

    app.use(express.favicon(__dirname + '/public/favicon.ico'));
});

app.get('/quotes/:name', function(request, response) {
  var quote = quotes[request.params.name];
  var model = {name:request.params.name, body:quote};
  console.log(model);
  response.render('quote.ejs', model);
});

app.listen(8086);
