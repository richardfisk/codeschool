var JSBridge_objCount = 0;

// Keeps the objects that should be communicated to the Objective-C code.
var JSBridge_objArray = new Array();

/*
 Receives as input an image object and returns its data
 encoded in a base64 string.
 
 This piece of code was based on Matthew Crumley's post
 at http://stackoverflow.com/questions/934012/get-image-data-in-javascript.
 */
function getBase64Image(img) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
	
	var newImg = new Image();
	newImg.src = img.src;
    canvas.width = newImg.width;
    canvas.height = newImg.height;
	
    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(newImg, 0, 0);
	
    // Get the data-URL formatted image
    var dataURL = canvas.toDataURL("image/png");
	
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}


/*
	Builds an empty instance of a JSBridge object. 
 */
function JSBridgeObj()
{
	this.objectJson = "";
	this.addObject = JSBridgeObj_AddObject;
	this.sendBridgeObject = JSBridgeObj_SendObject;
}

/*
	This mnethod receives as input a javascript object, and returns 
	a string with the json representation for the object. The return string is similar to:
 
	"<objectId>" : { "value": "<object_value>", "type": "<object_type>" }
 */
function JSBridgeObj_AddObjectAuxiliar(id, obj)
{
	var result = "";
	if(typeof(obj) != "undefined")
	{
		if(isObjAnArray(obj))
		{
			var objStr;
			var length = obj.length;
			
			objStr = "{";
			for(i = 0; i < length; i++)
			{
				if(objStr != "{")
				{
					objStr += ", ";
				}
				objStr += JSBridgeObj_AddObjectAuxiliar(("obj" + i), obj[i]);
			}
			
			objStr += "}";
			
			result = "\"" + id + "\": { \"value\":" + objStr + ", \"type\": \"array\"}";
		}
		else
		{
			var objStr;
			var objType;
			if(typeof(obj) == "object" && obj.nodeName == "IMG")
			{
				objStr = getBase64Image(obj);
				objType = "image";
			} else
			{
				objStr = obj;
				objType = typeof(obj);
			}
			
			result = "\"" + id + "\": { \"value\":" + "\"" + objStr + "\", \"type\": \"" + objType + "\"}";
		}
	}
	return result;
}

/*
	The addObject method implementation for the JSBridge object.
 */
function JSBridgeObj_AddObject(id, obj)
{
	var result = JSBridgeObj_AddObjectAuxiliar(id, obj);
	if(result != "")
	{
		if(this.objectJson != "")
		{
			this.objectJson += ", ";
		}
		this.objectJson += result;
	}
}


/*
	This method sends the object to the Objective-C code. Basically, 
	it tries to load a special URL, which passes the object id.
 */
function JSBridgeObj_SendObject()
{
	JSBridge_objArray[JSBridge_objCount] = this.objectJson;
	
	window.location.href= "JSBridge://ReadNotificationWithId=" + JSBridge_objCount;
	
	JSBridge_objCount++;
}

/*
	This method is invoked by the Objective-C code. It retrieves the json string representation
	of a JSBridge object given its id.
 */
function JSBridge_getJsonStringForObjectWithId(objId)
{
	var jsonStr = JSBridge_objArray[objId];
	
	JSBridge_objArray[objId] = null;
	
	return "{" + jsonStr + "}";
}


/*
	Checks if an object is an array.
 
	This piece of code was based on a code rertrieved from
	http://www.planetpdf.com/developer/article.asp?ContentID=testing_for_object_types_in_ja.
 */
function isObjAnArray(obj) {
	
	if (typeof(obj) == 'object') {  
		var criterion = obj.constructor.toString().match(/array/i); 
 		return (criterion != null);  
	}
	return false;
}

function doSomething() {
	alert("");
    var obj = new JSBridgeObj();
    obj.addObject("task", "process_mask");
    
    obj.sendBridgeObject();

}
