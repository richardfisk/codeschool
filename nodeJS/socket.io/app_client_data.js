var fs = require('fs');
var express = require('express');
var app = express.createServer();
var socket = require('socket.io');
var io = socket.listen(app);

var onQuestion = function(client, question) {
    client.get('question_asked', function(question_asked){
        if (question_asked != true) {
          client.broadcast.emit('question', question);
          client.set('question_asked', true);
        }    
    });
};

io.sockets.on('connection', function(client) {
  console.log("Client connected...");

  // listen here
  client.on('question', function(question){
    onQuestion(client, question);
  })
});

//Below doesn't work
io.sockets.on('question', function(question){
	console.log(question);
});
//-------------------
app.get('/client.html', function(request, response) {
	fs.createReadStream('client.html').pipe(response);	
	console.log(request.route);
});

app.listen(8999);
