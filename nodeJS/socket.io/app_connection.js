var fs = require('fs');

var express = require('express');
var app = express.createServer();

var socket = require('socket.io');

var io = socket.listen(app);

io.sockets.on('connection', function(client) {
  console.log('Client connected...');
});

app.get('/client.html', function(request, response) {
	fs.createReadStream('client.html').pipe(response);	
	console.log(request.route);
});

app.listen(8999);