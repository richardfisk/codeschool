#import "CSCourseVC.h"

@implementation CSCourseVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.courseIntroTextView.text = @"Quickly get up to speed on the core changes of iOS 7. Make your app stand out from the crowd by mastering new APIs and take advantage of great Xcode updates.";
}

// TODO: Implement the updateViewConstraints method and set the NSLayoutConstraint property's constant to be equal to the calculated height of the text view.
- (void)updateViewConstraints
{
    [super updateViewConstraints];
    CGFloat textHeight = [self textViewHeight:self.courseIntroTextView];
    self.layoutConstraint.constant = textHeight;
}


// We've taken the text view height calculation code from Level 2 and put it in a method for you.  Now, when you need to calculate a text view's height, you can just call this method and pass in the text view and it will return a CGFloat number with the value of the height.
- (CGFloat)textViewHeight:(UITextView *)textView
{
    [textView.layoutManager ensureLayoutForTextContainer:textView.textContainer];
    CGRect usedRect = [textView.layoutManager usedRectForTextContainer:textView.textContainer];
    return ceilf(usedRect.size.height +
                 textView.textContainerInset.top +
                 textView.textContainerInset.bottom);
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
