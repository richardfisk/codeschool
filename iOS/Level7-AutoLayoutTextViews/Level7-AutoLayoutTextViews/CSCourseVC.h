#import <UIKit/UIKit.h>

@interface CSCourseVC : UIViewController

// TODO: Connect this outlet in Interface Builder (the hollow circle in the left sidebar of line 6 means that it's not connected
@property (weak, nonatomic) IBOutlet UITextView *courseIntroTextView;

// TODO: Implement a weak NSLayoutConstraint property, designate it as an IBOutlet, and connect the outlet to the text view's height constraint in Interface Builder
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraint;


@end
