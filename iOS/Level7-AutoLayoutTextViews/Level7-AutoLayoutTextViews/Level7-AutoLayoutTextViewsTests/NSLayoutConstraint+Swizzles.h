#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (Swizzles)

- (void)custom_setConstant:(CGFloat)constant;

@end
