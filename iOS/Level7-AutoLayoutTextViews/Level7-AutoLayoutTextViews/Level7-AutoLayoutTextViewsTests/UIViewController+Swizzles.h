#import <UIKit/UIKit.h>

@interface UIViewController (Swizzles)

- (void)custom_updateViewConstraints;

@end
