#import "NSLayoutConstraint+Swizzles.h"
#import "CSSwizzler.h"
#import "CSAppDelegate+TestAdditions.h"

@implementation NSLayoutConstraint (Swizzles)

+ (void) load
{
    [CSSwizzler swizzleClassOfInstance:[[NSLayoutConstraint alloc] init]
                         replaceMethod:@selector(setConstant:)
                            withMethod:@selector(custom_setConstant:)];
}

- (void)custom_setConstant:(CGFloat)constant
{
    CSAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.constantValue = @(constant);
    [self custom_setConstant:constant];
}

@end
