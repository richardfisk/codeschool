#import "UIViewController+Swizzles.h"
#import "CSSwizzler.h"
#import "CSAppDelegate+TestAdditions.h"

@implementation UIViewController (Swizzles)

+ (void) load
{
    [CSSwizzler swizzleClass:[UIViewController class]
               replaceMethod:@selector(updateViewConstraints)
                  withMethod:@selector(custom_updateViewConstraints)];
}

- (void)custom_updateViewConstraints
{
    CSAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.calledUpdateViewConstraints = @YES;
    [self custom_updateViewConstraints];
}

@end
