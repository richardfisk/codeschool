#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "CSAppDelegate.h"
#import "CSCourseVC.h"
#import "CSPropertyDefinition.h"
#import "CSAppDelegate+TestAdditions.h"

#define kDoubleEpsilon (0.5)

static BOOL doubleCloseTo(double a, double b)
{
    if(fabs(a-b) < kDoubleEpsilon) {
        return YES;
    } else {
        return NO;
    }
}

@interface NSIBPrototypingLayoutConstraint : NSLayoutConstraint
@end

@interface NSAutoresizingMaskLayoutConstraint : NSLayoutConstraint
@end

@interface NSContentSizeLayoutConstraint : NSLayoutConstraint
@end

@interface Level7_AutoLayoutTextViewsTests : CSRecordingTestCase

@end

#pragma mark - Enum to NSString helper methods
NSString *NSStringFromNSLayoutAttribute(NSLayoutAttribute attribute)
{
    switch (attribute) {
        case 1:
            return @"NSLayoutAttributeLeft";
            break;
        case 2:
            return @"NSLayoutAttributeRight";
            break;
        case 3:
            return @"NSLayoutAttributeTop";
            break;
        case 4:
            return @"NSLayoutAttributeBottom";
            break;
        case 5:
            return @"NSLayoutAttributeLeading";
            break;
        case 6:
            return @"NSLayoutAttributeTrailing";
            break;
        case 7:
            return @"NSLayoutAttributeWidth";
            break;
        case 8:
            return @"NSLayoutAttributeHeight";
            break;
        case 9:
            return @"NSLayoutAttributeCenterX";
            break;
        case 10:
            return @"NSLayoutAttributeCenterY";
            break;
        case 11:
            return @"NSLayoutAttributeBaseline";
            break;
        default:
            return @"NSLayoutAttributeNotAnAttribute";
            break;
    }
}

NSString *NSStringFromNSLayoutRelation(NSLayoutRelation relation)
{
    switch (relation) {
        case -1:
            return @"NSLayoutRelationLessThanOrEqual";
            break;
        case 0:
            return @"NSLayoutRelationEqual";
            break;
        case 1:
            return @"NSLayoutRelationGreaterThanOrEqual";
            break;
        default:
            return @"OOPS";
            break;
    }
}

@implementation Level7_AutoLayoutTextViewsTests {
    CSAppDelegate *_appDel;
    CSCourseVC *_csCourseVC;
    UIView *_mainView;
    BOOL _mainViewWantsAutolayout;
}

- (void)setUp
{
    [super setUp];
    _appDel = [[UIApplication sharedApplication] delegate];
    _mainView = (UIView *)_appDel.window.subviews[0];
    _mainViewWantsAutolayout = (NSInteger)[_mainView performSelector:@selector(_wantsAutolayout)];
    _csCourseVC = (CSCourseVC *)_appDel.window.rootViewController;
}

- (void)tearDown
{
    _mainViewWantsAutolayout = NO;
    [super tearDown];
}

- (void)testTextViewOutletConnected
{
    CSPropertyDefinition *property = [self textViewProperty];
    if(property) {
        [_csCourseVC loadView];
        XCTAssertNotNil(_csCourseVC.courseIntroTextView, @"Did not connect the courseIntroTextView outlet in Interface Builder");
    } else {
        XCTFail(@"There was a UITextView property when you downloaded this project, but it looks like it's gone missing.  Try to re-download this project so you can start fresh.");
    }
}

- (void)testHeightConstraintExistsAndConnected
{
    CSPropertyDefinition *property = [self textViewHeightConstraintProperty];
    if(property) {
        [_csCourseVC loadView];
        XCTAssert([property isWeak],@"You created the NSLayoutConstraint property, but it should be typed weak, not strong");
        NSLayoutConstraint *heightConstraint = [_csCourseVC valueForKey:property.name];
        XCTAssertNotNil(heightConstraint, @"Did not connect the NSLayoutConstraint text view height outlet in Interface Builder");
    } else {
        XCTFail(@"Did not create an NSLayoutConstraint property to connect to the UITextView's height constraint");
    }
}

- (void)testUpdateViewConstraintsImplemented
{
    _appDel.constantValue = @0;
    [_csCourseVC updateViewConstraints];
    XCTAssert([_appDel.constantValue integerValue] > 0, @"Did not set the text view height constraints' constant in the updateViewConstraints method in CSCourseVC.m");
}

- (void)testTextViewConstraints
{
    NSString *restorationIdentiferToTest = @"text-view";
    NSArray *attributesToTest = @[@(NSLayoutAttributeLeading),@(NSLayoutAttributeTrailing),@(NSLayoutAttributeTop),@(NSLayoutAttributeBottom)];
    
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:attributesToTest forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id viewConstraintToTest = obj[0];
                    if([viewConstraintToTest firstAttribute] == [attributesToTest[0] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:20.0 forAttribute:[attributesToTest[0] integerValue]], @"There should be a leading space to container constraint between text-view and scroll-view with a constant of 20.0");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[1] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:20.0 forAttribute:[attributesToTest[1] integerValue]], @"There should be a trailing space to container constraint between text-view and scroll-view with a constant of 20.0");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[2] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:20.0 forAttribute:[attributesToTest[2] integerValue]], @"There should be a vertical spacing constraint between text-view and badge7 with a constant of 25.0");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[3] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:10.0 forAttribute:[attributesToTest[3] integerValue]], @"There should be a bottom space to container constraint between text-view and scroll-view with a constant of 10.0");
                    }
                }];
            }
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

#pragma mark - Property Helper Methods

- (CSPropertyDefinition *)textViewProperty
{
    CSPropertyDefinition *property;
    
    property = [CSPropertyDefinition firstPropertyWithType:@"UITextView" forClass:[CSCourseVC class]];
    
    return property;
}

- (CSPropertyDefinition *)textViewHeightConstraintProperty
{
    CSPropertyDefinition *property;
    
    property = [CSPropertyDefinition firstPropertyWithType:@"NSLayoutConstraint" forClass:[CSCourseVC class]];
    
    return property;
}

#pragma mark - Auto Layout Helper Methods

- (NSArray *)firstItemConstraintsForRestorationIdentifier:(NSString *)restorationID fromLayoutEngineConstraints:(NSArray *)layoutEngineConstraints
{
    __block NSMutableArray *relevantConstraints = [[NSMutableArray alloc] init];
    
    [layoutEngineConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([[[obj firstItem] restorationIdentifier] isEqualToString:restorationID]) {
            if([obj isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            }
        }
    }];
    return [[NSArray alloc] initWithArray:relevantConstraints];
}

- (NSArray *)secondItemConstraintsForRestorationIdentifier:(NSString *)restorationID fromLayoutEngineConstraints:(NSArray *)layoutEngineConstraints
{
    __block NSMutableArray *relevantConstraints = [[NSMutableArray alloc] init];
    
    [layoutEngineConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([[[obj secondItem] restorationIdentifier] isEqualToString:restorationID]) {
            if([obj isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            }
        }
    }];
    return [[NSArray alloc] initWithArray:relevantConstraints];
}

- (BOOL)reportAmbiguousLayoutTestFailureWithSet:(NSMutableSet *)setOfViewsWithAmbiguousLayout
{
    if(setOfViewsWithAmbiguousLayout.count == 1) {
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ view has ambiguous layout because you didn't create enough constraints for it.  Make sure that the view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", [[setOfViewsWithAmbiguousLayout allObjects] objectAtIndex:0]]);
        return YES;
    } else if(setOfViewsWithAmbiguousLayout.count == 2) {
        NSMutableString *viewsWithAmbiguousLayout = [[NSMutableString alloc] init];
        [[setOfViewsWithAmbiguousLayout allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if(idx < setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"%@ ",obj]];
            } else if(idx == setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"and %@",obj]];
            }
        }];
        
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ views have ambiguous layout because you didn't create enough constraints for them.  Make sure that each view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", viewsWithAmbiguousLayout]);
        return YES;
    } else if(setOfViewsWithAmbiguousLayout.count > 2) {
        NSMutableString *viewsWithAmbiguousLayout = [[NSMutableString alloc] init];
        [[setOfViewsWithAmbiguousLayout allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if(idx < setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"%@, ",obj]];
            } else if(idx == setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"and %@",obj]];
            }
        }];
        
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ views have ambiguous layout because you didn't create enough constraints for them.  Make sure that each view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", viewsWithAmbiguousLayout]);
        return YES;
    }
    return NO;
}

- (BOOL)constraint:(NSLayoutConstraint *)constraint hasConstantEqualTo:(CGFloat)constant forAttribute:(NSLayoutAttribute)attribute
{
    return doubleCloseTo([constraint constant], constant);
}

- (BOOL)checkCorrectAttributes:(NSArray *)attributes forConstraints:(NSArray *)constraints
{
    __block NSMutableArray *existingAttributes = [[NSMutableArray alloc] init];
    
    [constraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id constraint = obj[0];
        [existingAttributes addObject:@([constraint firstAttribute])];
    }];
    
    __block NSMutableArray *requiredCopy = [[NSMutableArray alloc] initWithArray:attributes];
    
    for (id attribute in attributes) {
        for(id existing in existingAttributes) {
            if([existing integerValue] == [attribute integerValue]) {
                [requiredCopy removeObject:existing];
            }
        }
    }
    __block BOOL isCorrect = YES;
    
    [requiredCopy enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XCTFail(@"%@",[NSString stringWithFormat:@"missing %@ constraint",NSStringFromNSLayoutAttribute([obj integerValue])]);
        isCorrect = NO;
    }];
    return isCorrect;
}

@end