#import "CSAppDelegate.h"

@interface CSAppDelegate (TestAdditions)

@property (strong, nonatomic) NSNumber *calledUpdateViewConstraints;
@property (strong, nonatomic) NSNumber *constantValue;

@end
