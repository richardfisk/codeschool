#import "CSAppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char calledUpdateViewConstraintsKey;
static char constantValueKey;

@implementation CSAppDelegate (TestAdditions)

- (void) setCalledUpdateViewConstraints:(NSNumber *)calledUpdateViewConstraints
{
    objc_setAssociatedObject(self, &calledUpdateViewConstraintsKey, calledUpdateViewConstraints, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)calledUpdateViewConstraints
{
    return (NSNumber *)objc_getAssociatedObject(self, &calledUpdateViewConstraintsKey);
}

- (void)setConstantValue:(NSNumber *)constantValue
{
    objc_setAssociatedObject(self, &constantValueKey, constantValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)constantValue
{
    return (NSNumber *)objc_getAssociatedObject(self, &constantValueKey);
}

@end
