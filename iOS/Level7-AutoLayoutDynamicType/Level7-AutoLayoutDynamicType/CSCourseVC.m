#import "CSCourseVC.h"

@implementation CSCourseVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.courseIntroTextView.text = @"Quickly get up to speed on the core changes of iOS 7. Make your app stand out from the crowd by mastering new APIs and take advantage of great Xcode updates.";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(contentSizeChanged:) name:@"UIContentSizeCategoryDidChangeNotification"
                                               object:nil];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    self.textViewHeight.constant = [self textViewHeight:self.courseIntroTextView];
}

- (CGFloat)textViewHeight:(UITextView *)textView
{
    [textView.layoutManager ensureLayoutForTextContainer:textView.textContainer];
    CGRect usedRect = [textView.layoutManager usedRectForTextContainer:textView.textContainer];
    return ceilf(usedRect.size.height +
                 textView.textContainerInset.top +
                 textView.textContainerInset.bottom);
}

// TODO: Implement the willAnimateRotationToInterfaceOrientation:duration: method and tell self.view to update constraints whenever the method is called.
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.view setNeedsUpdateConstraints];
}


- (void)contentSizeChanged:(id)sender
{
    UIFontDescriptor *introTextViewFontDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
    self.courseIntroTextView.font = [UIFont fontWithDescriptor:introTextViewFontDescriptor size:0];
    
    // TODO: tell self.view to update constraints whenever this method is called.
    [self.view setNeedsUpdateConstraints];
    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
