//
//  UIView+Swizzles.h
//  Level7-AutoLayoutDynamicType
//
//  Created by Jon Friskics on 11/8/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Swizzles)

- (void)custom_setNeedsUpdateConstraints;

@end
