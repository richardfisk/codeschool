#import "CSAppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char needsUpdateConstraintsCountKey;

@implementation CSAppDelegate (TestAdditions)

- (void) setNeedsUpdateConstraintsCount:(NSNumber *)needsUpdateConstraintsCount
{
    objc_setAssociatedObject(self, &needsUpdateConstraintsCountKey, needsUpdateConstraintsCount, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)needsUpdateConstraintsCount
{
    return (UIViewController *)objc_getAssociatedObject(self, &needsUpdateConstraintsCountKey);
}

@end
