#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "CSAppDelegate.h"
#import "CSCourseVC.h"
#import "CSAppDelegate+TestAdditions.h"

@interface Level7_AutoLayoutDynamicTypeTests : CSRecordingTestCase

@end

@implementation Level7_AutoLayoutDynamicTypeTests {
    CSAppDelegate *_appDel;
    CSCourseVC *_csCourseVC;
}

- (void)setUp
{
    [super setUp];
    _appDel = [[UIApplication sharedApplication] delegate];
    _csCourseVC = (CSCourseVC *)_appDel.window.rootViewController;
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testRotationCallsUpdateConstraints
{
    [_csCourseVC loadView];
    
    NSInteger numberOfSetNeedsUpdateConstraintsBefore = [_appDel.needsUpdateConstraintsCount integerValue];
    [_csCourseVC willAnimateRotationToInterfaceOrientation:3 duration:0];
    NSInteger numberOfSetNeedsUpdateConstraintsAfter = [_appDel.needsUpdateConstraintsCount integerValue];
    NSLog(@"---- %d ---- %d",numberOfSetNeedsUpdateConstraintsBefore,numberOfSetNeedsUpdateConstraintsAfter);
    
    XCTAssert(numberOfSetNeedsUpdateConstraintsAfter > numberOfSetNeedsUpdateConstraintsBefore, @"Did not call the setNeedsUpdateConstraints method in the willAnimationToInterfaceOrientation: method");
}

- (void)testContentSizeChangedCallsUpdateConstraints
{
    [_csCourseVC loadView];
    
    NSInteger numberOfSetNeedsUpdateConstraintsBefore = [_appDel.needsUpdateConstraintsCount integerValue];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UIContentSizeCategoryDidChangeNotification" object:_csCourseVC];
    NSInteger numberOfSetNeedsUpdateConstraintsAfter = [_appDel.needsUpdateConstraintsCount integerValue];
    NSLog(@"---- %d ---- %d",numberOfSetNeedsUpdateConstraintsBefore,numberOfSetNeedsUpdateConstraintsAfter);
    
    XCTAssert(numberOfSetNeedsUpdateConstraintsAfter > numberOfSetNeedsUpdateConstraintsBefore, @"Did not call the setNeedsUpdateConstraints method in the contentSizeChanged: method");
}

@end
