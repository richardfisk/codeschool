#import "CSAppDelegate.h"

@interface CSAppDelegate (TestAdditions)

@property (strong, nonatomic) NSNumber *needsUpdateConstraintsCount;

@end
