//
//  UIView+Swizzles.m
//  Level7-AutoLayoutDynamicType
//
//  Created by Jon Friskics on 11/8/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIView+Swizzles.h"
#import "CSSwizzler.h"
#import "CSAppDelegate+TestAdditions.h"

@implementation UIView (Swizzles)

+ (void) load
{
    [CSSwizzler swizzleClass:[UIView class]
               replaceMethod:@selector(setNeedsUpdateConstraints)
                  withMethod:@selector(custom_setNeedsUpdateConstraints)];
}

- (void)custom_setNeedsUpdateConstraints
{
    CSAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.needsUpdateConstraintsCount = @([appDelegate.needsUpdateConstraintsCount integerValue] + 1);
    [self custom_setNeedsUpdateConstraints];
}

@end
