#import <UIKit/UIKit.h>

@interface CSCourseVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *courseIntroTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeight;

@end
