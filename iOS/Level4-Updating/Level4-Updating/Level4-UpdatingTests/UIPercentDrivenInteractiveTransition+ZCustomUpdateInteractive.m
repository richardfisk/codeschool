//
//  UIPercentDrivenInteractiveTransition+ZCustomUpdateInteractive.m
//  Level4-Updating
//
//  Created by Eric Allam on 9/17/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIPercentDrivenInteractiveTransition+ZCustomUpdateInteractive.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation UIPercentDrivenInteractiveTransition (ZCustomUpdateInteractive)
+ (void) load
{
    [CSSwizzler swizzleClass:[UIPercentDrivenInteractiveTransition class]
               replaceMethod:@selector(updateInteractiveTransition:)
                  withMethod:@selector(custom_updateInteractiveTransition:)];
}

- (void)custom_updateInteractiveTransition:(CGFloat)percentComplete;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *interactiveUpdates = appDel.interactiveUpdates;
    
    [interactiveUpdates addObject:@(percentComplete)];
    
    appDel.interactiveUpdates = interactiveUpdates;
    
    [self custom_updateInteractiveTransition:percentComplete];
}
@end
