//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char presentedKey;
static char interactiveUpdatesKey;

@implementation AppDelegate (TestAdditions)

- (NSMutableArray *)interactiveUpdates
{
    NSMutableArray *result = objc_getAssociatedObject(self, &interactiveUpdatesKey);
    
    if (result) {
        return result;
    }else{
        return [NSMutableArray array];
    }
}

- (void)setInteractiveUpdates:(NSMutableArray *)interactiveUpdates
{
    objc_setAssociatedObject(self, &interactiveUpdatesKey, interactiveUpdates, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void) setPresentedViewController:(UIViewController *)presentedViewController
{
    objc_setAssociatedObject(self, &presentedKey, presentedViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentedViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentedKey);
}

@end
