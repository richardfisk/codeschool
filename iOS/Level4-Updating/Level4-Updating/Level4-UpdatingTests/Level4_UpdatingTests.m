//
//  Level3_AnimatedTransitionTests.m
//  Level3-AnimatedTransitionTests
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CSPropertyDefinition.h"

@interface Level4_UpdatingTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}
@end

@implementation Level4_UpdatingTests

- (void)setUp
{
    [super setUp];

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // This is how we are only running the tapViewWithAccessibilityLabel call once instead
    // of before every since test method
    if (!_appDel.presentedViewController) {
        [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testPropertyForInteractiveTransition
{
    CSPropertyDefinition *property = [self interactiveProperty];
    
    XCTAssert(property, @"Did not define a property on BadgesViewController for the interactive transitioning object");

}

- (void) testSettingProperty
{
    CSPropertyDefinition *property = [self interactiveProperty];
    
    if (property) {
        BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
        
        // Set the property to nil so we can test that it is set
        [badgesVC setValue:nil forKey:property.name];
        
        id<UIViewControllerAnimatedTransitioning> animated = [badgesVC animationControllerForDismissedController:_appDel.presentedViewController];
        
        id<UIViewControllerInteractiveTransitioning> transition = [badgesVC interactionControllerForDismissal:animated];
        
        XCTAssertEqualObjects([badgesVC valueForKey:property.name], transition, @"Did not set the %@ property in interactionControllerForDismissal: so you can access the interactive transition in the gesture action.", property.name);
    }else{
        XCTFail(@"Did not define a property on BadgesViewController for the interactive transition object");
    }
}

- (void) testUpdateInteractiveTransitionCalledOnSwipe
{
    [tester waitForViewWithAccessibilityLabel:@"Badge Stats"];
    [tester swipeViewWithAccessibilityLabel:@"Badge Stats" inDirection:KIFSwipeDirectionRight];
    
    XCTAssert(_appDel.interactiveUpdates.count > 0, @"Did not call updateInteractiveTransition on gesture change");
    XCTAssert([_appDel.interactiveUpdates[0] integerValue] < [[_appDel.interactiveUpdates lastObject] integerValue], @"Did not pass in the correct value to updateInteractiveTransition on gesture change. Make sure you divide the translation.x by the width of the view's frame.");
    
    CSPropertyDefinition *property = [self interactiveProperty];
    
    if (property) {
        BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
        
        UIPercentDrivenInteractiveTransition *interaction = [badgesVC valueForKey:property.name];
        
        XCTAssertEqualWithAccuracy(interaction.percentComplete, [[_appDel.interactiveUpdates lastObject] floatValue], 0.05, @"Make sure you send the updateInteractiveTransition: message to the %@ property on the BadgesViewController instance", property.name);
    }
    
}

- (CSPropertyDefinition *)interactiveProperty
{
    CSPropertyDefinition *property;
    
    property = [CSPropertyDefinition firstPropertyWithType:@"UIPercentDrivenInteractiveTransition" forClass:[BadgesViewController class]];
    
    if (!property) {
        property = [CSPropertyDefinition firstPropertyWithType:@"<UIViewControllerInteractiveTransitioning>" forClass:[BadgesViewController class]];
    }
    
    return property;
}

@end
