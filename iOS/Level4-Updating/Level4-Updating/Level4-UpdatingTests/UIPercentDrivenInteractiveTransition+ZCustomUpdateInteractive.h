//
//  UIPercentDrivenInteractiveTransition+ZCustomUpdateInteractive.h
//  Level4-Updating
//
//  Created by Eric Allam on 9/17/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPercentDrivenInteractiveTransition (ZCustomUpdateInteractive)
- (void)custom_updateInteractiveTransition:(CGFloat)percentComplete;
@end
