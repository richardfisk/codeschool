//
//  NSURLSession+ZAdditions.h
//  Level8-NSURLSession
//
//  Created by Eric Allam on 11/22/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLSession (ZAdditions)
- (NSURLSessionDataTask *)custom_dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler;

- (void)custom_finishTasksAndInvalidate;
@end
