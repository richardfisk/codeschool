//
//  CSPathsViewController+ZAdditions.h
//  Level8-NSURLSession
//
//  Created by Eric Allam on 11/25/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "CSPathsViewController.h"

@class CSTaskCustomCell;

@interface CSPathsViewController (ZAdditions)

@property (strong, nonatomic) CSTaskCustomCell *reusedCell;

- (CSTaskCustomCell *)custom_setupCellForIndexPath:(NSIndexPath *)indexPath;
@end
