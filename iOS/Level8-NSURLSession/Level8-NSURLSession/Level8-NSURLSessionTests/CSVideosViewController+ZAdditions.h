//
//  CSVideosViewController+ZAdditions.h
//  Level8-NSURLSession
//
//  Created by Eric Allam on 11/26/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "CSVideosViewController.h"

@class CSTaskCustomCell;

@interface CSVideosViewController (ZAdditions)
@property (strong, nonatomic) CSTaskCustomCell *reusedCell;

- (CSTaskCustomCell *)custom_setupCellForIndexPath:(NSIndexPath *)indexPath;
@end
