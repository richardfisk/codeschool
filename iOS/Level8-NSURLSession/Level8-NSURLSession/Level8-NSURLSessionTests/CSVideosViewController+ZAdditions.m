//
//  CSVideosViewController+ZAdditions.m
//  Level8-NSURLSession
//
//  Created by Eric Allam on 11/26/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "CSVideosViewController+ZAdditions.h"
#import "CSTaskCustomCell.h"
#import "CSSwizzler.h"
#import <objc/runtime.h>

static char reusedCellKey;

@implementation CSVideosViewController (ZAdditions)
+ (void) load
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    [CSSwizzler swizzleClass:self
               replaceMethod:@selector(setupCellForIndexPath:)
                  withMethod:@selector(custom_setupCellForIndexPath:)];
#pragma clang diagnostic pop
}

- (void)setReusedCell:(CSTaskCustomCell *)obj
{
    objc_setAssociatedObject(self, &reusedCellKey, obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CSTaskCustomCell *)reusedCell
{
    return objc_getAssociatedObject(self, &reusedCellKey);
}

- (CSTaskCustomCell *)custom_setupCellForIndexPath:(NSIndexPath *)indexPath;
{
    if (self.reusedCell) {
        return self.reusedCell;
    }else{
        return [self custom_setupCellForIndexPath:indexPath];
    }
}

@end
