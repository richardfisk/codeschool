//
//  Level8-NSURLSessionTests.m
//  Level8-NSURLSessionTests
//
//  Created by Eric Allam on 10/22/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

// Test imports
#import "CSRecordingTestCase.h"
#import "NSURLConnection+ZAdditions.h"
#import "AppDelegate+TestAdditions.h"
#import "CSPropertyDefinition.h"
#import "NSURLSession+ZAdditions.h"
#import "CSPathsViewController+ZAdditions.h"
#import "CSVideosViewController+ZAdditions.h"
#import "NSArray+Additions.h"

// App imports
#import "CSPathsViewController.h"
#import "CSTaskCustomCell.h"
#import "CSVideosViewController.h"
#import "CSVideoViewController.h"

@interface Level8_NSURLSessionTests : CSRecordingTestCase
- (CSPathsViewController *)pathsVC;
@property (assign) BOOL settingTheImageToNull;
@end

@implementation Level8_NSURLSessionTests

- (void)setUp
{
    [super setUp];
    
    self.settingTheImageToNull = NO;
}

#pragma mark - Testing CSPathsViewController conversion

-(void)testJSONRequestReplacedWithNSURLSession
{
    [tester waitForViewWithAccessibilityLabel:@"Code School Paths"];
    
    NSString *pathsJSONURL = @"http://codeschoolmockapi.herokuapp.com/api/app/v1/paths";
    
    BOOL result = [self didMakeNSURLConnectionRequestWithURL:pathsJSONURL];
    
    // Test whether the sendAsynchrousRequest call is still there
    XCTAssertFalse(result, @"Did not replace the JSON request in CSPathsViewController#viewDidLoad to use an NSURLSession");
    
    // Test whether they actually make the request with the task and populate the table
    
    CSPathsViewController *pathsVC = self.pathsVC;
    
    // Wait for the network request to finish and the table to be populated
    [tester runBlock:^KIFTestStepResult(NSError **error) {
        NSUInteger numberOfRows = [pathsVC tableView:pathsVC.tableView numberOfRowsInSection:0];
        
        KIFTestWaitCondition(numberOfRows > 0, error, @"Waiting for the network request to finish and the CSPathsViewController's tableView to be populated");
        
        return KIFTestStepResultSuccess;
    } timeout:3.0f];
}

- (void)testBadgeRequestsForCellReplacedWithNSURLSession
{
    NSURLSession *session;
    
    // Test CSPathsViewController for an added NSURLSession property
    CSPropertyDefinition *sessionProperty = [self sessionProperty];
    XCTAssert(sessionProperty, @"Did not add a NSURLSession property to CSPathsViewController to store the session for the badge images");
    
    CSPathsViewController *pathsVC = self.pathsVC;
    
    if (sessionProperty) {
        // Test to make sure the session property has been set
        
        session = (NSURLSession *)[pathsVC valueForKey:sessionProperty.name];
        
        XCTAssert(session, @"Did not set the session property of CSPathsViewController to an NSURLSession instance");
    }
    
    [tester waitForViewWithAccessibilityLabel:@"Code School Paths"];
    
    // Wait for the JSON network request to finish and the table to be populated
    [tester runBlock:^KIFTestStepResult(NSError **error) {
        NSUInteger numberOfRows = [pathsVC tableView:pathsVC.tableView numberOfRowsInSection:0];
        
        KIFTestWaitCondition(numberOfRows > 0, error, @"Waiting for the network request to finish and the CSPathsViewController's tableView to be populated");
        
        return KIFTestStepResultSuccess;
    } timeout:3.0f];
    
    // Test for one of the badges
    BOOL result = [self didMakeNSURLConnectionRequestWithURL:@"http://courseware.codeschool.com.s3.amazonaws.com/app/ios.png"];
    
    XCTAssertFalse(result, @"Didn't the replace the cell image requests with NSURLSession in CSPathsViewController");
    
    // Test to make sure the badges eventually get set (using NSURLSession)
    result = [self didMakeNSURLSessionRequestWithURL:@"http://courseware.codeschool.com.s3.amazonaws.com/app/ios.png"];
    
    XCTAssert(result, @"Didn't the replace the cell image requests with NSURLSession in CSPathsViewController");
    
    // Test to make sure the badge request uses the image session
    if (session) {
        NSURLRequest *request = [self sessionRequestForURL:@"http://courseware.codeschool.com.s3.amazonaws.com/app/ios.png"];
        
        if (request) {
            NSURLSession *sessionUsed = (NSURLSession *)[NSURLProtocol propertyForKey:@"NSURLSessionUsed" inRequest:request];
            
            XCTAssertEqualObjects(sessionUsed, session, @"Did not use the property for the image session to load the badges in CSPathsViewController");
        }
        
    }
}

- (void)testBadgeRequestsCancelledIfCellReused
{
    CSPathsViewController *pathsVC = [CSPathsViewController new];
    
    [self triggerViewDidLoad:pathsVC];
    
    [tester runBlock:^KIFTestStepResult(NSError **error) {
        NSUInteger numberOfRows = [pathsVC.tableView numberOfRowsInSection:0];
        
        KIFTestWaitCondition(numberOfRows > 0, error, @"Waiting for the network request to finish and the CSPathsViewController's tableView to be populated");
        
        return KIFTestStepResultSuccess;
    } timeout:3.0f];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    CSTaskCustomCell *cell = (CSTaskCustomCell *)[pathsVC tableView:pathsVC.tableView cellForRowAtIndexPath:indexPath];
    
    XCTAssert(cell.task, @"Did not set the task property of the CSTaskCustomCell in CSPathsViewController's tableView:cellForRowAtIndexPath: method");
    
    if (cell.task) {
        [cell.imageView addObserver:self
                         forKeyPath:NSStringFromSelector(@selector(image))
                            options:NSKeyValueObservingOptionNew
                            context:NULL];
        
        
        NSURLSessionTask *originalTask = cell.task;
        
        XCTAssert(cell.task.state == NSURLSessionTaskStateRunning || cell.task.state == NSURLSessionTaskStateCompleted, @"Did not 'resume' the NSURLSessionDataTask in CSPathsViewController's tableView:cellForRowAtIndexPath: method");
        
        // Test for the task getting cancelled if the cell is reused
        pathsVC.reusedCell = cell;
        
        [pathsVC tableView:pathsVC.tableView cellForRowAtIndexPath:indexPath];
        
        pathsVC.reusedCell = nil;
        
        XCTAssert(originalTask.state == NSURLSessionTaskStateCompleted, @"Did not 'cancel' the task on a reused cell in CSPathsViewController's tableView:cellForRowAtIndexPath: method");
        
        // Test for the completion handler checking for the error param
        
        [tester waitForTimeInterval:2.0];
        
        XCTAssert(self.settingTheImageToNull == NO, @"Did not check for the error parameter in the completion handler in CSPathsViewController's tableView:cellForRowAtIndexPath: method");
    }
    

}


- (void)testSessionIsInvalidatedInViewDidLoad
{
    CSPathsViewController *pathsVC = [CSPathsViewController new];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self triggerViewDidLoad:pathsVC];
    
    XCTAssert([appDel.finishTasksCalls includesStringEqualTo:@"[CSPathsViewController viewDidLoad]"], @"Did not invalidate the session in [CSPathsViewController viewDidLoad] and allow the tasks to finish");
}

#pragma mark - Testing CSVideosViewController conversion

- (void)testVideosVCConverted
{
    NSNumber *iosPathID = @3;
    
    NSString *JSONURL = [NSString stringWithFormat:@"http://codeschoolmockapi.herokuapp.com/api/app/v1/paths/%@/videos", iosPathID];
    
    CSVideosViewController *videosVC = [[CSVideosViewController alloc] initWithPathID:iosPathID];
    
    [self triggerViewDidLoad:videosVC];
    
    BOOL result = [self didMakeNSURLConnectionRequestWithURL:JSONURL];
    
    // Test whether the sendAsynchrousRequest call is still there
    XCTAssertFalse(result, @"Did not replace the JSON request in [CSVideosViewController viewDidLoad] to use an NSURLSession");
    
    // Wait for the network request to finish and the table to be populated
    [tester runBlock:^KIFTestStepResult(NSError **error) {
        NSUInteger numberOfRows = [videosVC.tableView numberOfRowsInSection:0];
        
        KIFTestWaitCondition(numberOfRows > 0, error, @"Waiting for the network request to finish and the CSVideosViewController's tableView to be populated");
        
        return KIFTestStepResultSuccess;
    } timeout:3.0f];
    
    // Tests for thumbnails
    NSURLSession *session;
    
    // Test CSVideosViewController for an added NSURLSession property
    CSPropertyDefinition *sessionProperty = [self sessionPropertyForClass:[CSVideosViewController class]];
    XCTAssert(sessionProperty, @"Did not add a NSURLSession property to CSVideosViewController to store the session for the video thumbnails");
    
    if (sessionProperty) {
        // Test to make sure the session property has been set
        session = (NSURLSession *)[sessionProperty valueOnObject:videosVC];
        
        XCTAssert(session, @"Did not set the session property of CSVideosViewController to an NSURLSession instance");
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    CSTaskCustomCell *cell = (CSTaskCustomCell *)[videosVC tableView:videosVC.tableView cellForRowAtIndexPath:indexPath];
    
    // Test for one of the badges
    result = [self didMakeNSURLConnectionRequestWithURL:@"http://hls.codeschool.com.s3.amazonaws.com/2e11422f3cd52020414ba002463cc801_1.jpg"];
    
    XCTAssertFalse(result, @"Didn't the replace the cell image requests with NSURLSession in CSVideosViewController");
    
    // Test to make sure the badges eventually get set (using NSURLSession)
    result = [self didMakeNSURLSessionRequestWithURL:@"http://hls.codeschool.com.s3.amazonaws.com/2e11422f3cd52020414ba002463cc801_1.jpg"];
    
    XCTAssert(result, @"Didn't the replace the cell image requests with NSURLSession in CSVideosViewController");
    
    // Test to make sure the thumbnail request uses the image session
    if (session) {
        NSURLRequest *request = [self sessionRequestForURL:@"http://hls.codeschool.com.s3.amazonaws.com/2e11422f3cd52020414ba002463cc801_1.jpg"];
        
        if (request) {
            NSURLSession *sessionUsed = (NSURLSession *)[NSURLProtocol propertyForKey:@"NSURLSessionUsed" inRequest:request];
            
            XCTAssertEqualObjects(sessionUsed, session, @"Did not use the property for the image session to load the badges in CSVideosViewController");
        }
        
    }
    
    // Test for the thumbnail task getting cancelled if the cell is reused
    XCTAssert(cell.task, @"Did not set the task property of the CSTaskCustomCell in CSVideosViewController's tableView:cellForRowAtIndexPath: method");
    
    if (cell.task) {
        [cell.imageView addObserver:self
                         forKeyPath:NSStringFromSelector(@selector(image))
                            options:NSKeyValueObservingOptionNew
                            context:NULL];
        
        
        NSURLSessionTask *originalTask = cell.task;
        
        XCTAssert(cell.task.state == NSURLSessionTaskStateRunning || cell.task.state == NSURLSessionTaskStateCompleted, @"Did not 'resume' the NSURLSessionDataTask in CSVideosViewController's tableView:cellForRowAtIndexPath: method");
        
        // Test for the task getting cancelled if the cell is reused
        videosVC.reusedCell = cell;
        
        [videosVC tableView:videosVC.tableView cellForRowAtIndexPath:indexPath];
        
        videosVC.reusedCell = nil;
        
        XCTAssert(originalTask.state == NSURLSessionTaskStateCompleted, @"Did not 'cancel' the task on a reused cell in CSVideosViewController's tableView:cellForRowAtIndexPath: method");
        
        // Test for the completion handler checking for the error param
        
        [tester waitForTimeInterval:2.0];
        
        XCTAssert(self.settingTheImageToNull == NO, @"Did not check for the error parameter in the completion handler in CSVideosViewController's tableView:cellForRowAtIndexPath: method");
    }
}

#pragma mark - Testing CSVideoViewController conversion

- (void)testVideoVCConverted
{
    NSDictionary *video = @{
        @"badge_url": @"https://d1ffx7ull4987f.cloudfront.net/images/achievements/large_badge/307/level-1-on-core-ios-7-45046cadb933cc707038d10dccb28110.png",
        @"course": @"Core iOS 7",
        @"description": @"Learn the most common problems you'll run into when upgrading your app for iOS 7.",
        @"download_url": @"http://hls.codeschool.com.s3.amazonaws.com/2a201cc5249eef3dda4f813d678de123.mp4",
        @"duration": @"4:31",
        @"highres_thumbnail_url": @"http://hls.codeschool.com.s3.amazonaws.com/2a201cc5249eef3dda4f813d678de123_1.jpg",
        @"id": @4,
        @"more_url": @"https://www.codeschool.com/courses/core-ios-7",
        @"streaming_url": @"http://hls.codeschool.com.s3.amazonaws.com/c27342f3979688fadbc7663f90035390.m3u8",
        @"thumbnail_url": @"http://hls.codeschool.com.s3.amazonaws.com/2e11422f3cd52020414ba002463cc801_1.jpg",
        @"title": @"Updating from iOS 6",
    };
    
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:video];
    
    [self triggerViewDidLoad:videoVC];
    
    BOOL result = [self didMakeNSURLConnectionRequestWithURL:video[@"highres_thumbnail_url"]];
    
    // Test whether the sendAsynchrousRequest call is still there
    XCTAssertFalse(result, @"Did not replace the JSON request in [CSVideoViewController viewDidLoad] to use an NSURLSession");
    
    result = [self didMakeNSURLSessionRequestWithURL:video[@"highres_thumbnail_url"]];
    
    XCTAssert(result, @"Did not replace the JSON request in [CSVideoViewController viewDidLoad] to use an NSURLSession");
}

#pragma mark - KVO methods

// This is used in the testBadgeRequestsCancelledIfCellReused method
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(image))]) {        
        id changeTo = change[NSKeyValueChangeNewKey];
        
        if ([changeTo isKindOfClass:[UIImage class]]) {
        }else{
            self.settingTheImageToNull = YES;
        }
    }
}


#pragma mark - Helper methods

- (NSURLRequest *)sessionRequestForURL:(NSString *)urlString
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSUInteger index = [self indexForSessionRequestWithURL:urlString];
    
    if (index != NSNotFound) {
        return appDel.sessionRequests[index];
    }else{
        return nil;
    }
}

- (BOOL)didMakeNSURLSessionRequestWithURL:(NSString *)urlString;
{
    NSUInteger index = [self indexForSessionRequestWithURL:urlString];
    
    if (index != NSNotFound) {
        return YES;
    }else{
        return NO;
    }
}


- (NSUInteger)indexForSessionRequestWithURL:(NSString *)urlString
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray *requests = [appDel.sessionRequests copy];
    
    if (requests.count == 0) {
        return NSNotFound;
    }
    
    NSUInteger index = [requests indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        
        NSURLRequest *requestToCheck = (NSURLRequest *)obj;
        NSString *requestURL = [[requestToCheck URL] absoluteString];
        
        return [requestURL isEqualToString:urlString];
    }];
    
    return index;
}

- (BOOL)didMakeNSURLConnectionRequestWithURL:(NSString *)urlString;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray *requests = [appDel.connectionRequests copy];
    
    if (requests.count == 0) {
        return NO;
    }
    
    NSUInteger index = [requests indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        
        NSURLRequest *requestToCheck = (NSURLRequest *)obj;
        NSString *requestURL = [[requestToCheck URL] absoluteString];
        
        return [requestURL isEqualToString:urlString];
    }];
    
    if (index != NSNotFound) {
        return YES;
    }else{
        return NO;
    }
}

- (CSPathsViewController *)pathsVC {
    UINavigationController *navVC = (UINavigationController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    return (CSPathsViewController *)[navVC topViewController];
}

- (CSPropertyDefinition *)sessionProperty
{
    return [CSPropertyDefinition firstPropertyWithType:@"NSURLSession" forClass:[CSPathsViewController class]];
}

- (CSPropertyDefinition *)sessionPropertyForClass:(Class)cls
{
    return [CSPropertyDefinition firstPropertyWithType:@"NSURLSession" forClass:cls];
}

- (void)triggerViewDidLoad:(UIViewController *)vc
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-value"
    vc.view;
#pragma clang diagnostic pop
}

- (NSString *)taskStateDescription:(NSURLSessionTask *)task
{
    switch (task.state) {
        case NSURLSessionTaskStateRunning:
            return @"NSURLSessionTaskStateRunning";
            break;
        case NSURLSessionTaskStateSuspended:
            return @"NSURLSessionTaskStateSuspended";
            break;
        case NSURLSessionTaskStateCanceling:
            return @"NSURLSessionTaskStateCanceling";
            break;
        case NSURLSessionTaskStateCompleted:
            return @"NSURLSessionTaskStateCompleted";
            break;
    }
}

@end
