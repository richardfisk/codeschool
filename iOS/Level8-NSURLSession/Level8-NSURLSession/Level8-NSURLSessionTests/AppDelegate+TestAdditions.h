//
//  AppDelegate+TestAdditions.h
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (TestAdditions) <NSURLSessionDelegate, NSURLSessionTaskDelegate>
@property (strong, atomic) NSArray *connectionRequests;
@property (strong, atomic) NSArray *sessionRequests;
@property (strong, nonatomic) NSURLSession *testSession;
@property (strong, nonatomic) NSArray *finishTasksCalls;

- (void)addNewFinishTaskCall:(NSString *)call;
- (void)resetFinishTaskCalls;
@end
