//
//  NSURLConnection+ZAdditions.h
//  Level8-NSURLSession
//
//  Created by Eric Allam on 11/21/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLConnection (ZAdditions)
+ (void)custom_sendAsynchronousRequest:(NSURLRequest*)request
                          queue:(NSOperationQueue*)queue
              completionHandler:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))handler;
@end
