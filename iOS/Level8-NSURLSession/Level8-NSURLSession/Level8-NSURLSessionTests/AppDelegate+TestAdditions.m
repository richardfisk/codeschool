//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/runtime.h>

static char connectionRequestsKey;
static char sessionRequestsKey;
static char testSessionKey;
static char finishTasksCallsKey;

@implementation AppDelegate (TestAdditions)

- (void)resetFinishTaskCalls;
{
    self.finishTasksCalls = @[];
}

- (void)addNewFinishTaskCall:(NSString *)call;
{
    NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:self.finishTasksCalls];
    
    [mutableArray addObject:call];
    
    self.finishTasksCalls = [NSArray arrayWithArray:mutableArray];
}

- (void)setFinishTasksCalls:(NSArray *)finishTasksCalls
{
    objc_setAssociatedObject(self, &finishTasksCallsKey, finishTasksCalls, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSArray *)finishTasksCalls
{
    return objc_getAssociatedObject(self, &finishTasksCallsKey);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    NSData *requestData = (NSData *)[NSURLProtocol propertyForKey:@"RequestData" inRequest:dataTask.originalRequest];
    
    NSMutableURLRequest *mutableRequest = (NSMutableURLRequest *)dataTask.originalRequest;
    
    if (requestData) {
        NSMutableData *mutableData = [[NSMutableData alloc] initWithData:requestData];
        [mutableData appendData:data];
        
        [NSURLProtocol setProperty:[NSData dataWithData:mutableData] forKey:@"RequestData" inRequest:mutableRequest];
    }else{
        [NSURLProtocol setProperty:data forKey:@"RequestData" inRequest:mutableRequest];
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    void (^completionHandler)(NSData *, NSURLResponse *, NSError *) = [NSURLProtocol propertyForKey:@"CompletionHandler" inRequest:task.originalRequest];
    
    NSData *data = [NSURLProtocol propertyForKey:@"RequestData" inRequest:task.originalRequest];
    
    if (completionHandler) {
        [session.delegateQueue addOperationWithBlock:^{
            completionHandler(data, task.response, error);
        }];
    }
}

- (void)setTestSession:(NSURLSession *)obj
{
    objc_setAssociatedObject(self, &testSessionKey, obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

// Lazy-load the session
- (NSURLSession *)testSession
{
    NSURLSession *session = objc_getAssociatedObject(self, &testSessionKey);
    
    if (!session) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
        session.sessionDescription = @"Test Session";
        
        self.testSession = session;
    }
    
    return session;
}

- (void)setSessionRequests:(NSArray *)obj
{
    objc_setAssociatedObject(self, &sessionRequestsKey, obj, OBJC_ASSOCIATION_RETAIN);
}

- (NSArray *)sessionRequests
{
    return objc_getAssociatedObject(self, &sessionRequestsKey);
}

- (void)setConnectionRequests:(NSArray *)connectionRequests
{
    objc_setAssociatedObject(self, &connectionRequestsKey, connectionRequests, OBJC_ASSOCIATION_RETAIN);
}

- (NSArray *)connectionRequests
{
    return objc_getAssociatedObject(self, &connectionRequestsKey);
}

@end
