//
//  NSURLSession+ZAdditions.m
//  Level8-NSURLSession
//
//  Created by Eric Allam on 11/22/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "NSURLSession+ZAdditions.h"
#import "AppDelegate+TestAdditions.h"
#import "CSSwizzler.h"

@implementation NSURLSession (ZAdditions)

+ (void)load
{
    [CSSwizzler swizzleClass:self
               replaceMethod:@selector(dataTaskWithURL:completionHandler:)
                  withMethod:@selector(custom_dataTaskWithURL:completionHandler:)];
    
    [CSSwizzler swizzleClass:self
                         replaceMethod:@selector(finishTasksAndInvalidate)
                            withMethod:@selector(custom_finishTasksAndInvalidate)];
}

- (void)custom_finishTasksAndInvalidate;
{
    NSString *calledFrom = [NSThread callStackSymbols][1];
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[.+\\]" options:0 error:nil];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [regex enumerateMatchesInString:calledFrom
                            options:0
                              range:NSMakeRange(0, calledFrom.length)
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
    {
        [appDel addNewFinishTaskCall:[calledFrom substringWithRange:result.range]];
        *stop = YES;
    }];
}

- (NSURLSessionDataTask *)custom_dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableArray *mutableRequests = [NSMutableArray arrayWithArray:appDel.sessionRequests];
    [mutableRequests addObject:request];
    
    appDel.sessionRequests = [NSArray arrayWithArray:mutableRequests];
    
    [NSURLProtocol setProperty:completionHandler forKey:@"CompletionHandler" inRequest:request];
    [NSURLProtocol setProperty:self forKey:@"NSURLSessionUsed" inRequest:request];
    
    return [appDel.testSession dataTaskWithRequest:request];
}
@end
