//
//  NSURLConnection+ZAdditions.m
//  Level8-NSURLSession
//
//  Created by Eric Allam on 11/21/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "NSURLConnection+ZAdditions.h"
#import "AppDelegate+TestAdditions.h"
#import "CSSwizzler.h"

@implementation NSURLConnection (ZAdditions)

+ (void)load
{
    [CSSwizzler swizzleClass:self
          replaceClassMethod:@selector(sendAsynchronousRequest:queue:completionHandler:)
                  withMethod:@selector(custom_sendAsynchronousRequest:queue:completionHandler:)];
}

+ (void)custom_sendAsynchronousRequest:(NSURLRequest*)request
                                 queue:(NSOperationQueue*)queue
                     completionHandler:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))handler;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *mutableRequests = [NSMutableArray arrayWithArray:appDel.connectionRequests];
    [mutableRequests addObject:request];
    
    appDel.connectionRequests = [NSArray arrayWithArray:mutableRequests];
    
    [self custom_sendAsynchronousRequest:request queue:queue completionHandler:handler];
}
@end
