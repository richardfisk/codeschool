#import "CSVideosViewController.h"
#import "CSVideoViewController.h"
#import "CSTaskCustomCell.h"

@interface CSVideosViewController ()
@property (strong, nonatomic) NSNumber *pathID;
@property (strong, nonatomic) NSArray *videos;
@end

static NSString * const CSVideosURL = @"http://codeschoolmockapi.herokuapp.com/api/app/v1/paths/%@/videos";
static NSString * const CSVideoCellIdentifier = @"com.CodeSchool.VideoCell";

@implementation CSVideosViewController

- (instancetype)initWithPathID:(NSNumber *)pathID
{
    self = [super init];
    
    if (self) {
        self.pathID = pathID;
        
        [self.tableView registerClass:[CSTaskCustomCell class] forCellReuseIdentifier:CSVideoCellIdentifier];
        
        // TODO: create the NSURLSession here to use for downloading thumbnails
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *CSVideosURLWithPathID = [NSString stringWithFormat:CSVideosURL, self.pathID];
    NSURL *videosURL = [NSURL URLWithString:CSVideosURLWithPathID];
    
    // After switching over to NSURLConnection, you won't need this request object anymore
    NSURLRequest *videosRequest = [NSURLRequest requestWithURL:videosURL];
    
    
    // TODO: replace this with NSURLSession (try just using [NSURLSession sharedSession])
    //       and dataTaskWithURL:completionHandler:
    [NSURLConnection sendAsynchronousRequest:videosRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
    {
        self.videos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.videos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSTaskCustomCell *cell = [self setupCellForIndexPath:indexPath];
    
    NSDictionary *video = self.videos[indexPath.row];
    
    cell.textLabel.text = video[@"title"];
    cell.detailTextLabel.text = video[@"duration"];
    cell.imageView.image = [UIImage imageNamed:@"VideoPlaceholder"];
    
    NSURL *thumbnailURL = [NSURL URLWithString:video[@"thumbnail_url"]];
    NSURLRequest *thumbnailRequest = [NSURLRequest requestWithURL:thumbnailURL];
    
    // TODO: replace this by using the session property on this controller and dataTaskWithURL:completionHandler:
    [NSURLConnection sendAsynchronousRequest:thumbnailRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
    {
        cell.imageView.image = [UIImage imageWithData:data];
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{    
    NSDictionary *video = self.videos[indexPath.row];
    
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:video];
    
    [self.navigationController pushViewController:videoVC animated:YES];
}

- (CSTaskCustomCell *)setupCellForIndexPath:(NSIndexPath *)indexPath
{
    CSTaskCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CSVideoCellIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
