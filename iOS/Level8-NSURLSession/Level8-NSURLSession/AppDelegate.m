#import "AppDelegate.h"
#import "CSPathsViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.tintColor = [UIColor colorWithRed:167/255.0 green:109/255.0 blue:58/255.0 alpha:1.0];
    
    CSPathsViewController *pathsVC = [CSPathsViewController new];
    
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:pathsVC];
    self.window.rootViewController = navVC;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
