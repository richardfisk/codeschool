#import "CSVideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "CSMoreInfoViewController.h"
#import "CSMoreInfoTransitionDelegate.h"

@interface CSVideoViewController ()
@property (strong, nonatomic) NSDictionary *video;
@property (strong, nonatomic) CSMoreInfoTransitionDelegate *transitionDelegate;
@end

@implementation CSVideoViewController

#pragma mark - Video methods

- (void)playVideo:(id)sender
{
    NSURL *contentURL = [NSURL URLWithString:self.video[@"streaming_url"]];

    MPMoviePlayerViewController *movieVC = [[MPMoviePlayerViewController alloc] initWithContentURL:contentURL];
        
    [self presentMoviePlayerViewControllerAnimated:movieVC];
}

#pragma mark - ViewController methods

- (instancetype) initWithVideoInfo:(NSDictionary *)video;
{
    self = [super init];
    
    if (self) {        
        self.video = video;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Handle the Dynamic Type change
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contentSizeChanged:) name:@"UIContentSizeCategoryDidChangeNotification" object:nil];
    
    self.titleLabel.text = self.video[@"title"];
    self.detailsView.text = self.video[@"description"];
    self.durationLabel.text = self.video[@"duration"];
    
    NSURL *highresThumbURL = [NSURL URLWithString:self.video[@"highres_thumbnail_url"]];
    NSURLRequest *highresRequest = [NSURLRequest requestWithURL:highresThumbURL];
    
    // TODO: replace this by using the [NSURLSession shared] along with dataTaskWithURL:completionHandler:
    [NSURLConnection sendAsynchronousRequest:highresRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
    {
        self.thumbnailView.image = [UIImage imageWithData:data];
    }];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    self.detailsHeightConstraint.constant = [self textViewHeight:self.detailsView];
}

#pragma mark - Dynamic Type

-(void)contentSizeChanged:(NSNotification *)note
{
    UIFontDescriptor *baseFont = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleCaption1];
    
    UIFont *detailsFont = [UIFont fontWithDescriptor:baseFont size:0];
    
    self.detailsView.font = detailsFont;
    
    [self.view setNeedsUpdateConstraints];
}

#pragma mark - More Info methods

- (IBAction)moreInfo:(id)sender;
{
    NSURL *webURL = [NSURL URLWithString:self.video[@"more_url"]];
    
    CSMoreInfoViewController *moreVC = [[CSMoreInfoViewController alloc]
                                           initWithURL:webURL];
    
    self.transitionDelegate = [CSMoreInfoTransitionDelegate new];
    
    moreVC.modalPresentationStyle = UIModalPresentationCustom;
    moreVC.transitioningDelegate = self.transitionDelegate;
    
    [self presentViewController:moreVC animated:YES completion:nil];
}

- (CGFloat)textViewHeight:(UITextView *)textView
{
    [textView.layoutManager ensureLayoutForTextContainer:textView.textContainer];
    CGRect usedRect = [textView.layoutManager usedRectForTextContainer:textView.textContainer];
    return ceilf(usedRect.size.height +
                 textView.textContainerInset.top +
                 textView.textContainerInset.bottom);
}

@end
