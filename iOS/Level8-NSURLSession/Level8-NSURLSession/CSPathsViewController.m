#import "CSPathsViewController.h"
#import "CSVideosViewController.h"
#import "CSTaskCustomCell.h"

@interface CSPathsViewController ()
@property (strong, nonatomic) NSArray *paths;
@end

static NSString * const CSPathsURL = @"http://codeschoolmockapi.herokuapp.com/api/app/v1/paths";
static NSString * const CSPathCellIdentifier = @"com.CodeSchool.PathCell";

@implementation CSPathsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.view.accessibilityLabel = @"Code School Paths";
        
        self.title = @"Code School";
        
        [self.tableView registerClass:[CSTaskCustomCell class] forCellReuseIdentifier:CSPathCellIdentifier];
        
        // TODO: initialize the session here for downloading badges in the Table View cells
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *pathsURL = [NSURL URLWithString:CSPathsURL];
    
    // After converting to NSURLSession, you won't need this request object anymore
    NSURLRequest *request = [NSURLRequest requestWithURL:pathsURL];
    
    // TODO: use an NSURLSession here instead, along with the dataTaskWithURL:completionHandler: method
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
    {
        self.paths = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.paths.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSTaskCustomCell *cell = [self setupCellForIndexPath:indexPath];
    
    NSDictionary *path = self.paths[indexPath.row];
    
    cell.textLabel.text = path[@"title"];
    cell.imageView.image = [UIImage imageNamed:@"BadgePlaceholder"];
    
    NSURL *badgeURL = [NSURL URLWithString:path[@"badge_url"]];
    NSURLRequest *badgeRequest = [NSURLRequest requestWithURL:badgeURL];
    
    // TODO: Cancel the data task if this cell is being reused
    
    // TODO: use the NSURLSession property on this controller along with dataTaskWithURL:completionHandler:
    [NSURLConnection sendAsynchronousRequest:badgeRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
    {
        cell.imageView.image = [UIImage imageWithData:data];
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *path = self.paths[indexPath.row];
    
    CSVideosViewController *videosVC = [[CSVideosViewController alloc] initWithPathID:path[@"id"]];
    
    [self.navigationController pushViewController:videosVC animated:YES];
}

- (CSTaskCustomCell *)setupCellForIndexPath:(NSIndexPath *)indexPath
{
    CSTaskCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CSPathCellIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
