#import "CSPhotoDetailVC.h"

#import "Photo.h"

#import "CSEditPhotoNote.h"

@implementation CSPhotoDetailVC

- (id)init
{
    self = [super init];
    if(self) {
        self.title = [NSString stringWithFormat:@"Photo ID %ld",(long)self.photo.photoId];
        
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                    target:self
                                                                                    action:@selector(editButtonTapped:)];
        self.navigationItem.rightBarButtonItem = editButton;
    }
    return self;
}

- (void)loadView
{
    self.scrollView = [[UIScrollView alloc] init];
    
    self.photoImageView = [[UIImageView alloc] init];
    [self.scrollView addSubview:self.photoImageView];

    self.photoNameLabel = [[UILabel alloc] init];
    self.photoNameLabel.backgroundColor = [UIColor clearColor];
    self.photoNameLabel.textAlignment = NSTextAlignmentRight;
    
    UIFontDescriptor *helvetica24 = [UIFontDescriptor fontDescriptorWithName:@"HelveticaNeue" size:24.0f];
    UIFontDescriptor *boldBase = [helvetica24 fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    self.photoNameLabel.font = [UIFont fontWithDescriptor:boldBase size:24.0f];
    
    self.photoNameLabel.textColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.photoNameLabel];
    
    self.functionBar = [[UIToolbar alloc] init];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *linkButton = [[UIBarButtonItem alloc] initWithTitle:@"Send Via Email"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(linkButtonTapped:)];
    [self.functionBar setItems:@[flexibleSpace, linkButton]];
    [self.scrollView addSubview:self.functionBar];
    
    self.notesView = [[UITextView alloc] init];
    self.notesView.editable = NO;
    UIFontDescriptor *helvetica22 = [UIFontDescriptor fontDescriptorWithName:@"HelveticaNeue" size:22.0f];
    self.notesView.font = [UIFont fontWithDescriptor:helvetica22 size:22.0f];
    
    [self.scrollView addSubview:self.notesView];
    
    self.view = self.scrollView;
}

- (void)viewDidLoad
{
    self.view.tintColor = [UIColor redColor];
    self.photoImageView.image = [self.photo loadImage:self.photo.filename];
    
    self.photoNameLabel.text = self.photo.name;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.notesView.text = self.photo.notes;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.photoImageView.frame = CGRectMake(0,
                                           0,
                                           CGRectGetWidth(self.scrollView.frame),
                                           200);

    // TODO: First, get the size of the label's text in a CGSize variable using the sizeWithAttributes: method
//    CGSize photoNameLabelSize2 = [self.photoNameLabel.text sizeWithAttributes:self.photoNameLabel.font.fontDescriptor.fontAttributes];
//    
        CGSize photoNameLabelSize = [self.photoNameLabel.text sizeWithAttributes:@{NSFontAttributeName: self.photoNameLabel.font, UIFontDescriptorTraitsAttribute: @(UIFontDescriptorTraitBold)}];
    NSLog(@"%@", NSStringFromCGSize(photoNameLabelSize));//, NSStringFromCGSize(photoNameLabelSize2));
    // TODO: Next, calculate the frame of photoNameLabel using the new CGSize variable.  The width and height of the label frame should come from the width and height of the CGSize calculation above.  The origin.x and origin.y, on the other hand, need to be set based on the position of self.photoImageView since the label needs to sit on top of the bottom-right corner of the image.

    // Consider setting the origin.x to the width of self.photoImageView.frame minus the new calculated width of the label minus 20 (for right padding), and the origin.y to the maxY of self.photoImageView.frame minus 40 (again, for bottom padding)
    self.photoNameLabel.frame = CGRectMake(- 20.0f + self.photoImageView.frame.size.width - photoNameLabelSize.width, CGRectGetMaxY(self.photoImageView.frame) - 40.0f, photoNameLabelSize.width, photoNameLabelSize.height);
    NSLog(@"%@", NSStringFromCGRect(self.photoNameLabel.frame));
    self.functionBar.frame = CGRectMake(0,
                                        CGRectGetMaxY(self.photoImageView.frame),
                                        CGRectGetWidth(self.view.frame),
                                        44);
    
    self.notesView.frame = CGRectMake(10,
                                      CGRectGetMaxY(self.functionBar.frame) + 10,
                                      CGRectGetWidth(self.scrollView.frame) - 10,
                                      140);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)editButtonTapped:(id)sender
{
    CSEditPhotoNote *csEditPhotoNote = [[CSEditPhotoNote alloc] init];
    csEditPhotoNote.photo = self.photo;

    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:self.photo.name
                                                                               style:UIBarButtonItemStyleBordered
                                                                              target:nil
                                                                              action:nil]];
    [self.navigationController pushViewController:csEditPhotoNote animated:YES];
}

- (void)linkButtonTapped:(id)sender
{
    NSString *subject = self.photo.name;
    NSString *body = self.photo.notes;
    NSArray *to = [NSArray arrayWithObject:@"nowhere@example.com"];
    
    MFMailComposeViewController *composeVC = [[MFMailComposeViewController alloc] init];
    composeVC.mailComposeDelegate = self;
    [composeVC setSubject:subject];
    [composeVC setMessageBody:body isHTML:NO];
    [composeVC setToRecipients:to];
    NSData *imageData = UIImagePNGRepresentation(self.photoImageView.image);
    [composeVC addAttachmentData:imageData mimeType:@"image/png" fileName:self.photo.filename];
    
    [self presentViewController:composeVC animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
