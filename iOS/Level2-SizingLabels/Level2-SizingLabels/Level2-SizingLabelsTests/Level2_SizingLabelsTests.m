#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "CSPhotoListVC.h"
#import "CSPhotoDetailVC.h"

#import "CSSwizzler.h"

#import "Photo.h"

@interface Level2_SizingLabelsTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}

@end

@implementation Level2_SizingLabelsTests

- (void)setUp
{
    [super setUp];
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)tearDown
{
    [super tearDown];
    _appDel = nil;
}

- (void)testLabelSizing
{
    UITabBarController *tabBarController = (UITabBarController *)_appDel.window.rootViewController;
    UINavigationController *aboutNav = tabBarController.viewControllers[0];
    CSPhotoListVC *csPhotoListVC = (CSPhotoListVC *)aboutNav.topViewController;
    [csPhotoListVC.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    Photo *photo = csPhotoListVC.dataSource[0];
    
    CSPhotoDetailVC *csPhotoDetailVC = [[CSPhotoDetailVC alloc] init];
    csPhotoDetailVC.photo = photo;
    [csPhotoDetailVC.view layoutIfNeeded];
    
    CGSize photoNameLabelSize = [csPhotoDetailVC.photoNameLabel.text sizeWithAttributes:@{NSFontAttributeName: csPhotoDetailVC.photoNameLabel.font, UIFontDescriptorTraitsAttribute: @(UIFontDescriptorTraitBold)}];
    
    XCTAssert(photoNameLabelSize.width == csPhotoDetailVC.photoNameLabel.frame.size.width, @"Did not use sizeWithAttributes to set the width of the frame");
    XCTAssert(photoNameLabelSize.height == csPhotoDetailVC.photoNameLabel.frame.size.height, @"Did not use sizeWithAttributes to set the height of the frame");
    XCTAssert(csPhotoDetailVC.photoNameLabel.frame.origin.x == CGRectGetWidth(csPhotoDetailVC.photoImageView.frame) - photoNameLabelSize.width - 20, @"Did not calculate the new frame.origin.y of the label to be equal to the self.photoImageView.frame.width - photoNameLabelSize.width - 20 (for side padding)");
}

@end
