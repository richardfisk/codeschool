//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char sizeWithAttributesCalledKey;

@implementation AppDelegate (TestAdditions)

- (void) setSizeWithAttributesCalled:(NSNumber *)sizeWithAttributesCalled
{
    objc_setAssociatedObject(self, &sizeWithAttributesCalledKey, sizeWithAttributesCalled, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)sizeWithAttributesCalled
{
    return objc_getAssociatedObject(self, &sizeWithAttributesCalledKey);
}

@end
