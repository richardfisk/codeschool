//
//  NSString+Custom.h
//  Level2-SizingLabels
//
//  Created by Jon Friskics on 9/19/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Custom)

- (CGSize)custom_sizeWithAttributes:(NSDictionary *)attrs;

@end
