//
//  NSString+Custom.m
//  Level2-SizingLabels
//
//  Created by Jon Friskics on 9/19/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "NSString+Custom.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation NSString (Custom)

+ (void)load
{
    [CSSwizzler swizzleClass:[NSString class]
               replaceMethod:@selector(sizeWithAttributes:)
                  withMethod:@selector(custom_sizeWithAttributes:)];
}

- (CGSize)custom_sizeWithAttributes:(NSDictionary *)attrs
{
    AppDelegate *_appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _appDel.sizeWithAttributesCalled = @YES;
    return [self custom_sizeWithAttributes:attrs];
}

@end
