//
//  SideDismissalAnimated.h
//  Level4-NewAnimatedTransition
//
//  Created by Eric Allam on 9/18/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SideDismissalAnimated : NSObject <UIViewControllerAnimatedTransitioning>

@end
