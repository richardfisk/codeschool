//
//  Level3_AnimatedTransitionTests.m
//  Level3-AnimatedTransitionTests
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CSPropertyDefinition.h"

@interface Level4_NewAnimatedTransitionTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}
@end

@implementation Level4_NewAnimatedTransitionTests

- (void)setUp
{
    [super setUp];

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // This is how we are only running the tapViewWithAccessibilityLabel call once instead
    // of before every since test method
    if (!_appDel.presentedViewController) {
        [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testInteractiveBooleanProperty
{
    CSPropertyDefinition *property = [self interactiveBoolProperty];
    
    XCTAssert(property, @"Did not add a BOOL property to BadgesViewController for the interactive transition");
}

- (void) testSettingInteractiveBooleanProperty
{
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    
    CSPropertyDefinition *property = [self interactiveBoolProperty];
    
    if (property) {
        XCTAssertFalse([[badgesVC valueForKey:property.name] boolValue], @"The %@ property on BadgesViewController should not be set to YES until the gesture starts", property.name);
        
        [tester swipeViewWithAccessibilityLabel:@"Badge Stats" inDirection:KIFSwipeDirectionRight];
        
        XCTAssert([[badgesVC valueForKey:property.name] boolValue], @"The %@ property on BadgesViewContorller should be set to YES when the pan gesture starts", property.name);
    }
}

- (void) testCustomAnimatedTransitionForInteractive
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    
    CSPropertyDefinition *property = [self interactiveBoolProperty];
    
    if (property) {
        BOOL original = [[badgesVC valueForKey:property.name] boolValue];
        
        [badgesVC setValue:@(NO) forKey:property.name];
        
        id<UIViewControllerAnimatedTransitioning> nonInteractiveAnimated = [badgesVC animationControllerForDismissedController:statsVC];
        
        [badgesVC setValue:@(YES) forKey:property.name];
        
        id<UIViewControllerAnimatedTransitioning> interactiveAnimated = [badgesVC animationControllerForDismissedController:statsVC];
        
        [badgesVC setValue:@(original) forKey:property.name];
        
        XCTAssertNotEqualObjects([nonInteractiveAnimated class], [interactiveAnimated class], @"Did not return a different animated transition from animationControllerForDismissedController: during an interactive dismissal");
        
        XCTAssert([interactiveAnimated conformsToProtocol:@protocol(UIViewControllerAnimatedTransitioning)], @"The object returned from animationControllerForDismissedController: does not conform to the UIViewControllerAnimatedTransitioning protocol");
    }
    
    
}

- (CSPropertyDefinition *)interactiveBoolProperty
{
    return [CSPropertyDefinition firstPropertyWithEncoding:@encode(BOOL) forClass:[BadgesViewController class]];
}

- (CSPropertyDefinition *)interactiveProperty
{
    CSPropertyDefinition *property;
    
    property = [CSPropertyDefinition firstPropertyWithType:@"UIPercentDrivenInteractiveTransition" forClass:[BadgesViewController class]];
    
    if (!property) {
        property = [CSPropertyDefinition firstPropertyWithType:@"<UIViewControllerInteractiveTransitioning>" forClass:[BadgesViewController class]];
    }
    
    return property;
}

@end
