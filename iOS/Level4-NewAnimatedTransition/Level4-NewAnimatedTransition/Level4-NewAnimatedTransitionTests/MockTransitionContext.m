//
//  MockTransitionContext.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/15/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "MockTransitionContext.h"

@interface MockTransitionContext () {
    UIView *_containerView;
    UIViewController *_toController;
    UIViewController *_fromController;
    BOOL _presentation;
}

@end

@implementation MockTransitionContext

- (NSString *) description
{
    return [NSString stringWithFormat:@"<MockTransitionContext containerView = %@; toController = %@; fromController = %@;>", _containerView, _toController, _fromController];
}

- (instancetype) initWithFromController:(UIViewController *)from toController:(UIViewController *)to presentation:(BOOL)flag;
{
    if (self = [super init]) {
        _toController = to;
        _fromController = from;
        _containerView = [[UIView alloc] initWithFrame:[[[UIApplication sharedApplication] keyWindow] frame]];
        
        _presentation = flag;
        
        // If this is a dismissal, add the to controller first (which would be the presenting controller)
        if (!_presentation) {
            [_containerView addSubview:_toController.view];
        }
        
        [_containerView addSubview:_fromController.view];
    }
    
    return self;
}

- (UIView *)containerView;
{
    return _containerView;
}

// Most of the time this is YES. For custom transitions that use the new UIModalPresentationCustom
// presentation type we will invoke the animateTransition: even though the transition should not be
// animated. This allows the custom transition to add or remove subviews to the container view.
- (BOOL)isAnimated;
{
    return YES;
}

- (BOOL)isInteractive;
{
    return NO;
}

- (BOOL)transitionWasCancelled;
{
    return NO;
}

- (UIModalPresentationStyle)presentationStyle;
{
    return UIModalPresentationCustom;
}

// It only makes sense to call these from an interaction controller that
// conforms to the UIViewControllerInteractiveTransitioning protocol and was
// vended to the system by a container view controller's delegate or, in the case
// of a present or dismiss, the transitioningDelegate.
- (void)updateInteractiveTransition:(CGFloat)percentComplete;{ }
- (void)finishInteractiveTransition;{ }
- (void)cancelInteractiveTransition;{ }

// This must be called whenever a transition completes (or is cancelled.)
// Typically this is called by the object conforming to the
// UIViewControllerAnimatedTransitioning protocol that was vended by the transitioning
// delegate.  For purely interactive transitions it should be called by the
// interaction controller. This method effectively updates internal view
// controller state at the end of the transition.
- (void)completeTransition:(BOOL)didComplete;
{
    _completed = YES;
}


// Currently only two keys are defined by the
// system - UITransitionContextToViewControllerKey, and
// UITransitionContextFromViewControllerKey.
- (UIViewController *)viewControllerForKey:(NSString *)key;
{
    if ([key isEqualToString:UITransitionContextFromViewControllerKey]) {
        return _fromController;
    }else{
        return _toController;
    }
}

// The frame's are set to CGRectZero when they are not known or
// otherwise undefined.  For example the finalFrame of the
// fromViewController will be CGRectZero if and only if the fromView will be
// removed from the window at the end of the transition. On the other
// hand, if the finalFrame is not CGRectZero then it must be respected
// at the end of the transition.
- (CGRect)initialFrameForViewController:(UIViewController *)vc;
{
    return vc.view.frame;
}

- (CGRect)finalFrameForViewController:(UIViewController *)vc;
{
    return CGRectZero;
}

@end
