//
//  Level3_AnimatedTransitionTests.m
//  Level3-AnimatedTransitionTests
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface Level3_DismissalTransitionTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}
@end

@implementation Level3_DismissalTransitionTests

- (void)setUp
{
    [super setUp];

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // This is how we are only running the tapViewWithAccessibilityLabel call once instead
    // of before every since test method
    if (!_appDel.presentedViewController) {
        [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testDismissalDelegateMethodImplemented
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    id <UIViewControllerTransitioningDelegate> delegate = statsVC.transitioningDelegate;
    
    XCTAssert([delegate respondsToSelector:@selector(animationControllerForDismissedController:)], @"Did not implement the animationControllerForDismissedController: method in the transitioning delegate");
}

- (void) testDismissalDelegateMethodReturnProtocol
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    id <UIViewControllerTransitioningDelegate> delegate = statsVC.transitioningDelegate;
    
    id transition = [delegate animationControllerForDismissedController:statsVC];
    
    XCTAssert([transition conformsToProtocol:@protocol(UIViewControllerAnimatedTransitioning)], @"The object returned from animationControllerForDismissedController: does not conform to the UIViewControllerAnimatedTransitioning protocol");
}

- (void)testDismissalTransition
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    
    id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate animationControllerForDismissedController:statsVC];
    
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:statsVC toController:badgesVC presentation:NO];
    
    NSTimeInterval animationInterval = [transition transitionDuration:context];
    
    XCTAssertEqualWithAccuracy(animationInterval, 1.0f, 0.5, @"Did not return a reasonable animation time, should be between 0.5 seconds and 1.5 seconds");
    
    // Make the statsVC view in the middle of the screen, but 20 points down and to the right
    statsVC.view.frame = CGRectMake(20, 20, 280, 528);
    
    [transition animateTransition:context];
    
    NSLog(@"keyframe deets: %@ %@ %@", _appDel.calledAnimateKeyframes, _appDel.keyframeDuration, _appDel.numberOfKeyframes);
    
    // Test to make sure they used the keyframe api
    XCTAssert([_appDel.calledAnimateKeyframes isEqualToNumber:@1], @"Did not use the UIView keyframe API in the dismissal transition animation");
    
    // Test to make sure they used at least two keyframes
    XCTAssert([_appDel.numberOfKeyframes integerValue] > 1, @"Did not use at least 2 keyframes in the dismissal transition animation");
    
    // Test to make sure they use the correct duration for the animation
    XCTAssert([_appDel.keyframeDuration isEqualToNumber:@(animationInterval)], @"Did not use the transitionDuration for the keyframe duration");
    
    // Test to make sure they complete the transition with YES
    [tester runBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
        
        KIFTestWaitCondition(context.completed, error, @"Did not call completeTransition:YES on the transitionContext inside the animation completion block");
        
        return KIFTestStepResultSuccess;
    } timeout:animationInterval+0.1];
}

@end
