//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char presentedKey;
static char animatedKey;
static char calledAnimateKeyframesKey;
static char numberOfKeyframesKey;
static char keyframeDurationKey;

@implementation AppDelegate (TestAdditions)

- (void) setKeyframeDuration:(NSNumber *)keyframeDuration
{
    objc_setAssociatedObject(self, &keyframeDurationKey, keyframeDuration, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)keyframeDuration
{
    return objc_getAssociatedObject(self, &keyframeDurationKey);
}

- (void) setNumberOfKeyframes:(NSNumber *)numberOfKeyframes
{
    objc_setAssociatedObject(self, &numberOfKeyframesKey, numberOfKeyframes, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)numberOfKeyframes
{
    NSNumber *result;
    
    if ((result = objc_getAssociatedObject(self, &numberOfKeyframesKey))) {
        return result;
    }else{
        return @0;
    }
}

- (void) setCalledAnimateKeyframes:(NSNumber *)calledAnimateKeyframes
{
    objc_setAssociatedObject(self, &calledAnimateKeyframesKey, calledAnimateKeyframes, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)calledAnimateKeyframes
{
    NSNumber *result;
    
    if ((result = objc_getAssociatedObject(self, &calledAnimateKeyframesKey))) {
        return result;
    }else{
        return @0;
    }
}

- (void) setAnimated:(NSNumber *)animated
{
    objc_setAssociatedObject(self, &animatedKey, animated, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)animated
{
    return objc_getAssociatedObject(self, &animatedKey);
}

- (void) setPresentedViewController:(UIViewController *)presentedViewController
{
    objc_setAssociatedObject(self, &presentedKey, presentedViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentedViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentedKey);
}

@end
