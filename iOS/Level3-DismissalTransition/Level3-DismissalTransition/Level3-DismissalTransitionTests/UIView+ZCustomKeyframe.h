//
//  UIView+ZCustomKeyframe.h
//  Level3-DismissalTransition
//
//  Created by Eric Allam on 9/16/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ZCustomKeyframe)
+ (void)custom_animateKeyframesWithDuration:(NSTimeInterval)duration
                                      delay:(NSTimeInterval)delay
                                    options:(UIViewKeyframeAnimationOptions)options
                                 animations:(void (^)(void))animations
                                 completion:(void (^)(BOOL finished))completion;

+ (void)custom_addKeyframeWithRelativeStartTime:(double)frameStartTime
                               relativeDuration:(double)frameDuration
                                     animations:(void (^)(void))animations;
@end
