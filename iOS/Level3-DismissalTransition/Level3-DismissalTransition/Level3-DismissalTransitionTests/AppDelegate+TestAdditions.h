//
//  AppDelegate+TestAdditions.h
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (TestAdditions)
@property (strong, nonatomic) UIViewController *presentedViewController;
@property (strong, nonatomic) NSNumber *animated;

// Dismissal Stuff
@property (strong, nonatomic) NSNumber *calledAnimateKeyframes;
@property (strong, nonatomic) NSNumber *numberOfKeyframes;
@property (strong, nonatomic) NSNumber *keyframeDuration;

@end
