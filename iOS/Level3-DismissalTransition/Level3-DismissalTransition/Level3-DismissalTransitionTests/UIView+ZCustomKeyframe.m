//
//  UIView+ZCustomKeyframe.m
//  Level3-DismissalTransition
//
//  Created by Eric Allam on 9/16/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIView+ZCustomKeyframe.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation UIView (ZCustomKeyframe)

+ (void) load
{
    [CSSwizzler swizzleClass:[UIView class] replaceClassMethod:@selector(animateKeyframesWithDuration:delay:options:animations:completion:) withMethod:@selector(custom_animateKeyframesWithDuration:delay:options:animations:completion:)];
    
    [CSSwizzler swizzleClass:[UIView class] replaceClassMethod:@selector(addKeyframeWithRelativeStartTime:relativeDuration:animations:) withMethod:@selector(custom_addKeyframeWithRelativeStartTime:relativeDuration:animations:)];
}

+ (void)custom_animateKeyframesWithDuration:(NSTimeInterval)duration
                                      delay:(NSTimeInterval)delay
                                    options:(UIViewKeyframeAnimationOptions)options
                                 animations:(void (^)(void))animations
                                 completion:(void (^)(BOOL finished))completion;
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.calledAnimateKeyframes = @1;
    appDel.keyframeDuration = @(duration);
    appDel.numberOfKeyframes = @0;
    
    [self custom_animateKeyframesWithDuration:duration
                                        delay:delay
                                      options:options
                                   animations:animations
                                   completion:completion];
}

+ (void)custom_addKeyframeWithRelativeStartTime:(double)frameStartTime
                               relativeDuration:(double)frameDuration
                                     animations:(void (^)(void))animations;
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.numberOfKeyframes = @([appDel.numberOfKeyframes integerValue] + 1);
    
    [self custom_addKeyframeWithRelativeStartTime:frameStartTime
                                 relativeDuration:frameDuration
                                       animations:animations];
}

@end
