#import "SideDismissalTransition.h"

@implementation SideDismissalTransition

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 5.0f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    CGRect presentedFrame = [transitionContext initialFrameForViewController:fromViewController];
    
    void (^completion)(BOOL) = ^(BOOL finished){
        [transitionContext completeTransition:YES];
    };
    
    CGFloat padding = 10.0f;
    
    void (^keyFrameAnimations)(void) = ^{
        [UIView addKeyframeWithRelativeStartTime:0.0f relativeDuration:0.8 animations:^{
            toViewController.view.frame = CGRectMake(0,0, CGRectGetWidth(toViewController.view.frame) - padding, CGRectGetHeight(toViewController.view.frame) - padding);
            fromViewController.view.frame = CGRectMake(presentedFrame.origin.x, -10.0f, CGRectGetWidth(presentedFrame), CGRectGetHeight(presentedFrame));
        }];
        [UIView addKeyframeWithRelativeStartTime:0.8f relativeDuration:0.8 animations:^{
            fromViewController.view.frame = presentedFrame;
            fromViewController.view.transform = CGAffineTransformMakeScale(0.9f, 0.9f);
        }];
    };
    
    [UIView animateKeyframesWithDuration:[self transitionDuration:transitionContext] delay:0.0f options:0 animations:keyFrameAnimations completion:completion];
}

@end
