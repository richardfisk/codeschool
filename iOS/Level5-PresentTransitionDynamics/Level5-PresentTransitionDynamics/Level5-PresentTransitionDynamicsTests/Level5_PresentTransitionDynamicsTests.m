//
//  Level5_PresentTransitionDynamicsTests.m
//  Level5-PresentTransitionDynamicsTests
//
//  Created by Jon Friskics and Eric Allam on 10/9/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SideTransition.h"
#import "CSPropertyDefinition.h"

@interface Level5_PresentTransitionDynamicsTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}

@end

@implementation Level5_PresentTransitionDynamicsTests


- (void)setUp
{
    [super setUp];
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testDidImplementDynamicAnimator
{
    StatsViewController *statsVC = [StatsViewController new];
    
    BadgesViewController *badgesVC = [BadgesViewController new];
    
    SideTransition *transition = [SideTransition new];
    
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:badgesVC toController:statsVC];
    
    [transition animateTransition:context];
    
    XCTAssert([[transition animator] isKindOfClass:[UIDynamicAnimator class]], @"Did not set the animator delegate property");
}

- (void)testDidSetAnimatorDelegate
{
    StatsViewController *statsVC = [StatsViewController new];
    
    BadgesViewController *badgesVC = [BadgesViewController new];
    
    SideTransition *transition = [SideTransition new];
    
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:badgesVC toController:statsVC];
    
    [transition animateTransition:context];
    
    XCTAssert([transition.animator.delegate isKindOfClass:[SideTransition class]], @"Did not set the animator delegate property");
}

- (void)testAnimatedTransition
{
    
    StatsViewController *statsVC = [StatsViewController new];
    statsVC.view.frame = [[[UIApplication sharedApplication] keyWindow] frame];
    
    BadgesViewController *badgesVC = [BadgesViewController new];
    badgesVC.view.frame = [[[UIApplication sharedApplication] keyWindow] frame];
    
    SideTransition *transition = [SideTransition new];
    
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:badgesVC toController:statsVC];
    
    [transition animateTransition:context];
    
    [transition.animator.behaviors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XCTAssert([obj isKindOfClass:[UISnapBehavior class]], @"Did not create and add a snap behavior");
    }];
    
    XCTAssert([self hasSetCorrectSnapPoint], @"Did not set the snap point to the midpoint of fromVC");
}

- (void)testHasImplementedAnimatorDelegateMethod
{
    StatsViewController *statsVC = [StatsViewController new];
    
    BadgesViewController *badgesVC = [BadgesViewController new];
    
    SideTransition *transition = [SideTransition new];
    
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:badgesVC toController:statsVC];
    
    [transition animateTransition:context];
    
    if ([self hasImplementedAnimatorDelegateMethod]) {
        [transition dynamicAnimatorDidPause:transition.animator];
        
        XCTAssert(context.completed, @"Did not complete the transition in the dynamicAnimatorDidPause: method");
    }else{
        XCTFail(@"Did not implement the dynamic animator did pause delegate method");
    }
    
}

- (BOOL)hasImplementedAnimatorDelegateMethod
{
    SideTransition *sideTransition = [[SideTransition alloc] init];
    
    return [sideTransition respondsToSelector:@selector(dynamicAnimatorDidPause:)];
}

- (BOOL)hasImplementedDelegateMethod
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    return [statsVC.transitioningDelegate respondsToSelector:@selector(animationControllerForPresentedController:presentingController:sourceController:)];
}

- (BOOL)hasSetCorrectSnapPoint
{
    NSLog(@"%s: {%d, %d}", __PRETTY_FUNCTION__, [_appDel.snapItem[0] intValue], [_appDel.snapItem[1] intValue]);
    
    return (([_appDel.snapItem[0] intValue] == 160) && ([_appDel.snapItem[1] intValue] == 240 || [_appDel.snapItem[1] intValue] == 284));
}

@end
