#import "SideTransition.h"

@implementation SideTransition

- (NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 1.0f;
}

- (void) animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    self.transitionContext = transitionContext;
    
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [[transitionContext containerView] addSubview:toVC.view];
    
    CGRect fullFrame = [transitionContext initialFrameForViewController:fromVC];
    
    toVC.view.frame = CGRectMake(fullFrame.size.width + 16, 20, fullFrame.size.width - 40, fullFrame.size.height - 40);
        
    // TODO: Create a UIDynamicAnimator instance using the transition context's containerView as the reference view
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:[transitionContext containerView]];
    
    // TODO: Set the animator object's delegate to self
    self.animator.delegate = self;

    // TODO: Create a snap behavior for toVC.view that snaps to the midpoint of fromVC.view with a damping of 0.9
    UISnapBehavior *snapBehavior = [[UISnapBehavior alloc] initWithItem:toVC.view snapToPoint:fromVC.view.center];
    snapBehavior.damping = 0.9;
    [self.animator addBehavior:snapBehavior];
}

// TODO: implement the dynamicAnimatorDidPause delegate method, and call the completeTransition method inside of it
- (void)dynamicAnimatorDidPause:(UIDynamicAnimator*)animator
{
    [self.transitionContext completeTransition:YES];
}

@end
