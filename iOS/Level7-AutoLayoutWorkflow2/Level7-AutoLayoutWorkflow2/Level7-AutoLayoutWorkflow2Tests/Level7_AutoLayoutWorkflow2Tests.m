#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "CSAppDelegate.h"

#define kDoubleEpsilon (0.5)

static BOOL doubleCloseTo(double a, double b)
{
    if(fabs(a-b) < kDoubleEpsilon) {
        return YES;
    } else {
        return NO;
    }
}

@interface NSIBPrototypingLayoutConstraint : NSLayoutConstraint
@end

@interface NSAutoresizingMaskLayoutConstraint : NSLayoutConstraint
@end

@interface NSContentSizeLayoutConstraint : NSLayoutConstraint
@end

@interface Level7_AutoLayoutWorkflow2Tests : CSRecordingTestCase

@end

#pragma mark - Enum to NSString helper methods
NSString *NSStringFromNSLayoutAttribute(NSLayoutAttribute attribute)
{
    switch (attribute) {
        case 1:
            return @"NSLayoutAttributeLeft";
            break;
        case 2:
            return @"NSLayoutAttributeRight";
            break;
        case 3:
            return @"NSLayoutAttributeTop";
            break;
        case 4:
            return @"NSLayoutAttributeBottom";
            break;
        case 5:
            return @"NSLayoutAttributeLeading";
            break;
        case 6:
            return @"NSLayoutAttributeTrailing";
            break;
        case 7:
            return @"NSLayoutAttributeWidth";
            break;
        case 8:
            return @"NSLayoutAttributeHeight";
            break;
        case 9:
            return @"NSLayoutAttributeCenterX";
            break;
        case 10:
            return @"NSLayoutAttributeCenterY";
            break;
        case 11:
            return @"NSLayoutAttributeBaseline";
            break;
        default:
            return @"NSLayoutAttributeNotAnAttribute";
            break;
    }
}

NSString *NSStringFromNSLayoutRelation(NSLayoutRelation relation)
{
    switch (relation) {
        case -1:
            return @"NSLayoutRelationLessThanOrEqual";
            break;
        case 0:
            return @"NSLayoutRelationEqual";
            break;
        case 1:
            return @"NSLayoutRelationGreaterThanOrEqual";
            break;
        default:
            return @"OOPS";
            break;
    }
}

@implementation Level7_AutoLayoutWorkflow2Tests {
    CSAppDelegate *_appDel;
    UIView *_mainView;
    BOOL _mainViewWantsAutolayout;
}

- (void)setUp
{
    [super setUp];
    _appDel = [[UIApplication sharedApplication] delegate];
    _mainView = (UIView *)_appDel.window.subviews[0];
    _mainViewWantsAutolayout = (NSInteger)[_mainView performSelector:@selector(_wantsAutolayout)];
}

- (void)tearDown
{
    _mainViewWantsAutolayout = NO;
    [super tearDown];
}

- (void)testCoreiOS7ImageView
{
    NSString *restorationIdentiferToTest = @"core-ios7-image";
    NSArray *attributesToTest = @[@(NSLayoutAttributeCenterX),@(NSLayoutAttributeCenterY)];
    
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:attributesToTest forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id viewConstraintToTest = obj[0];
                    if([viewConstraintToTest firstAttribute] == [attributesToTest[0] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:0 forAttribute:[attributesToTest[0] integerValue]], @"Did not set a center horizontally in container constraint between core-ios7-image and header-view");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[1] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:0 forAttribute:[attributesToTest[1] integerValue]], @"Did not set a center vertically in container constraint between core-ios7-image and header-view");
                    }
                }];
            }
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testBadge1
{
    NSString *restorationIdentiferToTest = @"badge1";
    NSArray *attributesToTest = @[@(NSLayoutAttributeTop)];
    
    NSString *mainViewRestorationIdentifierToTest = @"main-view";
    NSArray *mainViewAttributesToTest = @[@(NSLayoutAttributeLeading)];
    
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:attributesToTest forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id viewConstraintToTest = obj[0];
                    if([viewConstraintToTest firstAttribute] == [attributesToTest[0] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:54.0 forAttribute:[attributesToTest[0] integerValue]], @"Did not set a vertical spacing constraint between badge1 and badge7");
                    }
                }];
            }
        }
        
        NSArray *mainViewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:mainViewRestorationIdentifierToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *mainViewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:mainViewRestorationIdentifierToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *mainViewPrototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *mainViewContentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *mainViewNormalConstraints = [[NSMutableArray alloc] init];
        
        [mainViewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [mainViewPrototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [mainViewContentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [mainViewNormalConstraints addObject:vals];
            }
        }];
        
        [mainViewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [mainViewPrototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [mainViewContentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [mainViewNormalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *mainViewSetOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [mainViewPrototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [mainViewSetOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        if([self checkCorrectAttributes:mainViewAttributesToTest forConstraints:mainViewNormalConstraints]) {
            [mainViewNormalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                id mainViewConstraintToTest = obj[0];
                if([mainViewConstraintToTest firstAttribute] == [mainViewAttributesToTest[0] integerValue] && [[[mainViewConstraintToTest secondItem] restorationIdentifier] isEqualToString:mainViewRestorationIdentifierToTest] && [[[mainViewConstraintToTest firstItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                    XCTAssert([self constraint:mainViewConstraintToTest hasConstantEqualTo:20.0 forAttribute:[mainViewAttributesToTest[0] integerValue]], @"Did not set a leading space to container constraint between badge1 and main-view");
                }
            }];
        }
        
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testBadge2
{
    NSString *restorationIdentiferToTest = @"badge2";
    NSArray *attributesToTest = @[@(NSLayoutAttributeLeading),@(NSLayoutAttributeCenterY)];
    
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:attributesToTest forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id viewConstraintToTest = obj[0];
                    if([viewConstraintToTest firstAttribute] == [attributesToTest[0] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:-46.0 forAttribute:[attributesToTest[0] integerValue]], @"Did not set a horizontal spacing constraint between badge2 and badge1");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[1] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:0.0 forAttribute:[attributesToTest[1] integerValue]], @"Did not set a center y constraint between badge2 and badge1");
                    }
                }];
            }
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testBadge3
{
    NSString *restorationIdentiferToTest = @"badge3";
    NSArray *attributesToTest = @[@(NSLayoutAttributeLeading),@(NSLayoutAttributeCenterY)];
    
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:attributesToTest forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id viewConstraintToTest = obj[0];
                    if([viewConstraintToTest firstAttribute] == [attributesToTest[0] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:-46.0 forAttribute:[attributesToTest[0] integerValue]], @"Did not set a horizontal spacing constraint between badge3 and badge2");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[1] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:0.0 forAttribute:[attributesToTest[1] integerValue]], @"Did not set a center y constraint between badge3 and badge2");
                    }
                }];
            }
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testBadge4
{
    NSString *restorationIdentiferToTest = @"badge4";
    NSArray *attributesToTest = @[@(NSLayoutAttributeLeading),@(NSLayoutAttributeCenterY)];
    
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:attributesToTest forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id viewConstraintToTest = obj[0];
                    if([viewConstraintToTest firstAttribute] == [attributesToTest[0] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:-46.0 forAttribute:[attributesToTest[0] integerValue]], @"Did not set a horizontal spacing constraint between badge4 and badge3");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[1] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:0.0 forAttribute:[attributesToTest[1] integerValue]], @"Did not set a center y constraint between badge4 and badge3");
                    }
                }];
            }
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testBadge5
{
    NSString *restorationIdentiferToTest = @"badge5";
    NSArray *attributesToTest = @[@(NSLayoutAttributeLeading),@(NSLayoutAttributeCenterY)];
    
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:attributesToTest forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id viewConstraintToTest = obj[0];
                    if([viewConstraintToTest firstAttribute] == [attributesToTest[0] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:-46.0 forAttribute:[attributesToTest[0] integerValue]], @"Did not set a horizontal spacing constraint between badge5 and badge4");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[1] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:0.0 forAttribute:[attributesToTest[1] integerValue]], @"Did not set a center y constraint between badge5 and badge4");
                    }
                }];
            }
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testBadge6
{
    NSString *restorationIdentiferToTest = @"badge6";
    NSArray *attributesToTest = @[@(NSLayoutAttributeLeading),@(NSLayoutAttributeCenterY)];
    
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:restorationIdentiferToTest fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:attributesToTest forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id viewConstraintToTest = obj[0];
                    if([viewConstraintToTest firstAttribute] == [attributesToTest[0] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:-46.0 forAttribute:[attributesToTest[0] integerValue]], @"Did not set a horizontal spacing constraint between badge6 and badge5");
                    } else if([viewConstraintToTest firstAttribute] == [attributesToTest[1] integerValue] && [[[viewConstraintToTest secondItem] restorationIdentifier] isEqualToString:restorationIdentiferToTest]) {
                        XCTAssert([self constraint:viewConstraintToTest hasConstantEqualTo:0.0 forAttribute:[attributesToTest[1] integerValue]], @"Did not set a center y constraint between badge6 and badge5");
                    }
                }];
            }
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}


#pragma mark - Instance Methods

- (NSArray *)firstItemConstraintsForRestorationIdentifier:(NSString *)restorationID fromLayoutEngineConstraints:(NSArray *)layoutEngineConstraints
{
    __block NSMutableArray *relevantConstraints = [[NSMutableArray alloc] init];
    
    [layoutEngineConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([[[obj firstItem] restorationIdentifier] isEqualToString:restorationID]) {
            if([obj isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            }
        }
    }];
    return [[NSArray alloc] initWithArray:relevantConstraints];
}

- (NSArray *)secondItemConstraintsForRestorationIdentifier:(NSString *)restorationID fromLayoutEngineConstraints:(NSArray *)layoutEngineConstraints
{
    __block NSMutableArray *relevantConstraints = [[NSMutableArray alloc] init];
    
    [layoutEngineConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([[[obj secondItem] restorationIdentifier] isEqualToString:restorationID]) {
            if([obj isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            }
        }
    }];
    return [[NSArray alloc] initWithArray:relevantConstraints];
}

- (BOOL)reportAmbiguousLayoutTestFailureWithSet:(NSMutableSet *)setOfViewsWithAmbiguousLayout
{
    if(setOfViewsWithAmbiguousLayout.count == 1) {
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ view has ambiguous layout because you didn't create enough constraints for it.  Make sure that the view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", [[setOfViewsWithAmbiguousLayout allObjects] objectAtIndex:0]]);
        return YES;
    } else if(setOfViewsWithAmbiguousLayout.count == 2) {
        NSMutableString *viewsWithAmbiguousLayout = [[NSMutableString alloc] init];
        [[setOfViewsWithAmbiguousLayout allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if(idx < setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"%@ ",obj]];
            } else if(idx == setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"and %@",obj]];
            }
        }];
        
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ views have ambiguous layout because you didn't create enough constraints for them.  Make sure that each view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", viewsWithAmbiguousLayout]);
        return YES;
    } else if(setOfViewsWithAmbiguousLayout.count > 2) {
        NSMutableString *viewsWithAmbiguousLayout = [[NSMutableString alloc] init];
        [[setOfViewsWithAmbiguousLayout allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if(idx < setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"%@, ",obj]];
            } else if(idx == setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"and %@",obj]];
            }
        }];
        
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ views have ambiguous layout because you didn't create enough constraints for them.  Make sure that each view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", viewsWithAmbiguousLayout]);
        return YES;
    }
    return NO;
}

- (BOOL)constraint:(NSLayoutConstraint *)constraint hasConstantEqualTo:(CGFloat)constant forAttribute:(NSLayoutAttribute)attribute
{
    return doubleCloseTo([constraint constant], constant);
}

- (BOOL)checkCorrectAttributes:(NSArray *)attributes forConstraints:(NSArray *)constraints
{
    __block NSMutableArray *existingAttributes = [[NSMutableArray alloc] init];
    
    [constraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id constraint = obj[0];
        [existingAttributes addObject:@([constraint firstAttribute])];
    }];
    
    __block NSMutableArray *requiredCopy = [[NSMutableArray alloc] initWithArray:attributes];
    
    for (id attribute in attributes) {
        for(id existing in existingAttributes) {
            if([existing integerValue] == [attribute integerValue]) {
                [requiredCopy removeObject:existing];
            }
        }
    }
    __block BOOL isCorrect = YES;
    
    [requiredCopy enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XCTFail(@"%@",[NSString stringWithFormat:@"missing %@ constraint",NSStringFromNSLayoutAttribute([obj integerValue])]);
        isCorrect = NO;
    }];
    return isCorrect;
}

@end