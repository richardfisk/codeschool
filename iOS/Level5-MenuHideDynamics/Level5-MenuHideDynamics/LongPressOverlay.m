#import "LongPressOverlay.h"

@implementation LongPressOverlay

- (id)initWithFrame:(CGRect)frame point:(CGPoint)point {
    self = [super initWithFrame:frame];
    if(self) {
        self.pointOfLongPress = point;
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    self.mailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.mailButton setImage:[UIImage imageNamed:@"mail_icon"]
                     forState:UIControlStateNormal];
    self.mailButton.layer.opacity = 1.0;
    [self addSubview:self.mailButton];
    
    self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.editButton setImage:[UIImage imageNamed:@"edit_icon"]
                     forState:UIControlStateNormal];
    [self addSubview:self.editButton];

    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self];

    self.mailButton.frame = CGRectMake(self.pointOfLongPress.x - 320,
                                       self.pointOfLongPress.y - 200,
                                       40,
                                       40);
    
    self.editButton.frame = CGRectMake(self.pointOfLongPress.x + 320,
                                       self.pointOfLongPress.y + 200,
                                       40,
                                       40);
    
    UISnapBehavior *snap1 = [[UISnapBehavior alloc] initWithItem:self.mailButton
                                                     snapToPoint:CGPointMake(self.pointOfLongPress.x - 25, self.pointOfLongPress.y - 30)];
    snap1.damping = 0.8;
    UISnapBehavior *snap2 = [[UISnapBehavior alloc] initWithItem:self.editButton
                                                     snapToPoint:CGPointMake(self.pointOfLongPress.x + 25, self.pointOfLongPress.y - 30)];
    snap2.damping = 0.8;
    
    [self.animator addBehavior:snap1];
    [self.animator addBehavior:snap2];
}

@end
