//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>


static char animatorInitCalledKey;
static char referenceFrameKey;
static char pushItem1Key;
static char pushItem2Key;
static char shouldIAddBehaviorsKey;
static char animatorKey;

@implementation AppDelegate (TestAdditions)

- (void) setAnimatorInitCalled:(NSNumber *)animatorInitCalled
{
    objc_setAssociatedObject(self, &animatorInitCalledKey, animatorInitCalled, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSNumber *)animatorInitCalled
{
    return objc_getAssociatedObject(self, &animatorInitCalledKey);
}

- (void) setReferenceFrame:(NSString *)referenceFrame
{
    objc_setAssociatedObject(self, &referenceFrameKey, referenceFrame, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)referenceFrame
{
    return objc_getAssociatedObject(self, &referenceFrameKey);
}

- (void)setPushItem1:(NSArray *)pushItem1
{
    objc_setAssociatedObject(self, &pushItem1Key, pushItem1, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray *)pushItem1
{
    return objc_getAssociatedObject(self, &pushItem1Key);
}

- (void)setPushItem2:(NSArray *)pushItem2
{
    objc_setAssociatedObject(self, &pushItem2Key, pushItem2, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray *)pushItem2
{
    return objc_getAssociatedObject(self, &pushItem2Key);
}

- (void)setShouldIAddBehaviors:(NSNumber *)shouldIAddBehaviors
{
    objc_setAssociatedObject(self, &shouldIAddBehaviorsKey, shouldIAddBehaviors, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSNumber *)shouldIAddBehaviors
{
    return objc_getAssociatedObject(self, &shouldIAddBehaviorsKey);
}

- (void)setAnimator:(UIDynamicAnimator *)animator
{
    objc_setAssociatedObject(self, &animatorKey, animator, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (UIDynamicAnimator *)animator
{
    return objc_getAssociatedObject(self, &animatorKey);
}

@end
