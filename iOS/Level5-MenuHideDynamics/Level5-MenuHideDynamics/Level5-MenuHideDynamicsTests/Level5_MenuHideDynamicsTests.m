#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "CSPropertyDefinition.h"
#import "AppDelegate.h"
#import "AppDelegate+TestAdditions.h"
#import "CSPhotoDetailVC.h"
#import "LongPressOverlay.h"

@interface Level5_MenuHideDynamicsTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}

@end

@implementation Level5_MenuHideDynamicsTests

- (void)setUp
{
    [super setUp];
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _appDel.shouldIAddBehaviors = @YES;
    _appDel.animatorInitCalled = @NO;
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [(UITabBarController *)_appDel.window.rootViewController setSelectedIndex:0];
    [(UINavigationController *)[[(UITabBarController *)_appDel.window.rootViewController viewControllers] objectAtIndex:0] popToRootViewControllerAnimated:YES];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testDynamicAnimatorPropertyImplemented
{
    CSPropertyDefinition *property = [self animatorProperty];
    if(property) {
        [tester waitForViewWithAccessibilityLabel:@"cell"];
        [tester tapViewWithAccessibilityLabel:@"cell"];
        [tester waitForViewWithAccessibilityLabel:@"photoImageView"];
        [tester longPressViewWithAccessibilityLabel:@"photoImageView" duration:0.2f];
        [tester waitForTimeInterval:1.0f];
        _appDel.animatorInitCalled = @NO;
        _appDel.referenceFrame = nil;
        [tester tapScreenAtPoint:CGPointMake(200, 200)];
        [tester waitForTimeInterval:0.5f];
        
        XCTAssert([_appDel.animatorInitCalled intValue] > 0, @"Did not initialize the dynamic animator object");
        XCTAssert([self correctReferenceView], @"Did not use the containerView as the reference view for the dynamic animator");
    } else {
        XCTFail(@"You won't be able to attempt this challenge without a UIDynamicAnimator property.  There was one when you first downloaded this project, so you might want to delete this project and try again.");
    }
}

- (void)testPushBehaviors
{
    CSPropertyDefinition *property = [self animatorProperty];
    if(property)
    {
        [tester waitForViewWithAccessibilityLabel:@"cell"];
        [tester tapViewWithAccessibilityLabel:@"cell"];
        [tester waitForViewWithAccessibilityLabel:@"photoImageView"];

        UINavigationController *navController = (UINavigationController *)[[(UITabBarController *)_appDel.window.rootViewController viewControllers] objectAtIndex:0];
        CSPhotoDetailVC *photoDetailVC = (CSPhotoDetailVC *)navController.topViewController;
        
        [tester longPressViewWithAccessibilityLabel:@"photoImageView" duration:0.2f];
        [tester waitForTimeInterval:0.5f];
        
        // Reset the pushItems before tapping on the point
        _appDel.pushItem1 = @[];
        _appDel.pushItem2 = @[];
        
        [tester tapScreenAtPoint:CGPointMake(200, 200)];
        [tester waitForTimeInterval:1.0f];
        
        XCTAssert([_appDel.pushItem1[0] isEqual:photoDetailVC.overlay.mailButton], @"Did not set the first push behavior's item to the mailButton");
        XCTAssert([_appDel.pushItem1[1] integerValue] == 1, @"Did not set the first push behavior's mode to instantaneous");
        XCTAssert([_appDel.pushItem2[0] isEqual:photoDetailVC.overlay.editButton], @"Did not set the second push behavior's item to the editButton");
        XCTAssert([_appDel.pushItem2[1] integerValue] == 1, @"Did not set the second push behavior's mode to instantaneous");

        XCTAssert([photoDetailVC.animator.behaviors[0] pushDirection].dx < 0.0 && [photoDetailVC.animator.behaviors[0] pushDirection].dy > 0.0, @"Did not set the first push behavior's direction to down and the to left");
        
        XCTAssert([photoDetailVC.animator.behaviors[1] pushDirection].dx > 0.0 && [photoDetailVC.animator.behaviors[1] pushDirection].dy < 0.0, @"Did not set the second push behavior's direction to up and to the right");
    }
}

- (void)testOverlayRemovedAfterPush
{
    CSPropertyDefinition *property = [self animatorProperty];
    if(property)
    {
        [tester waitForViewWithAccessibilityLabel:@"cell"];
        [tester tapViewWithAccessibilityLabel:@"cell"];
        [tester waitForViewWithAccessibilityLabel:@"photoImageView"];
        
        UINavigationController *navController = (UINavigationController *)[[(UITabBarController *)_appDel.window.rootViewController viewControllers] objectAtIndex:0];
        CSPhotoDetailVC *photoDetailVC = (CSPhotoDetailVC *)navController.topViewController;
        
        [tester longPressViewWithAccessibilityLabel:@"photoImageView" duration:0.2f];
        [tester waitForTimeInterval:0.5f];
        
        XCTAssert(CGRectIntersectsRect(photoDetailVC.overlay.frame, photoDetailVC.overlay.mailButton.frame), @"mailButton should still be visible in the overlay, but it is not");
        XCTAssert(CGRectIntersectsRect(photoDetailVC.overlay.frame, photoDetailVC.overlay.editButton.frame), @"editButton should still be visible in the overlay, but it is not");
        
        [tester tapScreenAtPoint:CGPointMake(200, 200)];
        [tester waitForTimeInterval:1.0f];
        
        XCTAssert(!CGRectIntersectsRect(photoDetailVC.overlay.frame, photoDetailVC.overlay.mailButton.frame), @"mailButton's frame should not intersect with the overlay frame, but it does.");
        XCTAssert(!CGRectIntersectsRect(photoDetailVC.overlay.frame, photoDetailVC.overlay.editButton.frame), @"editButton's frame should not intersect with the overlay frame, but it does.");
        
        XCTAssert(![photoDetailVC.overlay isDescendantOfView:photoDetailVC.view],@"The overlay should be removed when the mailButton and editButton are no longer on screen, but they are off screen and the overlay is still visible.");
    }
}

- (CSPropertyDefinition *)animatorProperty
{
    CSPropertyDefinition *property;
    
    property = [CSPropertyDefinition firstPropertyWithType:@"UIDynamicAnimator" forClass:[CSPhotoDetailVC class]];
    
    return property;
}

- (BOOL)correctReferenceView
{
    BOOL threePointFive = [_appDel.referenceFrame isEqualToString:@"{{0, 0}, {320, 480}}"];
    BOOL four = [_appDel.referenceFrame isEqualToString:@"{{0, 0}, {320, 568}}"];
    return threePointFive || four;
}

@end
