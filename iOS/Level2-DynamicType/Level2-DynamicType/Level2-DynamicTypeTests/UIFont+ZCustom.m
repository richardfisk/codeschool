#import "UIFont+ZCustom.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation UIFont (ZCustom)

+ (void)load
{
    [CSSwizzler swizzleClass:[UIFont class]
          replaceClassMethod:@selector(fontWithDescriptor:size:)
                  withMethod:@selector(custom_fontWithDescriptor:size:)];
}

+ (UIFont *)custom_fontWithDescriptor:(UIFontDescriptor *)descriptor size:(CGFloat)pointSize
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSInteger newSize = [appDelegate.fontSizeAccumulator floatValue] + pointSize;
    appDelegate.fontSizeAccumulator = [NSNumber numberWithFloat:newSize];
    appDelegate.fontWithDescriptorCount = [NSNumber numberWithInteger:[appDelegate.fontWithDescriptorCount integerValue] + 1];
    return [self custom_fontWithDescriptor:descriptor size:pointSize];
}

@end
