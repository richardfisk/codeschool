#import "UIFontDescriptor+ZCustom.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation UIFontDescriptor (ZCustom)

+ (void)load
{
    [CSSwizzler swizzleClass:[UIFontDescriptor class]
          replaceClassMethod:@selector(preferredFontDescriptorWithTextStyle:)
                  withMethod:@selector(custom_preferredFontDescriptorWithTextStyle:)];
}

+ (UIFontDescriptor *)custom_preferredFontDescriptorWithTextStyle:(NSString *)style
{
    AppDelegate *_appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _appDel.preferredFontForStyleCount = [NSNumber numberWithInteger:[_appDel.contentSizeChangedCalled integerValue] + 1];
    return [self custom_preferredFontDescriptorWithTextStyle:style];
}

@end
