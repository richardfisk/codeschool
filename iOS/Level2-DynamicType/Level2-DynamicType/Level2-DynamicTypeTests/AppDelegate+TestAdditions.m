//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char contentSizeChangedCalledKey;
static char fontWithDescriptorCountKey;
static char preferredFontForStyleCountKey;
static char fontSizeAccumulatorKey;
static char needsLayoutCalledKey;

@implementation AppDelegate (TestAdditions)

- (void) setContentSizeChangedCalled:(NSNumber *)contentSizeChangedCalled
{
    objc_setAssociatedObject(self, &contentSizeChangedCalledKey, contentSizeChangedCalled, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)contentSizeChangedCalled
{
    return objc_getAssociatedObject(self, &contentSizeChangedCalledKey);
}


- (void) setFontWithDescriptorCount:(NSNumber *)fontWithDescriptorCount
{
    objc_setAssociatedObject(self, &fontWithDescriptorCountKey, fontWithDescriptorCount, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)fontWithDescriptorCount
{
    return objc_getAssociatedObject(self, &fontWithDescriptorCountKey);
}


- (void) setPreferredFontForStyleCount:(NSNumber *)preferredFontForStyleCount
{
    objc_setAssociatedObject(self, &preferredFontForStyleCountKey, preferredFontForStyleCount, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)preferredFontForStyleCount
{
    return objc_getAssociatedObject(self, &preferredFontForStyleCountKey);
}

- (void) setFontSizeAccumulator:(NSNumber *)fontSizeAccumulator
{
    objc_setAssociatedObject(self, &fontSizeAccumulatorKey, fontSizeAccumulator, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)fontSizeAccumulator
{
    return objc_getAssociatedObject(self, &fontSizeAccumulatorKey);
}

- (void)setNeedsLayoutCalled:(NSNumber *)needsLayoutCalled
{
    objc_setAssociatedObject(self, &needsLayoutCalledKey, needsLayoutCalled, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)needsLayoutCalled
{
    return objc_getAssociatedObject(self, &needsLayoutCalledKey);
}


@end
