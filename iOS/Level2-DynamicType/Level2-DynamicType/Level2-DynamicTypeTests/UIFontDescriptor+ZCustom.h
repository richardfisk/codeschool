#import <UIKit/UIKit.h>

@interface UIFontDescriptor (ZCustom)

+ (UIFontDescriptor *)custom_preferredFontDescriptorWithTextStyle:(NSString *)style;

@end
