#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "CSAboutVC.h"

#import "Photo.h"

#import <objc/runtime.h>

@interface Level2_DynamicTypeTests : CSRecordingTestCase {
    AppDelegate *_appDel;
    CSAboutVC *_aboutVC;
}

@end

@implementation Level2_DynamicTypeTests

- (void)setUp
{
    [super setUp];
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)tearDown
{
    _appDel.preferredFontForStyleCount = @0;
    _appDel.fontWithDescriptorCount = @0;
    [super tearDown];
}

- (void)testPreferredFontDescriptorWithTextStyle
{
    UITabBarController *tabBarController = (UITabBarController *)_appDel.window.rootViewController;
    UINavigationController *aboutNav = (UINavigationController *)tabBarController.viewControllers[2];
    _aboutVC = (CSAboutVC *)aboutNav.topViewController;
    NSInteger preferredFontForStyleCountBefore = [_appDel.preferredFontForStyleCount integerValue];
    if(preferredFontForStyleCountBefore == NSNotFound) {
        preferredFontForStyleCountBefore = 0;
    }
    [_aboutVC loadView];
    
    NSInteger preferredFontForStyleCountAfter = [_appDel.preferredFontForStyleCount integerValue];
    if(preferredFontForStyleCountBefore) {
        
    }
    if(preferredFontForStyleCountAfter) {
        
    }
    
    XCTAssert(preferredFontForStyleCountBefore < preferredFontForStyleCountAfter, @"Did not create fonts in loadView with preferredFontDescriptorWithTextStyle");
}

- (void)testFontResetInContentSizeChanged
{
    Method method = nil;
    SEL methodSelector = @selector(contentSizeChanged:);
    method = class_getInstanceMethod([CSAboutVC class], methodSelector);
    
    if(method) {
        UITabBarController *tabBarController = (UITabBarController *)_appDel.window.rootViewController;
        UINavigationController *aboutNav = (UINavigationController *)tabBarController.viewControllers[2];
        _aboutVC = (CSAboutVC *)aboutNav.topViewController;
        [_aboutVC.view layoutIfNeeded];
        
        NSInteger fontWithDescriptorCountBefore = [_appDel.fontWithDescriptorCount integerValue];
        NSInteger preferredFontForStyleCountBefore = [_appDel.preferredFontForStyleCount integerValue];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UIContentSizeCategoryDidChangeNotification" object:_aboutVC];
        
        XCTAssert([_appDel.contentSizeChangedCalled integerValue] == 1, @"Did not call contentSizeChanged: after the UIContentSizeCategoryDidChangeNotification was recevied");
        
        NSInteger fontWithDescriptorCountAfter = [_appDel.fontWithDescriptorCount integerValue];
        NSInteger preferredFontForStyleCountAfter = [_appDel.preferredFontForStyleCount integerValue];

        XCTAssert(fontWithDescriptorCountBefore < fontWithDescriptorCountAfter, @"Did not recreate the UIFontDescriptor in contentSizeChanged:");
        XCTAssert(preferredFontForStyleCountBefore < preferredFontForStyleCountAfter, @"Did not recreate the UIFont with preferredFontForTextStyle: in contentSizeChanged:");
        XCTAssert([_appDel.fontSizeAccumulator floatValue] == 60.0, @"Did not leave font sizes at 0");
        XCTAssert([_appDel.needsLayoutCalled integerValue] > 98, @"Did not send setNeedsLayout to self.view in the contentSizeChanged: method in CSAboutVC");
    } else {
        XCTFail(@"Did not implement the contentSizeChanged: method");
    }
}

@end
