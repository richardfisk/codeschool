//
//  UIView+ZCustom.h
//  Level2-DynamicType
//
//  Created by Jon Friskics on 9/19/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ZCustom)

- (void)custom_setNeedsLayout;

@end
