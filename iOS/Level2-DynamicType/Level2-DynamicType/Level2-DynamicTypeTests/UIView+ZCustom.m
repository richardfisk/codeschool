//
//  UIView+ZCustom.m
//  Level2-DynamicType
//
//  Created by Jon Friskics on 9/19/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIView+ZCustom.h"
#import "AppDelegate+TestAdditions.h"
#import "CSSwizzler.h"

@implementation UIView (ZCustom)

+ (void)load
{
    [CSSwizzler swizzleClass:[UIView class]
               replaceMethod:@selector(setNeedsLayout)
                  withMethod:@selector(custom_setNeedsLayout)];
}

- (void)custom_setNeedsLayout
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.needsLayoutCalled = [NSNumber numberWithInteger:[appDelegate.needsLayoutCalled integerValue] + 1];
    [self custom_setNeedsLayout];
}

@end
