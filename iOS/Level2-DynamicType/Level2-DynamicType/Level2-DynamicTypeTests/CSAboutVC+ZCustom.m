#import "CSAboutVC+ZCustom.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation CSAboutVC (ZCustom)

+ (void)load
{
    if([[[CSAboutVC alloc] init] respondsToSelector:@selector(contentSizeChanged:)]) {
        [CSSwizzler swizzleClass:[CSAboutVC class]
                   replaceMethod:@selector(contentSizeChanged:)
                      withMethod:@selector(custom_contentSizeChanged:)];
    }
}
- (void)custom_contentSizeChanged:(id)sender
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.contentSizeChangedCalled = @YES;
    [self custom_contentSizeChanged:sender];
}

@end
