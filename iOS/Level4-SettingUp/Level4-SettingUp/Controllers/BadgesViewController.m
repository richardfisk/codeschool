#import "BadgesViewController.h"
#import "BadgeDetailViewController.h"
#import "StatsViewController.h"
#import "SideTransition.h"
#import "SideDismissalTransition.h"

static NSString *const badgesURL = @"http://www.codeschool.com/users/rubymaverick.json";

@implementation BadgesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"rubymaverick's Badges";
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(displayStats:)];
        
        barButton.accessibilityLabel = @"Display Stats";
        
        self.navigationItem.rightBarButtonItem = barButton;
    }
    return self;
}

- (void) displayStats:(id)sender{
    
    StatsViewController *statsVC = [[StatsViewController alloc] init];
    statsVC.badges = _badges;
    
    statsVC.modalPresentationStyle = UIModalPresentationCustom;
    statsVC.transitioningDelegate = self;

    void (^completion)(void) = ^{
        UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleStatsGuesture:)];
        
        [statsVC.view addGestureRecognizer:recognizer];
    };
    
    [self presentViewController:statsVC animated:YES completion:completion];
}

- (void)handleStatsGuesture:(UIPanGestureRecognizer *)gesture
{
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        case UIGestureRecognizerStateChanged:{
            
        }
        case UIGestureRecognizerStateEnded:{
            
        }
        case UIGestureRecognizerStateCancelled:{
            
        }
        case UIGestureRecognizerStateFailed:{

        }
        case UIGestureRecognizerStatePossible:{
            
        }
    }
}

- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id <UIViewControllerAnimatedTransitioning>)animator
{
    UIPercentDrivenInteractiveTransition *transition = [[UIPercentDrivenInteractiveTransition alloc] init];
    
    return transition;
}

- (id <UIViewControllerAnimatedTransitioning>) animationControllerForPresentedController:(UIViewController *)presented
                                                                    presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    SideTransition *transition = [[SideTransition alloc] init];
    
    return transition;
}

- (id <UIViewControllerAnimatedTransitioning>) animationControllerForDismissedController:(UIViewController *)dismissed
{
    SideDismissalTransition *transition = [[SideDismissalTransition alloc] init];
    
    return transition;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *URL = [NSURL URLWithString:badgesURL];
    
    NSMutableURLRequest *request;
    request = [NSMutableURLRequest requestWithURL:URL];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];

    
    [NSURLConnection sendAsynchronousRequest:request
                    queue:[NSOperationQueue mainQueue]
        completionHandler:^(NSURLResponse *r, NSData *data, NSError *e) {
        
        self.badges = [NSJSONSerialization JSONObjectWithData:data
                                            options:0
                                              error:nil][@"badges"];
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _badges.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = self.badges[indexPath.row][@"name"];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *badge = _badges[indexPath.row];
    
    BadgeDetailViewController *badgeDetail = [[BadgeDetailViewController alloc] init];
    badgeDetail.badge = badge;
    
    [self.navigationController pushViewController:badgeDetail animated:YES];
}


@end
