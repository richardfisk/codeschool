//
//  BadgesViewController+OverridePresent.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIViewController+OverridePresent.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation UIViewController (OverridePresent)

+ (void) load
{
    [CSSwizzler swizzleClass:[UIViewController class]
               replaceMethod:@selector(presentViewController:animated:completion:)
                  withMethod:@selector(custom_presentViewController:animated:completion:)];
    
    [CSSwizzler swizzleClass:[UIViewController class] replaceMethod:@selector(dismissViewControllerAnimated:completion:) withMethod:@selector(custom_dismissViewControllerAnimated:completion:)];

}

- (void)custom_dismissViewControllerAnimated: (BOOL)flag completion: (void (^)(void))completion;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSNumber *dismissCount = appDel.dismissed;
    appDel.dismissed = @([dismissCount integerValue] + 1);

    
    [self custom_dismissViewControllerAnimated:flag completion:completion];
}

- (void)custom_presentViewController:(UIViewController *)viewControllerToPresent
                            animated:(BOOL)flag
                          completion:(void (^)(void))completion;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.presentedViewController = viewControllerToPresent;
    appDel.animated = @(flag);
    
    [self custom_presentViewController:viewControllerToPresent animated:flag completion:completion];
}
@end
