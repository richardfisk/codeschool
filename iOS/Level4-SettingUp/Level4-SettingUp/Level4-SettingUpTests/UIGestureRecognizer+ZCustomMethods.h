//
//  UIGestureRecognizer+ZCustomMethods.h
//  Level4-SettingUp
//
//  Created by Eric Allam on 9/17/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIGestureRecognizer (ZCustomMethods)
- (id)custom_initWithTarget:(id)target action:(SEL)action;
- (void)custom_addTarget:(id)target action:(SEL)action;
@end
