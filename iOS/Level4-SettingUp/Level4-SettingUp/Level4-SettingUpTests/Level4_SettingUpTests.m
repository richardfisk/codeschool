//
//  Level3_AnimatedTransitionTests.m
//  Level3-AnimatedTransitionTests
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIGestureRecognizer+ZCustomMethods.h"

@interface Level4_SettingUpTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}
@end

@implementation Level4_SettingUpTests

- (void)setUp
{
    [super setUp];

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // This is how we are only running the tapViewWithAccessibilityLabel call once instead
    // of before every since test method
    if (!_appDel.presentedViewController) {
        [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testGestureAddedOnPresentationCompletionBlock
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    // Test to make sure they add the gesture eventually
    [tester runBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
        
        KIFTestWaitCondition(statsVC.view.gestureRecognizers.count > 0, error, @"Did not add a gesture recognizer to the StatsViewController.view in the presentViewController completion block");
        
        return KIFTestStepResultSuccess;
    } timeout:3.0f];
    
    UIPanGestureRecognizer *gesture = [statsVC.view.gestureRecognizers firstObject];
    
    XCTAssert([gesture isMemberOfClass:[UIPanGestureRecognizer class]], @"Did not add a gesture recognizer to the StatsViewController.view in the presentViewController completion block");
    
    SEL gestureAction = NSSelectorFromString(_appDel.gestureAction);
    
    id gestureTarget = _appDel.gestureTarget;
    
    XCTAssert([gestureTarget respondsToSelector:gestureAction], @"Did not implement the method %@ on the gesture's target", NSStringFromSelector(gestureAction));
}

- (void) testGestureActionDismissesViewController
{
    [tester waitForViewWithAccessibilityLabel:@"Badge Stats"];
    [tester swipeViewWithAccessibilityLabel:@"Badge Stats" inDirection:KIFSwipeDirectionRight];
    
    XCTAssert([_appDel.dismissed isEqualToNumber:@1], @"Did not dismiss the view controller when the pan gesture begins");
}

- (void) testDismissalInteractiveDelegateMethod
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    id<UIViewControllerTransitioningDelegate> delegate = statsVC.transitioningDelegate;
    
    XCTAssert([delegate respondsToSelector:@selector(interactionControllerForDismissal:)], @"The transition delegate does not implement the interactionControllerForDismissal: method");
    
    if ([delegate respondsToSelector:@selector(interactionControllerForDismissal:)]) {
        
        id<UIViewControllerAnimatedTransitioning> animated = [delegate animationControllerForDismissedController:statsVC];
        id<UIViewControllerInteractiveTransitioning> transition = [delegate interactionControllerForDismissal:animated];
        
        XCTAssert([transition conformsToProtocol:@protocol(UIViewControllerInteractiveTransitioning) ], @"The object returned from interactionControllerForDismissal should conform to the UIViewControllerInteractiveTransitioning protocol.");
        
        XCTAssert([transition isKindOfClass:[UIPercentDrivenInteractiveTransition class]], @"The object returned from interactionControllerForDismissal should be an instance of UIPercentDrivenInteractiveTransition");
    }
}

@end
