//
//  UIGestureRecognizer+ZCustomMethods.m
//  Level4-SettingUp
//
//  Created by Eric Allam on 9/17/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIGestureRecognizer+ZCustomMethods.h"
#import "AppDelegate+TestAdditions.h"
#import "CSSwizzler.h"
#import "BadgesViewController.h"

@implementation UIGestureRecognizer (ZCustomMethods)

+ (void) load
{
    [CSSwizzler swizzleClass:[UIGestureRecognizer class] replaceMethod:@selector(initWithTarget:action:) withMethod:@selector(custom_initWithTarget:action:)];
    
    [CSSwizzler swizzleClass:[UIGestureRecognizer class] replaceMethod:@selector(addTarget:action:) withMethod:@selector(custom_addTarget:action:)];
}

- (id)custom_initWithTarget:(id)target action:(SEL)action;
{
    if ([target isMemberOfClass:[BadgesViewController class]]) {
        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDel.gestureAction = NSStringFromSelector(action);
        appDel.gestureTarget = target;
    }
    
    
    return [self custom_initWithTarget:target action:action];
}

- (void)custom_addTarget:(id)target action:(SEL)action;
{
    if ([target isMemberOfClass:[BadgesViewController class]]) {
        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDel.gestureAction = NSStringFromSelector(action);
        appDel.gestureTarget = target;
    }
    
    [self custom_addTarget:target action:action];
}
@end
