//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char presentedKey;
static char animatedKey;
static char dismissedKey;
static char gestureActionKey;
static char gestureTargetKey;

@implementation AppDelegate (TestAdditions)

- (id) gestureTarget
{
    return objc_getAssociatedObject(self, &gestureTargetKey);
}

- (void) setGestureTarget:(id)gestureTarget
{
    objc_setAssociatedObject(self, &gestureTargetKey, gestureTarget, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void) setGestureAction:(NSString *)gestureAction
{
    objc_setAssociatedObject(self, &gestureActionKey, gestureAction, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)gestureAction
{
    return objc_getAssociatedObject(self, &gestureActionKey);
}

- (void) setDismissed:(NSNumber *)dismissed
{
    objc_setAssociatedObject(self, &dismissedKey, dismissed, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)dismissed
{
    NSNumber *result = objc_getAssociatedObject(self, &dismissedKey);
    
    if (result) {
        return result;
    }else{
        return @0;
    }
}

- (void) setAnimated:(NSNumber *)animated
{
    objc_setAssociatedObject(self, &animatedKey, animated, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)animated
{
    return objc_getAssociatedObject(self, &animatedKey);
}

- (void) setPresentedViewController:(UIViewController *)presentedViewController
{
    objc_setAssociatedObject(self, &presentedKey, presentedViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentedViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentedKey);
}

@end
