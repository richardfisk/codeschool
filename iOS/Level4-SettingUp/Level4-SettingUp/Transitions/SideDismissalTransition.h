//
//  SideDismissalTransition.h
//  Level4-SettingUp
//
//  Created by Eric Allam on 9/16/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SideDismissalTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
