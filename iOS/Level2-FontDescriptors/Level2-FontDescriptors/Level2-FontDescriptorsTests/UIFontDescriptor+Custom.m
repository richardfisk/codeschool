//
//  UIFontDescriptor+Custom.m
//  Level2-FontDescriptors
//
//  Created by Jon Friskics on 9/19/13.
//  Copyright (c) 2013 Jon Friskics. All rights reserved.
//

#import "UIFontDescriptor+Custom.h"
#import "CSSwizzler.h"

@implementation UIFontDescriptor (Custom)

+ (void)load
{
}

- (UIFontDescriptor *)custom_fontDescriptorWithFamily:(NSString *)newFamily
{
    return [self custom_fontDescriptorWithFamily:newFamily];
}

@end
