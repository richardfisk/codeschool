//
//  UIFontDescriptor+Custom.h
//  Level2-FontDescriptors
//
//  Created by Jon Friskics on 9/19/13.
//  Copyright (c) 2013 Jon Friskics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFontDescriptor (Custom)

- (UIFontDescriptor *)custom_fontDescriptorWithFamily:(NSString *)newFamily;

@end
