//
//  UIFont+Custom.h
//  Level2-FontDescriptors
//
//  Created by Jon Friskics on 9/19/13.
//  Copyright (c) 2013 Jon Friskics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Custom)

+ (UIFont *)custom_fontWithName:(NSString *)fontName size:(CGFloat)fontSize;
+ (UIFont *)custom_fontWithDescriptor:(UIFontDescriptor *)descriptor size:(CGFloat)pointSize;

@end
