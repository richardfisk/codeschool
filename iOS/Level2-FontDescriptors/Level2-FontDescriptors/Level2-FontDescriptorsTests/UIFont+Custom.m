//
//  UIFont+Custom.m
//  Level2-FontDescriptors
//
//  Created by Jon Friskics on 9/19/13.
//  Copyright (c) 2013 Jon Friskics. All rights reserved.
//

#import "UIFont+Custom.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation UIFont (Custom)

+ (void)load
{
    [CSSwizzler swizzleClass:[UIFont class]
          replaceClassMethod:@selector(fontWithDescriptor:size:)
                  withMethod:@selector(custom_fontWithDescriptor:size:)];

    [CSSwizzler swizzleClass:[UIFont class]
          replaceClassMethod:@selector(fontWithName:size:)
                  withMethod:@selector(custom_fontWithName:size:)];
}

+ (UIFont *)custom_fontWithName:(NSString *)fontName size:(CGFloat)fontSize
{
    AppDelegate *appDel = [[UIApplication sharedApplication] delegate];
    appDel.fontWithNameCalled = [NSNumber numberWithInteger:[appDel.fontWithNameCalled intValue] + 1];
    return [self custom_fontWithName:fontName size:fontSize];
}

+ (UIFont *)custom_fontWithDescriptor:(UIFontDescriptor *)descriptor size:(CGFloat)pointSize
{
    AppDelegate *appDel = [[UIApplication sharedApplication] delegate];
    appDel.fontWithDescriptorCalled = [NSNumber numberWithInteger:[appDel.fontWithDescriptorCalled intValue] + 1];
    return [self custom_fontWithDescriptor:descriptor size:pointSize];
}

@end
