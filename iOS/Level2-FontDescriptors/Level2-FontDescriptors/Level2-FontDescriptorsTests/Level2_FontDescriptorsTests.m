#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "CSPhotoListVC.h"
#import "CSPhotoDetailVC.h"
#import "CSEditPhotoNote.h"
#import "CSAboutVC.h"
#import "CSPhotoCell.h"

#import "UIFont+Custom.h"
#import "CSSwizzler.h"

@interface Level2_FontDescriptorsTests : CSRecordingTestCase {
    AppDelegate *_appDel;
    CSAboutVC *_aboutVC;
    UINavigationController *_aboutVCNav;
}

@end

@implementation Level2_FontDescriptorsTests

- (void)setUp
{
    [super setUp];
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testFontDescriptorHeaderLabel
{
    CSAboutVC *aboutVC = [[CSAboutVC alloc] init];
    [aboutVC loadView];
    
    XCTAssert([aboutVC.headerLabel.font.fontDescriptor.fontAttributes[@"NSFontNameAttribute"] isEqualToString:@"Futura-CondensedExtraBold"], @"Did not use font descriptors to set the headerLabel font to Futura-CondensedExtraBold");
    XCTAssert([aboutVC.headerLabel.font.fontDescriptor.fontAttributes[@"NSFontSizeAttribute"] floatValue] == 40.0f, @"Did not use font descriptors to set the headerLabel font to 40.0f");
}

- (void)testFontDescriptorAuthorLabel
{
    CSAboutVC *aboutVC = [[CSAboutVC alloc] init];
    [aboutVC loadView];
    
    XCTAssert([aboutVC.authorLabel.font.fontDescriptor.fontAttributes[@"NSFontNameAttribute"] isEqualToString:@"HelveticaNeue"], @"Did not use font descriptors to set the authorLabel font to Helvetica Neue");
    XCTAssert([aboutVC.authorLabel.font.fontDescriptor.fontAttributes[@"NSFontSizeAttribute"] floatValue] == 20.0f, @"Did not use font descriptors to set the authorLabel font to 20.0f");
}

- (void)testFontDescriptorDescriptionTextView
{
    CSAboutVC *aboutVC = [[CSAboutVC alloc] init];
    [aboutVC loadView];
    
    XCTAssert([aboutVC.descriptionTextView.font.fontDescriptor.fontAttributes[@"NSFontNameAttribute"] isEqualToString:@"HelveticaNeue"], @"Did not use font descriptors to set the descriptionTextView font to Helvetica Neue");
    XCTAssert([aboutVC.descriptionTextView.font.fontDescriptor.fontAttributes[@"NSFontSizeAttribute"] floatValue] == 15.0f, @"Did not use font descriptors to set the descriptionTextView font to 15.0f");
}

- (void)testFontDescriptorCSInfoTextView
{
    CSAboutVC *aboutVC = [[CSAboutVC alloc] init];
    [aboutVC loadView];
    
    XCTAssert([aboutVC.csInfoTextView.font.fontDescriptor.fontAttributes[@"NSFontNameAttribute"] isEqualToString:@"HelveticaNeue"], @"Did not use font descriptors to set the csInfoTextView font to Helvetica Neue");
    XCTAssert([aboutVC.csInfoTextView.font.fontDescriptor.fontAttributes[@"NSFontSizeAttribute"] floatValue] == 15.0f, @"Did not use font descriptors to set the csInfoTextView font to 15.0f");
}

- (void)testFontDescriptorsInAboutVC
{
    CSAboutVC *aboutVC = [[CSAboutVC alloc] init];
    [aboutVC loadView];
    
    XCTAssert([_appDel.fontWithNameCalled integerValue] == 6, @"Should not be using fontWithName:size: to set fonts - instead use UIFontDescriptors");
    XCTAssert([_appDel.fontWithDescriptorCalled integerValue] % 4 == 0, @"Did not use fontWithDescriptor:size: to set the fonts for the 2 labels and 2 text views in CSAboutVC");
}

@end
