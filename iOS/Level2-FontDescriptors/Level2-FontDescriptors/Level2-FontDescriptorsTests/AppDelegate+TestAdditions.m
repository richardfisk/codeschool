//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char fontWithNameCalledKey;
static char fontWithDescriptorCalledKey;

@implementation AppDelegate (TestAdditions)

- (void) setFontWithNameCalled:(NSNumber *)fontWithNameCalled
{
    objc_setAssociatedObject(self, &fontWithNameCalledKey, fontWithNameCalled, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)fontWithNameCalled
{
    return objc_getAssociatedObject(self, &fontWithNameCalledKey);
}

- (void) setFontWithDescriptorCalled:(NSNumber *)fontWithDescriptorCalled
{
    objc_setAssociatedObject(self, &fontWithDescriptorCalledKey, fontWithDescriptorCalled, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)fontWithDescriptorCalled
{
    return objc_getAssociatedObject(self, &fontWithDescriptorCalledKey);
}

@end
