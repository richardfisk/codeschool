#import "CSVideosViewController.h"
#import "CSVideoViewController.h"
#import "CSTaskCustomCell.h"

@interface CSVideosViewController () <UIViewControllerRestoration>
@property (strong, nonatomic) NSNumber *pathID;
@property (strong, nonatomic) NSArray *videos;
@property (strong, nonatomic) NSURLSession *imageSession;
@end

static NSString * const CSVideosURL = @"http://codeschoolmockapi.herokuapp.com/api/app/v1/paths/%@/videos";
static NSString * const CSVideoCellIdentifier = @"com.CodeSchool.VideoCell";

@implementation CSVideosViewController

- (instancetype)initWithPathID:(NSNumber *)pathID
{
    self = [super init];
    
    if (self) {
        self.pathID = pathID;
        
        [self.tableView registerClass:[CSTaskCustomCell class] forCellReuseIdentifier:CSVideoCellIdentifier];
        
        // TODO: Set the restoration identifier and class
        self.restorationIdentifier = @"CSVideosViewController";
        self.restorationClass = [self class];
        NSURLSessionConfiguration *imageConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.imageSession = [NSURLSession sessionWithConfiguration:imageConfig];
    }
    
    return self;
}

#pragma mark - State Restoration methods

// TODO: Implement the two state restoration methods to save and restore this application with
//       the data it needs
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:self.pathID forKey:@"pathID"];
    [super encodeRestorableStateWithCoder:coder];
}

+ (UIViewController *)viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
    UIViewController *viewController = nil;
    NSNumber *pathID = [coder decodeObjectForKey:@"pathID"];
    if (pathID) {
        viewController = [[CSVideosViewController alloc]
                        initWithPathID:pathID];
    }
return viewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSString *CSVideosURLWithPathID = [NSString stringWithFormat:CSVideosURL, self.pathID];
    
    NSURLSessionDataTask *task = [session dataTaskWithURL:[NSURL URLWithString:CSVideosURLWithPathID]
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          self.videos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
          
          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
              [self.tableView reloadData];
          }];
          
      }];
    
    [task resume];
    
    [session finishTasksAndInvalidate];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.videos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSTaskCustomCell *cell = [self setupCellForIndexPath:indexPath];
    
    NSDictionary *video = self.videos[indexPath.row];
    
    cell.textLabel.text = video[@"title"];
    cell.detailTextLabel.text = video[@"duration"];
    cell.imageView.image = [UIImage imageNamed:@"VideoPlaceholder"];
    
    if (cell.task) {
        [cell.task cancel];
    }
    
    cell.task = [self.imageSession dataTaskWithURL:[NSURL URLWithString:video[@"thumbnail_url"]]
                                                  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          if (!error) {
              [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                  cell.imageView.image = [UIImage imageWithData:data];
              }];
          }
      }];
    
    [cell.task resume];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{    
    NSDictionary *video = self.videos[indexPath.row];
    
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:video];
    
    [self.navigationController pushViewController:videoVC animated:YES];
}

- (CSTaskCustomCell *)setupCellForIndexPath:(NSIndexPath *)indexPath
{
    CSTaskCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CSVideoCellIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
