#import "CSMoreInfoViewController.h"

@interface CSMoreInfoViewController ()
@property (strong, nonatomic) NSURL *webURL;
@end

@implementation CSMoreInfoViewController

- (instancetype) initWithURL:(NSURL *)webURL;
{
    self = [super init];
    if (self) {
        self.webURL = webURL;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)viewDidAppear:(BOOL)animated;
{
    NSURLRequest *request = [NSURLRequest requestWithURL:self.webURL];
    
    [self.webView loadRequest:request];
}

- (void)close:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
