#import "AppDelegate.h"
#import "CSPathsViewController.h"
#import "CSVideoDownloader.h"

@implementation AppDelegate

- (id)init
{
    self = [super init];
    
    return self;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.tintColor = [UIColor colorWithRed:167/255.0 green:109/255.0 blue:58/255.0 alpha:1.0];
    
    CSPathsViewController *pathsVC = [CSPathsViewController new];
    
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:pathsVC];
    self.window.rootViewController = navVC;
    
    // TODO: make the rootViewController opt-in to the state restoration system
    self.window.rootViewController.restorationIdentifier = @"RootViewController";
    self.window.rootViewController.restorationClass = [self.window.rootViewController class];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    self.backgroundCompletionHandler = completionHandler;
    
    [CSVideoDownloader sharedDownloader];
}

// TODO: opt-in to the State Restoration system
- (BOOL) application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}
- (BOOL) application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}
@end
