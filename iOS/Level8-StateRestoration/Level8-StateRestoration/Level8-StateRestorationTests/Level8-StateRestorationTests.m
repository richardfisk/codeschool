//
//  Level8-StateRestorationTests.m
//  Level8-StateRestorationTests
//
//  Created by Eric Allam on 10/22/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

// Test imports
#import "CSRecordingTestCase.h"
#import "CSPropertyDefinition.h"
#import "AppDelegate+TestAdditions.h"

// App imports
#import "CSVideoViewController.h"
#import "CSVideoDownloader.h"
#import "CSPathsViewController.h"
#import "CSVideosViewController.h"

@interface Level8_StateRestorationTests : CSRecordingTestCase
@property (strong, nonatomic) AppDelegate *appDel;
@end

@implementation Level8_StateRestorationTests

- (void)setUp
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    });
}

-(void)testAppDelegateAdoption
{
    NSCoder *coder = [NSCoder new];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    });
    if ([_appDel respondsToSelector:@selector(application:shouldSaveApplicationState:)]) {
        
        XCTAssert([_appDel application:[UIApplication sharedApplication] shouldSaveApplicationState:coder], @"Did not return YES from the application:shouldSaveApplicationState: method");
        
    }else{
        XCTFail(@"Did not opt-in to the State Restoration system in the AppDelegate");
    }
    
    if ([_appDel respondsToSelector:@selector(application:shouldRestoreApplicationState:)]) {
        XCTAssert([_appDel application:[UIApplication sharedApplication] shouldRestoreApplicationState:coder], @"Did not return YES from the application:shouldRestoreApplicationState: method");
    }else{
        XCTFail(@"Did not opt-in to the State Restoration system in the AppDelegate");
    }
}

-(void)testRestorationIdentifiers
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    });
    NSLog(@"%@", _appDel.window.rootViewController.restorationIdentifier);
    XCTAssert(_appDel.window.rootViewController.restorationIdentifier, @"Did not set a restorationIdentifier on the window's rootViewController");
    
    CSPathsViewController *pathsVC = [CSPathsViewController new];
    
    XCTAssert(pathsVC.restorationIdentifier, @"Did not set a restorationIdentifier for instances of CSPathsViewController");
    
}

-(void)testVideosViewControllerStateRestoration
{
    CSVideosViewController *videosVC = [[CSVideosViewController alloc] initWithPathID:@3];
    
    XCTAssert(videosVC.restorationIdentifier, @"Did not set a restorationIdentifier for instances of CSVideosViewController");
    
    XCTAssertEqualObjects(videosVC.restorationClass, [videosVC class], @"Did not set a restorationClass for instances of CSVideosViewController");
    
    XCTAssert([[videosVC class] conformsToProtocol:@protocol(UIViewControllerRestoration)], @"The CSVideosViewController does not conform to the UIViewControllerRestoration protocol");
    
    NSMutableData *data = [NSMutableData data];
    
    NSKeyedArchiver *coder = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    
    [videosVC encodeRestorableStateWithCoder:coder];
    
    [coder finishEncoding];
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    
    Class<UIViewControllerRestoration> csVideosClass = NSClassFromString(@"CSVideosViewController");
    
    UIViewController *restoredVC = [csVideosClass viewControllerWithRestorationIdentifierPath:@[] coder:unarchiver];
    
    XCTAssert(restoredVC, @"Did not implement the viewControllerWithRestorationIdentifierPath:coder: method in CSVideosViewController to return an instance with the pathID decoded from the coder passed in");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    XCTAssert([restoredVC performSelector:sel_registerName("pathID")], @"The restored CSVideosViewController instance does not have the pathID set");
#pragma clang diagnostic pop
    
}

-(void)testVideoViewControllerStateRestoration
{
    NSDictionary *videoInfo = @{
        @"badge_url": @"https://d1ffx7ull4987f.cloudfront.net/images/achievements/large_badge/307/level-1-on-core-ios-7-45046cadb933cc707038d10dccb28110.png",
        @"course": @"Core iOS 7",
        @"description": @"Learn the most common problems you'll run into when upgrading your app for iOS 7.",
        @"download_url": @"http://hls.codeschool.com.s3.amazonaws.com/2a201cc5249eef3dda4f813d678de123_1.jpg",
        @"duration": @"4:31",
        @"highres_thumbnail_url": @"http://hls.codeschool.com.s3.amazonaws.com/2a201cc5249eef3dda4f813d678de123_1.jpg",
        @"id": @4,
        @"more_url": @"https://www.codeschool.com/courses/core-ios-7",
        @"streaming_url": @"http://hls.codeschool.com.s3.amazonaws.com/c27342f3979688fadbc7663f90035390.m3u8",
        @"thumbnail_url": @"http://hls.codeschool.com.s3.amazonaws.com/2e11422f3cd52020414ba002463cc801_1.jpg",
        @"title": @"Updating from iOS 6",
    };
    
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:videoInfo];
    
    XCTAssert(videoVC.restorationIdentifier, @"Did not set a restorationIdentifier for instances of CSVideoViewController");
    
    XCTAssertEqualObjects(videoVC.restorationClass, [videoVC class], @"Did not set a restorationClass for instances of CSVideoViewController");
    
    XCTAssert([[videoVC class] conformsToProtocol:@protocol(UIViewControllerRestoration)], @"The CSVideoViewController does not conform to the UIViewControllerRestoration protocol");
    
    NSMutableData *data = [NSMutableData data];
    
    NSKeyedArchiver *coder = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    
    [videoVC encodeRestorableStateWithCoder:coder];
    
    [coder finishEncoding];
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    
    Class<UIViewControllerRestoration> csVideoClass = NSClassFromString(@"CSVideoViewController");
    
    UIViewController *restoredVC = [csVideoClass viewControllerWithRestorationIdentifierPath:@[] coder:unarchiver];
    
    XCTAssert(restoredVC, @"Did not implement the viewControllerWithRestorationIdentifierPath:coder: method in CSVideoViewController to return an instance with the videoInfo decoded from the coder passed in");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    XCTAssert([restoredVC performSelector:sel_registerName("video")], @"The restored CSVideoViewController instance does not have the videoInfo");
#pragma clang diagnostic pop
}

@end
