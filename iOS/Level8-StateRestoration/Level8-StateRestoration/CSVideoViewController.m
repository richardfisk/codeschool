#import "CSVideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "CSMoreInfoViewController.h"
#import "CSMoreInfoTransitionDelegate.h"
#import "CSNoticeDownloadView.h"
#import "CSVideoDownloader.h"

@interface CSVideoViewController () <UIViewControllerRestoration>
@property (strong, nonatomic) NSDictionary *video;
@property (strong, nonatomic) CSMoreInfoTransitionDelegate *transitionDelegate;
@property (strong, nonatomic) CSNoticeDownloadView *noticeView;
@end

@implementation CSVideoViewController

#pragma mark - ViewController methods

- (instancetype) initWithVideoInfo:(NSDictionary *)video;
{
    self = [super init];
    
    if (self) {        
        self.video = video;
        
        // TODO: Set the restoration identifier and class
        self.restorationIdentifier = @"CSVideoViewController";
        self.restorationClass = [self class];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Handle the Dynamic Type change
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contentSizeChanged:) name:@"UIContentSizeCategoryDidChangeNotification" object:nil];
    
    self.titleLabel.text = self.video[@"title"];
    self.detailsView.text = self.video[@"description"];
    self.durationLabel.text = self.video[@"duration"];
    
    self.downloadButton.possibleTitles = [NSSet setWithArray:@[@"Download Video", @"Watch Download"]];
    self.noticeView = [CSNoticeDownloadView noticeView];
    
    [self configureDownloadButton:self.isVideoDownloaded];
    
    // TODO replace this with using the global shared NSURLSession
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:self.video[@"highres_thumbnail_url"]]
                                 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                     
                                     [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                         self.thumbnailView.image = [UIImage imageWithData:data];
                                     }];
                                 }] resume];
}

#pragma mark - State Restoration methods

// TODO: Implement the two state restoration methods to save and restore this application with
//       the data it needs

//+ (UIViewController *) viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
//{
//    
//}
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:self.video forKey:@"video"];
    [super encodeRestorableStateWithCoder:coder];
}

+ (UIViewController *)viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
    UIViewController *viewController = nil;
    NSDictionary *video = [coder decodeObjectForKey:@"video"];
    if (video) {
        viewController = [[CSVideoViewController alloc]
                          initWithVideoInfo:video];
    }
    return viewController;
}
#pragma mark - Video methods

- (IBAction)startDownload:(id)sender
{
    NSURL *URL = [NSURL URLWithString:self.video[@"download_url"]];
    
    NSURLSessionDownloadTask *task = [[CSVideoDownloader sharedDownloader] downloadTaskWithURL:URL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProgress:)
                                                 name:CSVideoProgressNotification
                                               object:task];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishDownload:)
                                                 name:CSVideoCompletionNotification
                                               object:task];
    
    [task resume];
}

- (void)updateProgress:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        self.progressView.progress = [[notification userInfo][CSVideoProgressUserInfoKey] floatValue];
    }];
}

- (void)finishDownload:(NSNotification *)notification
{
    if ([notification.userInfo[CSVideoURLUserInfoKey] isEqualToString:self.video[@"download_url"]]) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self updateViewsForFinishedDownload];
        }];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)playVideo:(id)sender
{
    NSURL *contentURL = [NSURL URLWithString:self.video[@"streaming_url"]];
    
    MPMoviePlayerViewController *movieVC = [[MPMoviePlayerViewController alloc] initWithContentURL:contentURL];
    
    [self presentMoviePlayerViewControllerAnimated:movieVC];
}

- (void)watchDownload:(id)sender
{
    NSURL *contentURL = [NSURL fileURLWithPath:[self downloadedVideoPath]];
    
    MPMoviePlayerViewController *movieVC = [[MPMoviePlayerViewController alloc] initWithContentURL:contentURL];
    
    [self presentMoviePlayerViewControllerAnimated:movieVC];
}

- (NSString *)downloadedVideoPath
{
    NSString *filePath = [self generateVideoPath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        return filePath;
    }else{
        return nil;
    }
}

- (BOOL)isVideoDownloaded
{
    if (self.downloadedVideoPath) {
        return YES;
    }else{
        return NO;
    }
}

- (void)configureDownloadButton:(BOOL)downloaded
{
    if (downloaded) {
        self.downloadButton.title = @"Watch Download";
        self.downloadButton.action = @selector(watchDownload:);
    }else{
        self.downloadButton.title = @"Download Video";
        self.downloadButton.action = @selector(startDownload:);
    }
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    self.detailsHeightConstraint.constant = [self textViewHeight:self.detailsView];
}

#pragma mark - Dynamic Type

-(void)contentSizeChanged:(NSNotification *)note
{
    UIFontDescriptor *baseFont = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleCaption1];
    
    UIFont *detailsFont = [UIFont fontWithDescriptor:baseFont size:0];
    
    self.detailsView.font = detailsFont;
    
    [self.view setNeedsUpdateConstraints];
}

#pragma mark - More Info methods

- (IBAction)moreInfo:(id)sender;
{
    NSURL *webURL = [NSURL URLWithString:self.video[@"more_url"]];
    
    CSMoreInfoViewController *moreVC = [[CSMoreInfoViewController alloc]
                                           initWithURL:webURL];
    
    self.transitionDelegate = [CSMoreInfoTransitionDelegate new];
    
    moreVC.modalPresentationStyle = UIModalPresentationCustom;
    moreVC.transitioningDelegate = self.transitionDelegate;
    
    [self presentViewController:moreVC animated:YES completion:nil];
}

#pragma mark - Helper methods

- (CGFloat)textViewHeight:(UITextView *)textView
{
    [textView.layoutManager ensureLayoutForTextContainer:textView.textContainer];
    CGRect usedRect = [textView.layoutManager usedRectForTextContainer:textView.textContainer];
    return ceilf(usedRect.size.height +
                 textView.textContainerInset.top +
                 textView.textContainerInset.bottom);
}

- (NSString *)generateVideoPath
{
    return [self buildDownloadPath:[NSURL URLWithString:self.video[@"download_url"]]];
}

- (NSString *)buildDownloadPath:(NSURL *)videoURL
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths[0] stringByAppendingPathComponent:@"Videos"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:docsPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSString *newFileName = videoURL.lastPathComponent;
    
    return [docsPath stringByAppendingPathComponent:newFileName];
}

- (void)showDownloadNotice;
{
    [self.noticeView displayNotice];
}

#pragma mark - Methods for updating the UI

- (void)updateViewsForFinishedDownload
{
    [self configureDownloadButton:YES];
    [self showDownloadNotice];
}


@end
