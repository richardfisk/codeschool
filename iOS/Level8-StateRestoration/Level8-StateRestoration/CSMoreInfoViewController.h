#import <UIKit/UIKit.h>

@interface CSMoreInfoViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)close:(id)sender;

- (instancetype) initWithURL:(NSURL *)webURL;

@end
