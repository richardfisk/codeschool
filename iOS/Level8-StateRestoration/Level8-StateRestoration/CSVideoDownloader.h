#import <Foundation/Foundation.h>

@interface CSVideoDownloader : NSObject
+ (instancetype) sharedDownloader;
- (NSURLSessionDownloadTask *)downloadTaskWithURL:(NSURL *)URL;

- (NSString *)downloadedVideoPathForURL:(NSURL *)url;
@end

extern NSString *const CSVideoProgressNotification;
extern NSString *const CSVideoCompletionNotification;
extern NSString *const CSVideoProgressUserInfoKey;
extern NSString *const CSVideoURLUserInfoKey;
