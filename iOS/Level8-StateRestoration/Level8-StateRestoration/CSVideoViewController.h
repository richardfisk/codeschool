#import <UIKit/UIKit.h>

@interface CSVideoViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *durationLabel;
@property (nonatomic, strong) IBOutlet UIImageView *thumbnailView;
@property (nonatomic, strong) IBOutlet UITextView *detailsView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *downloadButton;
@property (nonatomic, strong) IBOutlet UIProgressView *progressView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *detailsHeightConstraint;

- (IBAction)startDownload:(id)sender;
- (IBAction)playVideo:(id)sender;
- (IBAction)moreInfo:(id)sender;

- (instancetype) initWithVideoInfo:(NSDictionary *)video;
@end
