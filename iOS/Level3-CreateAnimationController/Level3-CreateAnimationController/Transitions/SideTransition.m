#import "SideTransition.h"

@implementation SideTransition

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 1.0f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    UIView *transitionViewContainerView = [transitionContext containerView];
    [transitionViewContainerView addSubview:fromViewController.view];
    [transitionViewContainerView addSubview:toViewController.view];
 
    CGRect fullFrame = [transitionContext initialFrameForViewController:fromViewController];
    NSLog(@"ff %@", NSStringFromCGRect(fullFrame));
    [self animateToView:toViewController.view fromView:fromViewController.view transitionContext:transitionContext fullFrame:fullFrame];
}

- (void)animateToView:(UIView *)toView fromView:(UIView *)fromView transitionContext:(id <UIViewControllerContextTransitioning>)transitionContext fullFrame:(CGRect)fullFrame
{
    void (^completion)(BOOL) = ^(BOOL finished) {
        [transitionContext completeTransition:YES];
    };
    
    CGFloat paddingX = 10.0f;
    CGFloat paddingY = 10.0f;
    
    toView.frame = CGRectMake( CGRectGetWidth(fullFrame), paddingY, CGRectGetWidth(fullFrame) - 2 * paddingX, CGRectGetHeight(fullFrame) - 2 * paddingY);
    NSLog(@"starting%@", toView);
    
    void (^animations)(void) = ^{
        CGRect insetFrame = CGRectMake(paddingX, paddingY, CGRectGetWidth(toView.frame) - 2 * paddingX, CGRectGetHeight(fromView.frame) - 2 * paddingY);
        toView.frame = insetFrame;
//        NSLog(@"ending %@ %@", toView, NSStringFromCGRect(insetFrame));
    };
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0f usingSpringWithDamping:2.2f initialSpringVelocity:3.5f options:UIViewAnimationOptionCurveEaseInOut animations:animations completion:completion];
}

@end
