#import "StatsViewController.h"

@implementation StatsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.earnedBadgeLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.badges.count];
    
    // If this controller is being shown in a custom presentation style,
    // give it a border
    if (self.modalPresentationStyle == UIModalPresentationCustom) {
        self.view.layer.borderColor = [UIColor grayColor].CGColor;
        self.view.layer.borderWidth = 2.0f;
    }
}

- (IBAction) goodJob:(id)sender;
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
