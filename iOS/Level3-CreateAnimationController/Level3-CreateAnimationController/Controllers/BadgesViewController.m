#import "BadgesViewController.h"
#import "BadgeDetailViewController.h"
#import "StatsViewController.h"
#import "SideTransition.h"

static NSString *const badgesURL = @"http://www.codeschool.com/users/rubymaverick.json";

@implementation BadgesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"rubymaverick's Badges";
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(displayStats:)];
        
        barButton.accessibilityLabel = @"Display Stats";
        
        self.navigationItem.rightBarButtonItem = barButton;
    }
    return self;
}

// Present the StatsViewController as a modal
- (void) displayStats:(id)sender{
    
    StatsViewController *statsVC = [[StatsViewController alloc] init];
    statsVC.badges = _badges;
    
    statsVC.modalPresentationStyle = UIModalPresentationCustom;
    statsVC.transitioningDelegate = self;
    
    [self presentViewController:statsVC animated:YES completion:nil];
}

// TODO: implement the transitioning delegate method that controls the custom presentation animation
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return [[SideTransition alloc] init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *URL = [NSURL URLWithString:badgesURL];
    
    NSMutableURLRequest *request;
    request = [NSMutableURLRequest requestWithURL:URL];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];

    
    [NSURLConnection sendAsynchronousRequest:request
                    queue:[NSOperationQueue mainQueue]
        completionHandler:^(NSURLResponse *r, NSData *data, NSError *e) {
        
        self.badges = [NSJSONSerialization JSONObjectWithData:data
                                            options:0
                                              error:nil][@"badges"];
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _badges.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = self.badges[indexPath.row][@"name"];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *badge = _badges[indexPath.row];
    
    BadgeDetailViewController *badgeDetail = [[BadgeDetailViewController alloc] init];
    badgeDetail.badge = badge;
    
    [self.navigationController pushViewController:badgeDetail animated:YES];
}


@end
