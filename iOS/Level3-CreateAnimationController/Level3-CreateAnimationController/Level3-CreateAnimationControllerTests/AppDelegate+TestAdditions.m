//
//  AppDelegate+TestAdditions.m
//  Level3-CreateAnimationController
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char presentedKey;
static char animatedKey;
static char beforeTransitionKey;

@implementation AppDelegate (TestAdditions)

- (void) setBeforeTransition:(void (^)(void))beforeTransition
{
    objc_setAssociatedObject(self, &beforeTransitionKey, beforeTransition, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(void)) beforeTransition
{
    return objc_getAssociatedObject(self, &beforeTransitionKey);
}

- (void) setAnimated:(NSNumber *)animated
{
    objc_setAssociatedObject(self, &animatedKey, animated, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)animated
{
    return objc_getAssociatedObject(self, &animatedKey);
}

- (void) setPresentedViewController:(UIViewController *)presentedViewController
{
    objc_setAssociatedObject(self, &presentedKey, presentedViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentedViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentedKey);
}

@end
