//
//  UIView+ZCustomSpringAnimation.m
//  Level3-CreateAnimationController
//
//  Created by Eric Allam on 9/15/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIView+ZCustomSpringAnimation.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation UIView (ZCustomSpringAnimation)

+ (void) load
{
    [CSSwizzler swizzleClass:[UIView class] replaceClassMethod:@selector(animateWithDuration:delay:usingSpringWithDamping:initialSpringVelocity:options:animations:completion:) withMethod:@selector(custom_animateWithDuration:delay:usingSpringWithDamping:initialSpringVelocity:options:animations:completion:)];
}

+ (void)custom_animateWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay usingSpringWithDamping:(CGFloat)dampingRatio initialSpringVelocity:(CGFloat)velocity options:(UIViewAnimationOptions)options animations:(void (^)(void))animations completion:(void (^)(BOOL finished))completion;
{
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDel.beforeTransition) {
        appDel.beforeTransition();
    }

    [self custom_animateWithDuration:duration delay:delay usingSpringWithDamping:dampingRatio initialSpringVelocity:velocity options:options animations:animations completion:completion];
}

@end
