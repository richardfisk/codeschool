//
//  Level3_AnimatedTransitionTests.m
//  Level3-CreateAnimationControllerTests
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SideTransition.h"

@interface Level3_CreateAnimationControllerTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}
@end

@implementation Level3_CreateAnimationControllerTests

- (void)setUp
{
    [super setUp];

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // This is how we are only running the tapViewWithAccessibilityLabel call once instead
    // of before every since test method
    if (!_appDel.presentedViewController) {
        [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAdoptingProtocol
{
    SideTransition *transition = [[SideTransition alloc] init];
    
    XCTAssert([transition conformsToProtocol:@protocol(UIViewControllerAnimatedTransitioning)]);
}

- (void) testTransitioningDelegate
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    if ([self hasImplementedDelegateMethod]) {
        id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate
                                                                 animationControllerForPresentedController:statsVC
                                                                 presentingController:_appDel.window.rootViewController sourceController:_appDel.window.rootViewController];
        
        XCTAssertTrue([transition conformsToProtocol:@protocol(UIViewControllerAnimatedTransitioning)], @"The object returned from animationControllerForPresentedController:presentingController:sourceController: does not conform to the UIViewControllerAnimatedTransitioning protocol");
    }else{
        XCTFail(@"The transitioning delegate does not implement animationControllerForPresentedController:presentingController:sourceController:");
    }
}

- (void)testTransitionDuration
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    
    if ([self hasImplementedDelegateMethod]) {
        id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate
                                                                 animationControllerForPresentedController:statsVC
                                                                 presentingController:badgesVC sourceController:badgesVC];
        
        MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:badgesVC toController:statsVC];
        
        NSTimeInterval animationInterval = [transition transitionDuration:context];
        
        XCTAssertEqualWithAccuracy(animationInterval, 1.0f, 0.5, @"Did not return a reasonable animation time, should be between 0.5 seconds and 1.5 seconds");
    }else{
        XCTFail(@"The transitioning delegate does not implement animationControllerForPresentedController:presentingController:sourceController:");
    }

}

- (void)testAnimatedTransition
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    
    if ([self hasImplementedDelegateMethod]) {
        id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate
                                                                 animationControllerForPresentedController:statsVC
                                                                 presentingController:badgesVC sourceController:badgesVC];
        
        MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:badgesVC toController:statsVC];
        
        __block CGRect beforeFrame;
        
        _appDel.beforeTransition = ^{
            beforeFrame = statsVC.view.frame;
        };
        
        [transition animateTransition:context];
        
        CGRect finalFrame = statsVC.view.frame;
        
        UIView *containerView = [context containerView];
        
        XCTAssert(beforeFrame.origin.x >= 320, @"Did not set the 'to' view initial position to be off the right side of the screen. Make sure the frame's origin.x is 320 or more.");
        
        XCTAssertEqual(beforeFrame.origin.y, finalFrame.origin.y, @"The initial origin.y and the final origin.y should be the same value");
        
        XCTAssert(finalFrame.origin.y > 0, @"The final origin.y should be more than 0 so the StatsViewController is shown smaller than the full frame");
        
        XCTAssert(finalFrame.origin.x > 0, @"The final origin.x should be more than 0 so the StatsViewController is shown smaller than the full frame");
        
        CGRect windowFrame = [[[UIApplication sharedApplication] keyWindow] frame];
        
        XCTAssert(finalFrame.size.height < windowFrame.size.height, @"The final size of the StatsViewController should be smaller than the full width and height of the window");
        
        XCTAssert(finalFrame.size.width < windowFrame.size.width, @"The final size of the StatsViewController should be smaller than the full width and height of the window");
        
        XCTAssertEqualObjects(@([containerView.subviews count]), @2, @"Did not add the 'to' controller's view to the context's containerView in animateTransition:");
        
        XCTAssertEqualObjects(containerView.subviews[1], statsVC.view, @"Did not add the 'to' controller's view to the context's containerView in animateTransition:");
    }else{
        XCTFail(@"The transitioning delegate does not implement animationControllerForPresentedController:presentingController:sourceController:");
    }


}

- (void)testCalledComplete
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    
    if ([self hasImplementedDelegateMethod]) {
        id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate
                                                                 animationControllerForPresentedController:statsVC
                                                                 presentingController:badgesVC sourceController:badgesVC];
        
        MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:badgesVC toController:statsVC];
        
        NSTimeInterval animationInterval = [transition transitionDuration:context];
        
        
        __block CGRect beforeFrame;
        
        _appDel.beforeTransition = ^{
            beforeFrame = statsVC.view.frame;
        };
        
        [transition animateTransition:context];
        
        [tester runBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
            
            KIFTestWaitCondition(context.completed, error, @"Did not call completeTransition:YES on the transitionContext inside the animation completion block");
            
            return KIFTestStepResultSuccess;
        } timeout:animationInterval+0.1];
    }else{
        XCTFail(@"The transitioning delegate does not implement animationControllerForPresentedController:presentingController:sourceController:");
    }
    
    
}


- (BOOL)hasImplementedDelegateMethod
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    return [statsVC.transitioningDelegate respondsToSelector:@selector(animationControllerForPresentedController:presentingController:sourceController:)];
}

@end
