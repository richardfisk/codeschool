#import <UIKit/UIKit.h>

@interface BadgesViewController : UITableViewController <UIViewControllerTransitioningDelegate>
@property (strong, nonatomic) NSArray *badges;
@property (strong, nonatomic) UIPercentDrivenInteractiveTransition *myInteractiveTransition;
@end
