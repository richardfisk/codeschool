//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char presentedKey;
static char interactiveUpdatesKey;
static char calledCancelKey;
static char calledFinishKey;
static char calledCompleteTransitionKey;
static char completeTransitionFlagKey;

@implementation AppDelegate (TestAdditions)

- (void)setCompleteTransitionFlag:(NSNumber *)completeTransitionFlag
{
    objc_setAssociatedObject(self, &completeTransitionFlagKey, completeTransitionFlag, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)completeTransitionFlag
{
    return objc_getAssociatedObject(self, &completeTransitionFlagKey);
}

- (NSNumber *)calledCompleteTransition
{
    NSNumber *result = objc_getAssociatedObject(self, &calledCompleteTransitionKey);
    
    if (result) {
        return result;
    }else{
        return @0;
    }
}

- (void)setCalledCompleteTransition:(NSNumber *)calledCompleteTransition
{
    objc_setAssociatedObject(self, &calledCompleteTransitionKey, calledCompleteTransition, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)calledCancel
{
    NSNumber *result = objc_getAssociatedObject(self, &calledCancelKey);
    
    if (result) {
        return result;
    }else{
        return @0;
    }
}

- (void)setCalledCancel:(NSNumber *)calledCancel
{
    objc_setAssociatedObject(self, &calledCancelKey, calledCancel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)calledFinish
{
    NSNumber *result = objc_getAssociatedObject(self, &calledFinishKey);
    
    if (result) {
        return result;
    }else{
        return @0;
    }
}

- (void)setCalledFinish:(NSNumber *)calledFinish
{
    objc_setAssociatedObject(self, &calledFinishKey, calledFinish, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableArray *)interactiveUpdates
{
    NSMutableArray *result = objc_getAssociatedObject(self, &interactiveUpdatesKey);
    
    if (result) {
        return result;
    }else{
        return [NSMutableArray array];
    }
}

- (void)setInteractiveUpdates:(NSMutableArray *)interactiveUpdates
{
    objc_setAssociatedObject(self, &interactiveUpdatesKey, interactiveUpdates, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void) setPresentedViewController:(UIViewController *)presentedViewController
{
    objc_setAssociatedObject(self, &presentedKey, presentedViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentedViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentedKey);
}

@end
