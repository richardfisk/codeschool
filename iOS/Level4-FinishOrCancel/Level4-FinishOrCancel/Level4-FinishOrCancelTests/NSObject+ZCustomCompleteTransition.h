//
//  NSObject+ZCustomCompleteTransition.h
//  Level4-Answer-NewAnimatedTransition
//
//  Created by Eric Allam on 9/18/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ZCustomCompleteTransition)
- (void)custom_completeTransition:(BOOL)flag;
@end
