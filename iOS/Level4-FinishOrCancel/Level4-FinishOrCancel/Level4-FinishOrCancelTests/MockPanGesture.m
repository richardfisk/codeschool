//
//  MockPanGesture.m
//  Level4-FinishOrCancel
//
//  Created by Eric Allam on 9/24/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "MockPanGesture.h"

@implementation MockPanGesture
- (UIGestureRecognizerState)state
{
    return self.mockedState;
}
@end
