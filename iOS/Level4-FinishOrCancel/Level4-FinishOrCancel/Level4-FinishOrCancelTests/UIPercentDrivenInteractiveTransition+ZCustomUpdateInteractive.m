//
//  UIPercentDrivenInteractiveTransition+ZCustomUpdateInteractive.m
//  Level4-Updating
//
//  Created by Eric Allam on 9/17/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIPercentDrivenInteractiveTransition+ZCustomUpdateInteractive.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

static CGFloat customPercentComplete;
static BOOL swizzledPercentComplete = NO;

@implementation UIPercentDrivenInteractiveTransition (ZCustomUpdateInteractive)
+ (void) load
{
    [CSSwizzler swizzleClass:[UIPercentDrivenInteractiveTransition class]
               replaceMethod:@selector(updateInteractiveTransition:)
                  withMethod:@selector(custom_updateInteractiveTransition:)];
    
    [CSSwizzler swizzleClass:[UIPercentDrivenInteractiveTransition class]
               replaceMethod:@selector(cancelInteractiveTransition)
                  withMethod:@selector(custom_cancelInteractiveTransition)];
    
    [CSSwizzler swizzleClass:[UIPercentDrivenInteractiveTransition class]
               replaceMethod:@selector(finishInteractiveTransition)
                  withMethod:@selector(custom_finishInteractiveTransition)];
}

+(void)swizzleCustomPercentComplete:(CGFloat)percent
{
    customPercentComplete = percent;
    
    if (!swizzledPercentComplete) {
        [CSSwizzler swizzleClass:[UIPercentDrivenInteractiveTransition class]
                   replaceMethod:@selector(percentComplete)
                      withMethod:@selector(custom_percentComplete)];
        
        swizzledPercentComplete = YES;
    }
    
}

- (CGFloat)custom_percentComplete;
{
    return customPercentComplete;
}

- (void)custom_cancelInteractiveTransition
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.calledCancel = @(YES);
    
    [self custom_cancelInteractiveTransition];
}

- (void)custom_finishInteractiveTransition
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.calledFinish = @(YES);
    
    [self custom_finishInteractiveTransition];
}


- (void)custom_updateInteractiveTransition:(CGFloat)percentComplete;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *interactiveUpdates = appDel.interactiveUpdates;
    
    [interactiveUpdates addObject:@(percentComplete)];
    
    appDel.interactiveUpdates = interactiveUpdates;
    
    [self custom_updateInteractiveTransition:percentComplete];
}
@end
