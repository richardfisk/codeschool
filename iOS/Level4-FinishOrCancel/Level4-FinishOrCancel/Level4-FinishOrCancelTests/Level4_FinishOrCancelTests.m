//
//  Level3_AnimatedTransitionTests.m
//  Level3-AnimatedTransitionTests
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CSPropertyDefinition.h"
#import "UIPercentDrivenInteractiveTransition+ZCustomUpdateInteractive.h"
#import "MockPanGesture.h"

@interface BadgesViewController ()
- (void)handleGesture:(UIPanGestureRecognizer *)gesture;
@end

@interface Level4_FinishOrCancelTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}
@end

@implementation Level4_FinishOrCancelTests

- (void)setUp
{
    [super setUp];

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [KIFTestActor setDefaultTimeout:2.0f];

    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    
    [badgesVC dismissViewControllerAnimated:NO completion:nil];
}

- (void)testInteractiveFinish
{
    [UIPercentDrivenInteractiveTransition swizzleCustomPercentComplete:0.5f];
    [tester swipeViewWithAccessibilityLabel:@"Badge Stats" inDirection:KIFSwipeDirectionRight];
    
    XCTAssert([_appDel.calledFinish isEqualToNumber:@1], @"Did not call finishInteractiveTransition when the gesture ended");
    
    [tester waitForAbsenceOfViewWithAccessibilityLabel:@"Badge Stats"];
    
    // Make sure completeTransition: is called with YES
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    XCTAssert(![statsVC.view isDescendantOfView:_appDel.window], @"Did not pass YES to completeTranstion: when the interactive transition was not cancelled");
}

- (void)testInteractiveCancel
{
    [UIPercentDrivenInteractiveTransition swizzleCustomPercentComplete:0.1f];
    
    _appDel.calledCancel = @0;
    
    [tester waitForTappableViewWithAccessibilityLabel:@"Badge Stats"];
    [tester swipeViewWithAccessibilityLabel:@"Badge Stats" inDirection:KIFSwipeDirectionRight];
    
    XCTAssert([_appDel.calledCancel isEqualToNumber:@1], @"Did not call cancelInteractiveTransition when the gesture ended with a small percentComplete");

}

- (void)testInteractiveCancelOnGestureCancel
{
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    
    MockPanGesture *gesture = [[MockPanGesture alloc] init];
    gesture.mockedState = UIGestureRecognizerStateCancelled;
    
    _appDel.calledCancel = @0;
    
    [badgesVC performSelectorOnMainThread:@selector(handleGesture:) withObject:gesture waitUntilDone:YES];
    
    XCTAssert([_appDel.calledCancel isEqualToNumber:@1], @"Did not call cancelInteractiveTransition when the gesture was cancelled");
    
    _appDel.calledCancel = @0;
}

@end
