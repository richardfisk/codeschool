//
//  MockPanGesture.h
//  Level4-FinishOrCancel
//
//  Created by Eric Allam on 9/24/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MockPanGesture : UIPanGestureRecognizer
@property (assign, nonatomic) UIGestureRecognizerState mockedState;
@end
