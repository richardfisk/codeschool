//
//  NSObject+ZCustomCompleteTransition.m
//  Level4-Answer-NewAnimatedTransition
//
//  Created by Eric Allam on 9/18/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "NSObject+ZCustomCompleteTransition.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation NSObject (ZCustomCompleteTransition)
+ (void)load
{
    [CSSwizzler swizzleClass:[NSObject class]
               replaceMethod:@selector(completeTransition:)
                  withMethod:@selector(custom_completeTransition:)];
    
}
- (void)custom_completeTransition:(BOOL)flag;
{
    NSLog(@"GOT HERE %s", __PRETTY_FUNCTION__);
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.calledCompleteTransition = @(YES);
    appDel.completeTransitionFlag = @(flag);
    
    [self custom_completeTransition:flag];
}
@end
