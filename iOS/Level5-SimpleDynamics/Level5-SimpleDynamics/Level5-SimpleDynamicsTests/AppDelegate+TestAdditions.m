//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char presentedKey;
static char animatedKey;
static char beforeTransitionKey;
static char animatorInitCalledKey;
static char referenceFrameKey;
static char gravityItemsKey;
static char collisionItemsKey;
static char dynamicItemsKey;


@implementation AppDelegate (TestAdditions)

- (void) setAnimatorInitCalled:(NSNumber *)animatorInitCalled
{
    objc_setAssociatedObject(self, &animatorInitCalledKey, animatorInitCalled, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSNumber *)animatorInitCalled
{
    return objc_getAssociatedObject(self, &animatorInitCalledKey);
}

- (void) setReferenceFrame:(NSString *)referenceFrame
{
    objc_setAssociatedObject(self, &referenceFrameKey, referenceFrame, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)referenceFrame
{
    return objc_getAssociatedObject(self, &referenceFrameKey);
}

- (void) setGravityItems:(NSArray *)gravityItems
{
    objc_setAssociatedObject(self, &gravityItemsKey, gravityItems, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray *)gravityItems
{
    return objc_getAssociatedObject(self, &gravityItemsKey);
}

- (void) setCollisionItems:(NSArray *)collisionItems
{
    objc_setAssociatedObject(self, &collisionItemsKey, collisionItems, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray *)collisionItems
{
    return objc_getAssociatedObject(self, &collisionItemsKey);
}

- (void) setDynamicItems:(NSArray *)dynamicItems
{
    objc_setAssociatedObject(self, &dynamicItemsKey, dynamicItems, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray *)dynamicItems
{
    return objc_getAssociatedObject(self, &dynamicItemsKey);
}

- (void) setBeforeTransition:(void (^)(void))beforeTransition
{
    objc_setAssociatedObject(self, &beforeTransitionKey, beforeTransition, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(void)) beforeTransition
{
    return objc_getAssociatedObject(self, &beforeTransitionKey);
}

- (void) setAnimated:(NSNumber *)animated
{
    objc_setAssociatedObject(self, &animatedKey, animated, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)animated
{
    return objc_getAssociatedObject(self, &animatedKey);
}

- (void) setPresentedViewController:(UIViewController *)presentedViewController
{
    objc_setAssociatedObject(self, &presentedKey, presentedViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentedViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentedKey);
}

@end
