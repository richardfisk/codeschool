#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "CSPropertyDefinition.h"
#import "AppDelegate.h"
#import "AppDelegate+TestAdditions.h"
#import "BlocksVC.h"

@interface Level5_SimpleDynamicsTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}

@end

@implementation Level5_SimpleDynamicsTests

- (void)setUp
{
    [super setUp];
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    _appDel.animatorInitCalled = NO;
    _appDel.referenceFrame = nil;
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDynamicAnimatorPropertyImplementedInViewDidLoad
{
    CSPropertyDefinition *property = [self animatorProperty];
    _appDel.animatorInitCalled = NO;
    _appDel.referenceFrame = nil;
    BlocksVC *vc = [[BlocksVC alloc] init];
    [vc viewDidLoad];
    
    if(property) {
        XCTAssert(_appDel.animatorInitCalled, @"Did not initializer the dynamic animator object");
        XCTAssert([self correctReferenceView], @"Did not use the containerView UIView as the referenceView for the dynamic aniamtor");
    } else {
        XCTFail(@"You won't be able to attempt this challenge until you declare the UIDynamicAnimator property");
    }
}

- (void)testGravityBehavior
{
    CSPropertyDefinition *property = [self animatorProperty];
    if(property) {
        XCTAssert(_appDel.gravityItems.count == 4 && [_appDel.gravityItems[0] isKindOfClass:[UIImageView class]] && [_appDel.gravityItems[1] isKindOfClass:[UIImageView class]] && [_appDel.gravityItems[2] isKindOfClass:[UIImageView class]] && [_appDel.gravityItems[3] isKindOfClass:[UIImageView class]],@"Did not initialize the UIGravityBehavior object with the 4 UIImageViews i, O, S, and seven");
        
        BlocksVC *vc = [[BlocksVC alloc] init];
        [vc viewDidLoad];
        [vc.animator.behaviors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj isKindOfClass:[UIGravityBehavior class]]) {
                XCTAssertEqualWithAccuracy([obj gravityDirection].dx, 0, 0.1, @"Did not set the gravity behavior to have no x direction");
                XCTAssert([obj gravityDirection].dy >= 1.5, @"Did not set the gravity behavior to have a y direction of 1.5 or higher");
            }
        }];
    } else {
        XCTFail(@"You won't be able to attempt this challenge until you declare the UIDynamicAnimator property");
    }
}

- (void)testCollisionBehavior
{
    CSPropertyDefinition *property = [self animatorProperty];
    if(property) {
        XCTAssert(_appDel.collisionItems.count == 4 && [_appDel.collisionItems[0] isKindOfClass:[UIImageView class]] && [_appDel.collisionItems[1] isKindOfClass:[UIImageView class]] && [_appDel.collisionItems[2] isKindOfClass:[UIImageView class]] && [_appDel.collisionItems[3] isKindOfClass:[UIImageView class]],@"Did not initialize the UICollisionBehavior object with the 4 UIImageViews i, O, S, and seven");
        
        BlocksVC *vc = [[BlocksVC alloc] init];
        [vc viewDidLoad];
        [vc.animator.behaviors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj isKindOfClass:[UICollisionBehavior class]]) {
                XCTAssert([obj translatesReferenceBoundsIntoBoundary], @"Did not use the container view bounds as a collision boundary");
            }
        }];
    } else {
        XCTFail(@"You won't be able to attempt this challenge until you declare the UIDynamicAnimator property");
    }
}

- (void)testDynamicItemBehavior
{
    CSPropertyDefinition *property = [self animatorProperty];
    if(property) {
        XCTAssert(_appDel.dynamicItems.count == 4 && [_appDel.dynamicItems[0] isKindOfClass:[UIImageView class]] && [_appDel.dynamicItems[1] isKindOfClass:[UIImageView class]] && [_appDel.dynamicItems[2] isKindOfClass:[UIImageView class]] && [_appDel.dynamicItems[3] isKindOfClass:[UIImageView class]],@"Did not initialize a UIDynamicItemBehavior object with the 4 UIImageViews i, O, S, and seven");
        
        BlocksVC *vc = [[BlocksVC alloc] init];
        [vc viewDidLoad];
        [vc.animator.behaviors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj isKindOfClass:[UIDynamicItemBehavior class]]) {
                XCTAssert([obj elasticity] >= 0.5 || [obj elasticity] <= 0.8, @"Did not set the elasticity of the UIDynamicItemBehavior to a number between 0.5 and 0.8");
            }
        }];
    } else {
        XCTFail(@"You won't be able to attempt this challenge until you declare the UIDynamicAnimator property");
    }
}

- (CSPropertyDefinition *)animatorProperty
{
    CSPropertyDefinition *property;
    
    property = [CSPropertyDefinition firstPropertyWithType:@"UIDynamicAnimator" forClass:[BlocksVC class]];
    
    return property;
}

- (BOOL)correctReferenceView
{
    return [_appDel.referenceFrame isEqualToString:@"{{0, 0}, {320, 260}}"];
}

@end
