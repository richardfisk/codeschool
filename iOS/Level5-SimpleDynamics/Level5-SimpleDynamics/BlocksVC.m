#import "BlocksVC.h"

@interface BlocksVC ()

@property (nonatomic, strong, readwrite) UIDynamicAnimator *animator;

@end

@implementation BlocksVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 260)];
    [self.view addSubview:containerView];
    UIView *containerBottom = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(containerView.frame) - 1, CGRectGetWidth(containerView.frame), 1)];
    containerBottom.backgroundColor = [UIColor grayColor];
    [containerView addSubview:containerBottom];
    
    UIImageView *i = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"01-i-block"]];
    UIImageView *O = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"02-O-block"]];
    UIImageView *S = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"03-S-block"]];
    UIImageView *seven = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"04-7-block"]];
    
    i.frame = CGRectMake(45, 40, 50, 46);
    O.frame = CGRectMake(105, 50, 50, 46);
    S.frame = CGRectMake(165, 70, 50, 46);
    seven.frame = CGRectMake(225, 85, 50, 46);
    
    [containerView addSubview:i];
    [containerView addSubview:O];
    [containerView addSubview:S];
    [containerView addSubview:seven];
    
    // TODO: create a UIDynamicAnimator instance using containerView as the reference view
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:containerView];
    
    // TODO: create a UIGravityBehavior instance, set the gravity direction, and add the behavior to the animator
    UIGravityBehavior *gravityBehaviour = [[UIGravityBehavior alloc] initWithItems:@[i,O,S, seven]];
    gravityBehaviour.gravityDirection = CGVectorMake(0, 4.0f);
    [_animator addBehavior:gravityBehaviour];
    
    // TODO: create a UICollisonBehavior instance, set collision boundaries, and add the behavior to the animator
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[i,O,S, seven]];
    collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
    [_animator addBehavior:collisionBehavior];
    // TODO: create a UIDynamicItemBehavior instance, set the elasticity, and add the behavior to the animator
    UIDynamicItemBehavior *itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[i,O,S, seven]];
    itemBehavior.elasticity = 0.5f;
    [_animator addBehavior:itemBehavior];
}

@end
