//
//  UIView+ZCustomSpringAnimation.h
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/15/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ZCustomSpringAnimation)
+ (void)custom_animateWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay usingSpringWithDamping:(CGFloat)dampingRatio initialSpringVelocity:(CGFloat)velocity options:(UIViewAnimationOptions)options animations:(void (^)(void))animations completion:(void (^)(BOOL finished))completion;
@end
