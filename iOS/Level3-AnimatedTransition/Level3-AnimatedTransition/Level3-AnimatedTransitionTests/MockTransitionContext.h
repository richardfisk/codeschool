//
//  MockTransitionContext.h
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/15/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MockTransitionContext : NSObject <UIViewControllerContextTransitioning>
@property (assign, nonatomic, getter = isCompleted) BOOL completed;

- (instancetype) initWithFromController:(UIViewController *)from toController:(UIViewController *)to;

@end
