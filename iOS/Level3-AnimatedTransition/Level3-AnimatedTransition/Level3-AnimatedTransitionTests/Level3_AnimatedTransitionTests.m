//
//  Level3_AnimatedTransitionTests.m
//  Level3-AnimatedTransitionTests
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface Level3_AnimatedTransitionTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}
@end

@implementation Level3_AnimatedTransitionTests

- (void)setUp
{
    [super setUp];

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // This is how we are only running the tapViewWithAccessibilityLabel call once instead
    // of before every since test method
    if (!_appDel.presentedViewController) {
        [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testPresentCorrectViewControllerAnimated
{
    
    XCTAssert(_appDel.presentedViewController, @"Did not present an instance of StatsViewController when the user taps the Display Stats button");
    
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    XCTAssert([statsVC isKindOfClass:[StatsViewController class]], @"Did not present an instance of StatsViewController when the user taps the Display Stats button");
    
    XCTAssertEqualObjects(_appDel.animated, @1, @"Did not present the controller with animated YES");
}

- (void) testCustomPresentation
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    XCTAssertEqual(statsVC.modalPresentationStyle, UIModalPresentationCustom, @"Did not set the modalPresentationStyle to UIModalPresentationCustom");
}

- (void) testTransitioningDelegate
{
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    
    XCTAssertTrue([statsVC.transitioningDelegate conformsToProtocol:@protocol(UIViewControllerTransitioningDelegate)], @"Did not set a transitioningDelegate that conforms to UIViewControllerTransitioningDelegate");
}

@end
