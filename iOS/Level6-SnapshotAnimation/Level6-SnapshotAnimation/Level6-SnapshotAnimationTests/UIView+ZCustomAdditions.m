//
//  UIView+ZCustomAdditions.m
//  Level6-SnapshotAnimation
//
//  Created by Eric Allam on 10/28/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIView+ZCustomAdditions.h"
#import "AppDelegate+TestAdditions.h"
#import "CSSwizzler.h"

@implementation UIView (ZCustomAdditions)
+ (void)load
{
    [CSSwizzler swizzleClass:[UIView class]
               replaceMethod:@selector(snapshotViewAfterScreenUpdates:)
                  withMethod:@selector(custom_snapshotViewAfterScreenUpdates:)];
}

- (UIView *)custom_snapshotViewAfterScreenUpdates:(BOOL)afterUpdates;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIView *snapshot = [self custom_snapshotViewAfterScreenUpdates:afterUpdates];
    
    if (!appDel.snapshottedView) {
        appDel.afterUpdates = @(afterUpdates);
        appDel.snapshottedView = self;
        appDel.snapshottedFrame = self.frame;
    }
    
    appDel.viewSnapshot = snapshot;
    
    return snapshot;
}
@end
