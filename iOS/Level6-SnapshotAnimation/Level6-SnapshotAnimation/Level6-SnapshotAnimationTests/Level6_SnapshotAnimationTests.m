#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MockTransitionContext.h"
#import "SideTransition.h"
#import "UIView+ZCustomAdditions.h"

@interface Level6_SnapshotAnimationTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}
@end

@implementation Level6_SnapshotAnimationTests

- (void)setUp
{
    [super setUp];
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    tester.executionBlockTimeout = 2.0;
    [KIFTestActor setDefaultTimeout:2.0];
    
    
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testTakingSnapshotAfterScreenUpdates
{
    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    
    XCTAssert([_appDel.snapshottedView.accessibilityLabel isEqualToString:@"Stats View"], @"Did not call snapshotViewAfterScreenUpdates: on the 'to' view controller in the SideTransition animation");
    
    XCTAssert(_appDel.afterUpdates, @"Send in YES to snapshotViewAfterScreenUpdates:");
    
    CGRect expectedSnapshotFrame = CGRectMake(336, 20, 280, 528);
    
    XCTAssert(CGRectEqualToRect(expectedSnapshotFrame, _appDel.snapshottedFrame), @"Call snapshotViewAfterScreenUpdates: after setting the toVC.view's frame to the initial value");
    
    [tester tapViewWithAccessibilityLabel:@"Good Job"];
}

- (void)testAddSnapshotToContainerView
{
    UIViewController *navVC = _appDel.window.rootViewController;
    StatsViewController *statsVC = [[StatsViewController alloc] init];
    
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:navVC toController:statsVC];
    
    SideTransition *transition = [[SideTransition alloc] init];
    [transition animateTransition:context];
    
    XCTAssert([_appDel.viewSnapshot isDescendantOfView:context.containerView], @"Add the snapshot view to the transitionContext's containerView instead of adding the toVC.view");
    
    XCTAssert(![statsVC.view isDescendantOfView:context.containerView], @"Add the snapshot view to the transitionContext's containerView instead of adding the toVC.view");
    
}

- (void)testAnimatingSnapshot
{
    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    
    CGRect expectedSnapshotFinalFrame = CGRectMake(20, 20, 280, 528);
    
    XCTAssert(CGRectEqualToRect(expectedSnapshotFinalFrame, _appDel.viewSnapshot.frame), @"Did not animate the snapshot view into position");
    
    [tester tapViewWithAccessibilityLabel:@"Good Job"];
}

- (void)testCompletionCallback
{
    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    
    [tester waitForViewWithAccessibilityLabel:@"Stats View"]; // Make sure you remove the snapshot view from the transition context and add back in the toVC.view before calling completeTransition
    
    [tester tapViewWithAccessibilityLabel:@"Good Job"];
}

@end
