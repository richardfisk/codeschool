//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char presentedKey;
static char snapshottedViewKey;
static char afterUpdatesKey;
static char snapshottedViewKey;
static char snapshottedFrameKey;
static char viewSnapshotKey;

@implementation AppDelegate (TestAdditions)

- (void)setViewSnapshot:(UIView *)viewSnapshot
{
    objc_setAssociatedObject(self, &viewSnapshotKey, viewSnapshot, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)viewSnapshot
{
    return objc_getAssociatedObject(self, &viewSnapshotKey);
}

- (void)setAfterUpdates:(NSValue *)afterUpdates
{
    objc_setAssociatedObject(self, &afterUpdatesKey, afterUpdates, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSValue *)afterUpdates
{
    return objc_getAssociatedObject(self, &afterUpdatesKey);
}


- (void)setSnapshottedView:(UIView *)snapshottedView
{
    objc_setAssociatedObject(self, &snapshottedViewKey, snapshottedView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)snapshottedView
{
    return objc_getAssociatedObject(self, &snapshottedViewKey);
}

- (void)setSnapshottedFrame:(CGRect)snapshottedFrame
{
    objc_setAssociatedObject(self, &snapshottedFrameKey, [NSValue valueWithCGRect:snapshottedFrame], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGRect)snapshottedFrame
{
    id snapshottedFrameValue = objc_getAssociatedObject(self, &snapshottedFrameKey);
    
    return [snapshottedFrameValue CGRectValue];
}

- (void) setPresentedViewController:(UIViewController *)presentedViewController
{
    objc_setAssociatedObject(self, &presentedKey, presentedViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentedViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentedKey);
}

@end
