#import "SideTransition.h"

@implementation SideTransition

- (NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 1.0f;
}

- (void) animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];

    CGRect fullFrame = [transitionContext initialFrameForViewController:fromVC];
    
    toVC.view.frame = CGRectMake(fullFrame.size.width + 16, 20, fullFrame.size.width - 40, fullFrame.size.height - 40);
    
    // TODO: take a snapshot of toVC.view and animate that instead of the toVC.view
    
    [[transitionContext containerView] addSubview:toVC.view];
    UIView *snapshotView = [toVC.view snapshotViewAfterScreenUpdates:YES];
    snapshotView.frame = toVC.view.frame;
    NSLog(@"%@", snapshotView);
    snapshotView.accessibilityLabel = @"Good Job";
    [[transitionContext containerView] addSubview:snapshotView];
//    [toVC.view removeFromSuperview];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 usingSpringWithDamping:0.5f initialSpringVelocity:0.6f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        snapshotView.frame = CGRectMake(20, 20, fullFrame.size.width - 40, fullFrame.size.height - 40);
        
    } completion:^(BOOL finished) {
        // TODO: clean up the snapshot view
        toVC.view.frame = snapshotView.frame;
        [[transitionContext containerView] addSubview:toVC.view];
        [snapshotView removeFromSuperview];

        [transitionContext completeTransition:YES];
    }];
}

- (void)wtf:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    CGRect fullFrame = [transitionContext initialFrameForViewController:fromVC];
    
    toVC.view.frame = CGRectMake(fullFrame.size.width + 16, 20, fullFrame.size.width - 40, fullFrame.size.height - 40);
    
    // TODO: take a snapshot of toVC.view and animate that instead of the toVC.view
    UIView *snapshotView = [toVC.view snapshotViewAfterScreenUpdates:YES];
    snapshotView.frame = toVC.view.frame;
    snapshotView.accessibilityLabel = @"Good Job";
    [[transitionContext containerView] addSubview:snapshotView];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 usingSpringWithDamping:0.5f initialSpringVelocity:0.6f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        snapshotView.frame = CGRectMake(20, 20, fullFrame.size.width - 40, fullFrame.size.height - 40);
        
    } completion:^(BOOL finished) {
        // TODO: clean up the snapshot view
        toVC.view.frame = snapshotView.frame;
        [snapshotView removeFromSuperview];
        [[transitionContext containerView] addSubview:toVC.view];
        [transitionContext completeTransition:YES];
    }];
}

@end
