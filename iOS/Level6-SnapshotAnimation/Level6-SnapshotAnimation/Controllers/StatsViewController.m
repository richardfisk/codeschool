#import "StatsViewController.h"

@implementation StatsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.accessibilityLabel = @"Stats View";
    
    self.earnedBadgeLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.badges.count];
}

- (IBAction) goodJob:(id)sender;
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
