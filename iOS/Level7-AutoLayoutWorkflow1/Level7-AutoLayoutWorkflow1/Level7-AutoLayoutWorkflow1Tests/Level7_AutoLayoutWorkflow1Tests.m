#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "CSAppDelegate.h"

#define kDoubleEpsilon (0.5)

static BOOL doubleCloseTo(double a, double b)
{
    if(fabs(a-b) < kDoubleEpsilon) {
        return YES;
    } else {
        return NO;
    }
}

@interface NSIBPrototypingLayoutConstraint : NSLayoutConstraint
@end

@interface NSAutoresizingMaskLayoutConstraint : NSLayoutConstraint
@end

@interface NSContentSizeLayoutConstraint : NSLayoutConstraint
@end

@interface Level7_AutoLayoutWorkflow1Tests : CSRecordingTestCase

@end

#pragma mark - Enum to NSString helper methods
NSString *NSStringFromNSLayoutAttribute(NSLayoutAttribute attribute)
{
    switch (attribute) {
        case 1:
            return @"NSLayoutAttributeLeft";
            break;
        case 2:
            return @"NSLayoutAttributeRight";
            break;
        case 3:
            return @"NSLayoutAttributeTop";
            break;
        case 4:
            return @"NSLayoutAttributeBottom";
            break;
        case 5:
            return @"NSLayoutAttributeLeading";
            break;
        case 6:
            return @"NSLayoutAttributeTrailing";
            break;
        case 7:
            return @"NSLayoutAttributeWidth";
            break;
        case 8:
            return @"NSLayoutAttributeHeight";
            break;
        case 9:
            return @"NSLayoutAttributeCenterX";
            break;
        case 10:
            return @"NSLayoutAttributeCenterY";
            break;
        case 11:
            return @"NSLayoutAttributeBaseline";
            break;
        default:
            return @"NSLayoutAttributeNotAnAttribute";
            break;
    }
}

NSString *NSStringFromNSLayoutRelation(NSLayoutRelation relation)
{
    switch (relation) {
        case -1:
            return @"NSLayoutRelationLessThanOrEqual";
            break;
        case 0:
            return @"NSLayoutRelationEqual";
            break;
        case 1:
            return @"NSLayoutRelationGreaterThanOrEqual";
            break;
        default:
            return @"OOPS";
            break;
    }
}

@implementation Level7_AutoLayoutWorkflow1Tests {
    CSAppDelegate *_appDel;
    UIView *_mainView;
    BOOL _mainViewWantsAutolayout;
}

- (void)setUp
{
    [super setUp];
    _appDel = [[UIApplication sharedApplication] delegate];
    _mainView = (UIView *)_appDel.window.subviews[0];
    _mainViewWantsAutolayout = (NSInteger)[_mainView performSelector:@selector(_wantsAutolayout)];
}

- (void)tearDown
{
    _mainViewWantsAutolayout = NO;
    [super tearDown];
}

- (void)testCsLogoConstraints
{
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *csLogoConstraints = [self firstItemConstraintsForRestorationIdentifier:@"cs-logo" fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [csLogoConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                id csLogoConstraint = obj[0];
                if([csLogoConstraint firstAttribute] == NSLayoutAttributeLeading) {
                    XCTAssert([self constraint:csLogoConstraint hasConstantEqualTo:10.0 forAttribute:NSLayoutAttributeLeading], @"Did not set a Leading Space to Container constraint on cs-logo with a constant of 10");
                } else if([csLogoConstraint firstAttribute] == NSLayoutAttributeTop) {
                    XCTAssert([self constraint:csLogoConstraint hasConstantEqualTo:8.0 forAttribute:NSLayoutAttributeLeading], @"Did not set a Top Space constraint on cs-logo with a constant of 10");
                }
            }];
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testPathLogoConstraints
{
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *csLogoConstraints = [self firstItemConstraintsForRestorationIdentifier:@"path-logo" fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [csLogoConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                id pathLogoViewConstraint = obj[0];
                if([pathLogoViewConstraint firstAttribute] == NSLayoutAttributeCenterY) {
                    XCTAssert([self constraint:pathLogoViewConstraint hasConstantEqualTo:-0.5 forAttribute:NSLayoutAttributeCenterY], @"Did not set a Center Y constraint between path-logo and cs-logo");
                } else if([pathLogoViewConstraint firstAttribute] == NSLayoutAttributeLeading) {
                    XCTAssert([self constraint:pathLogoViewConstraint hasConstantEqualTo:141.0 forAttribute:NSLayoutAttributeLeading], @"Did not set a Horizontal Space constraint between path-logo and cs-logo");
                }
            }];
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testHeaderViewConstraints
{
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *headerViewConstraints = [self firstItemConstraintsForRestorationIdentifier:@"header-view" fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [headerViewConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                id headerViewConstraint = obj[0];
                if([headerViewConstraint firstAttribute] == NSLayoutAttributeWidth) {
                    XCTAssert([self constraint:headerViewConstraint hasConstantEqualTo:320.0 forAttribute:NSLayoutAttributeWidth], @"Did not set a width constraint for header-view with a constant of 320");
                } else if([headerViewConstraint firstAttribute] == NSLayoutAttributeHeight) {
                    XCTAssert([self constraint:headerViewConstraint hasConstantEqualTo:100 forAttribute:NSLayoutAttributeHeight], @"Did not set a height constraint for header-view with a constant of 100");
                } else if([headerViewConstraint firstAttribute] == NSLayoutAttributeTop) {
                    if([[[headerViewConstraint secondItem] restorationIdentifier] isEqualToString:@"cs-logo"]) {
                        XCTAssert([self constraint:headerViewConstraint hasConstantEqualTo:7.0 forAttribute:NSLayoutAttributeTop], @"Did not set a vertical spacing constraint between header-view and cs-logo");
                    } else if([[[headerViewConstraint secondItem] restorationIdentifier] isEqualToString:@"path-logo"]) {
                        XCTAssert([self constraint:headerViewConstraint hasConstantEqualTo:5.0 forAttribute:NSLayoutAttributeTop], @"Did not set a vertical spacing constraint between header-view and path-logo");
                    } else {
                        XCTFail(@"Did not set a vertical spacing constraint between header-view and either cs-logo or path-view");
                    }
                } else if([headerViewConstraint firstAttribute] == NSLayoutAttributeCenterX) {
                    XCTAssert([self constraint:headerViewConstraint hasConstantEqualTo:0 forAttribute:NSLayoutAttributeCenterX], @"Did not set a Center Horizontally in Container constrant for header-view");
                }
            }];
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testPlayCourseViewConstraints
{
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *playCourseViewConstraints = [self firstItemConstraintsForRestorationIdentifier:@"play-course-view" fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [playCourseViewConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                id playCourseViewConstraint = obj[0];
                if([playCourseViewConstraint firstAttribute] == NSLayoutAttributeWidth) {
                    XCTAssert([self constraint:playCourseViewConstraint hasConstantEqualTo:320.0 forAttribute:NSLayoutAttributeWidth], @"Did not set a width constraint for play-course-view with a constant of 320");
                } else if([playCourseViewConstraint firstAttribute] == NSLayoutAttributeHeight) {
                    XCTAssert([self constraint:playCourseViewConstraint hasConstantEqualTo:44.0 forAttribute:NSLayoutAttributeHeight], @"Did not set a height constraint for play-course-view with a constant of 44");
                } else if([playCourseViewConstraint firstAttribute] == NSLayoutAttributeTop) {
                    if([[[playCourseViewConstraint secondItem] restorationIdentifier] isEqualToString:@"play-course-view"]) {
                        XCTAssert([self constraint:playCourseViewConstraint hasConstantEqualTo:0.0 forAttribute:NSLayoutAttributeTop], @"Did not set a vertical spacing constraint between play-course-view and header-view");
                    }
                } else if([playCourseViewConstraint firstAttribute] == NSLayoutAttributeLeading) {
                    XCTAssert([self constraint:playCourseViewConstraint hasConstantEqualTo:0 forAttribute:NSLayoutAttributeLeading], @"Did not set a leading space constrant for play-course-view");
                }
            }];
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}

- (void)testBadgeConstraints
{
    if(_mainViewWantsAutolayout) {
        id layoutEngine = [_mainView performSelector:@selector(_layoutEngine)];
        NSArray *layoutEngineConstraints = [layoutEngine performSelector:@selector(constraints)];
        
        NSArray *viewConstraintsFirstItem = [self firstItemConstraintsForRestorationIdentifier:@"badge7" fromLayoutEngineConstraints:layoutEngineConstraints];
        NSArray *viewConstraintsSecondItem = [self secondItemConstraintsForRestorationIdentifier:@"badge7" fromLayoutEngineConstraints:layoutEngineConstraints];
        
        __block NSMutableArray *prototypingConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *contentSizeConstraints = [[NSMutableArray alloc] init];
        __block NSMutableArray *normalConstraints = [[NSMutableArray alloc] init];
        
        [viewConstraintsFirstItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] firstItem] restorationIdentifier], [[obj[0] firstItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        [viewConstraintsSecondItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj[0] isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [prototypingConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj[0] isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [contentSizeConstraints addObject:vals];
            } else if([obj[0] isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj[0], [[obj[0] secondItem] restorationIdentifier], [[obj[0] secondItem] class]];
                [normalConstraints addObject:vals];
            }
        }];
        
        __block NSMutableSet *setOfViewsWithAmbiguousLayout = [[NSMutableSet alloc] init];
        
        [prototypingConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [setOfViewsWithAmbiguousLayout addObject:obj[1]];
        }];
        
        BOOL isAmbiguousLayout = [self reportAmbiguousLayoutTestFailureWithSet:setOfViewsWithAmbiguousLayout];
        
        if(!isAmbiguousLayout) {
            if([self checkCorrectAttributes:@[@(NSLayoutAttributeCenterX),@(NSLayoutAttributeTop)] forConstraints:normalConstraints]) {
                [normalConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    id badgeConstraint = obj[0];
                    if([badgeConstraint firstAttribute] == NSLayoutAttributeTop && [[[badgeConstraint secondItem] restorationIdentifier] isEqualToString:@"play-course-view"]) {
                        XCTAssert([self constraint:badgeConstraint hasConstantEqualTo:52.0 forAttribute:NSLayoutAttributeTop], @"Did not set a vertical spacing constraint between badge7 and play-course-view");
                    } else if([badgeConstraint firstAttribute] == NSLayoutAttributeCenterX) {
                        XCTAssert([self constraint:badgeConstraint hasConstantEqualTo:0 forAttribute:NSLayoutAttributeCenterX], @"Did not set a center-x constraint for badge7");
                    }
                }];
            }
        }
    } else {
        XCTFail(@"Make sure that the 'Use Autolayout' checkbox is checked for the Interface Builder document, because it's not checked right now.");
    }
}


#pragma mark - Instance Methods

- (NSArray *)firstItemConstraintsForRestorationIdentifier:(NSString *)restorationID fromLayoutEngineConstraints:(NSArray *)layoutEngineConstraints
{
    __block NSMutableArray *relevantConstraints = [[NSMutableArray alloc] init];
    
    [layoutEngineConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([[[obj firstItem] restorationIdentifier] isEqualToString:restorationID]) {
            if([obj isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj firstItem] restorationIdentifier], [[obj firstItem] class]];
                [relevantConstraints addObject:vals];
            }
        }
    }];
    return [[NSArray alloc] initWithArray:relevantConstraints];
}

- (NSArray *)secondItemConstraintsForRestorationIdentifier:(NSString *)restorationID fromLayoutEngineConstraints:(NSArray *)layoutEngineConstraints
{
    __block NSMutableArray *relevantConstraints = [[NSMutableArray alloc] init];
    
    [layoutEngineConstraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([[[obj secondItem] restorationIdentifier] isEqualToString:restorationID]) {
            if([obj isKindOfClass:[NSIBPrototypingLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSAutoresizingMaskLayoutConstraint class]]) {
            } else if([obj isKindOfClass:[NSContentSizeLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            } else if([obj isKindOfClass:[NSLayoutConstraint class]]) {
                NSArray *vals = @[obj, [[obj secondItem] restorationIdentifier], [[obj secondItem] class]];
                [relevantConstraints addObject:vals];
            }
        }
    }];
    return [[NSArray alloc] initWithArray:relevantConstraints];
}

- (BOOL)reportAmbiguousLayoutTestFailureWithSet:(NSMutableSet *)setOfViewsWithAmbiguousLayout
{
    if(setOfViewsWithAmbiguousLayout.count == 1) {
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ view has ambiguous layout because you didn't create enough constraints for it.  Make sure that the view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", [[setOfViewsWithAmbiguousLayout allObjects] objectAtIndex:0]]);
        return YES;
    } else if(setOfViewsWithAmbiguousLayout.count == 2) {
        NSMutableString *viewsWithAmbiguousLayout = [[NSMutableString alloc] init];
        [[setOfViewsWithAmbiguousLayout allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if(idx < setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"%@ ",obj]];
            } else if(idx == setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"and %@",obj]];
            }
        }];
        
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ views have ambiguous layout because you didn't create enough constraints for them.  Make sure that each view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", viewsWithAmbiguousLayout]);
        return YES;
    } else if(setOfViewsWithAmbiguousLayout.count > 2) {
        NSMutableString *viewsWithAmbiguousLayout = [[NSMutableString alloc] init];
        [[setOfViewsWithAmbiguousLayout allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if(idx < setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"%@, ",obj]];
            } else if(idx == setOfViewsWithAmbiguousLayout.count-1) {
                [viewsWithAmbiguousLayout appendString:[NSString stringWithFormat:@"and %@",obj]];
            }
        }];
        
        XCTFail(@"%@",[NSString stringWithFormat:@"The %@ views have ambiguous layout because you didn't create enough constraints for them.  Make sure that each view has constraints that will determine the X and Y position of the top left corner of the view, as well as the width and height of the view.", viewsWithAmbiguousLayout]);
        return YES;
    }
    return NO;
}

- (BOOL)constraint:(NSLayoutConstraint *)constraint hasConstantEqualTo:(CGFloat)constant forAttribute:(NSLayoutAttribute)attribute
{
    return doubleCloseTo([constraint constant], constant);
}

- (BOOL)checkCorrectAttributes:(NSArray *)attributes forConstraints:(NSArray *)constraints
{
    __block NSMutableArray *existingAttributes = [[NSMutableArray alloc] init];
    
    [constraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id constraint = obj[0];
        [existingAttributes addObject:@([constraint firstAttribute])];
    }];
    
    __block NSMutableArray *requiredCopy = [[NSMutableArray alloc] initWithArray:attributes];
    
    for (id attribute in attributes) {
        for(id existing in existingAttributes) {
            if([existing integerValue] == [attribute integerValue]) {
                [requiredCopy removeObject:existing];
            }
        }
    }
    __block BOOL isCorrect = YES;
    
    [requiredCopy enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XCTFail(@"%@",[NSString stringWithFormat:@"missing %@ constraint",NSStringFromNSLayoutAttribute([obj integerValue])]);
        isCorrect = NO;
    }];
    return isCorrect;
}

@end