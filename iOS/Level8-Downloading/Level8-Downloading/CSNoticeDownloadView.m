#import "CSNoticeDownloadView.h"

@interface CSNoticeDownloadView () <UIDynamicAnimatorDelegate>
@property (strong, nonatomic) UIDynamicAnimator *animator;
@end

@implementation CSNoticeDownloadView

+ (instancetype) noticeView;
{
    CSNoticeDownloadView *noticeView = [[[NSBundle mainBundle] loadNibNamed:@"CSNoticeDownloadView" owner:nil options:nil] lastObject];
    
    if ([noticeView isKindOfClass:self]) {
        return noticeView;
    }else{
        return nil;
    }
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:[UIApplication sharedApplication].keyWindow];
    self.animator.delegate = self;
    
    return self;
}

- (void)displayNotice;
{
    self.frame = CGRectMake(0, -64, 320, 64);
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    UISnapBehavior *snap = [[UISnapBehavior alloc] initWithItem:self snapToPoint:CGPointMake(160, 32)];
    snap.damping = 0.4f;
    [self.animator addBehavior:snap];
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator
{
    [self.animator removeAllBehaviors];
    
    [UIView animateWithDuration:0.5f animations:^{
        self.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
