//
//  NSURLSession+ZAdditions.m
//  Level8-Downloading
//
//  Created by Eric Allam on 26/11/2013.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "NSURLSession+ZAdditions.h"
#import "AppDelegate+TestAdditions.h"
#import <objc/runtime.h>
#import "CSSwizzler.h"

@implementation NSURLSession (ZAdditions)

// Define the interface for our custom implementation of downloadTaskWithURL:
static NSURLSessionDownloadTask * CustomDownloadTaskWithURL(id, SEL, NSURL *);

// Create a function pointer for the original downloadTaskWithURL: to be stored in
static NSURLSessionDownloadTask * (*CustomDownloadedTaskWithURLIMP)(id, SEL, NSURL *);

// Define our custom implementation of downloadTaskWithURL:, calling CustomDownloadedTaskWithURLIMP
// to call the original method
static NSURLSessionDownloadTask * CustomDownloadTaskWithURL(id self, SEL _cmd, NSURL *url){
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSURLSessionDownloadTask *task = CustomDownloadedTaskWithURLIMP(self, _cmd, url);
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincompatible-pointer-types"
    [NSURLProtocol setProperty:self forKey:@"TaskSession" inRequest:task.originalRequest];
#pragma clang diagnostic pop
    
    appDel.lastDownloadTask = task;
    
    return task;
}

+ (void)load
{
    [CSSwizzler swizzleClass:[[self sharedSession] class]
               replaceMethod:@selector(downloadTaskWithURL:)
          withImplementation:(IMP)CustomDownloadTaskWithURL
                     storeIn:(IMP *)&CustomDownloadedTaskWithURLIMP];
}

@end
