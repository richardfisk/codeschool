//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/runtime.h>

static char lastDownloadTaskKey;

@implementation AppDelegate (TestAdditions)

- (void)setLastDownloadTask:(NSURLSessionTask *)lastDownloadTask
{
    objc_setAssociatedObject(self, &lastDownloadTaskKey, lastDownloadTask, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSURLSessionTask *)lastDownloadTask
{
    return objc_getAssociatedObject(self, &lastDownloadTaskKey);
}

@end
