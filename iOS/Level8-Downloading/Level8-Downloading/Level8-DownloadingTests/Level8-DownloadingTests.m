//
//  Level8-DownloadingTests.m
//  Level8-DownloadingTests
//
//  Created by Eric Allam on 10/22/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

// Test imports
#import "CSRecordingTestCase.h"
#import "CSPropertyDefinition.h"
#import "AppDelegate+TestAdditions.h"

// App imports
#import "CSVideoViewController.h"

@interface Level8_DownloadingTests : CSRecordingTestCase
@property (strong, nonatomic) NSDictionary *videoInfo;
@end

@implementation Level8_DownloadingTests

- (void)setUp
{
    [super setUp];
    
    self.videoInfo = @{
        @"badge_url": @"https://d1ffx7ull4987f.cloudfront.net/images/achievements/large_badge/307/level-1-on-core-ios-7-45046cadb933cc707038d10dccb28110.png",
        @"course": @"Core iOS 7",
        @"description": @"Learn the most common problems you'll run into when upgrading your app for iOS 7.",
        @"download_url": @"http://hls.codeschool.com.s3.amazonaws.com/2a201cc5249eef3dda4f813d678de123_1.jpg",
        @"duration": @"4:31",
        @"highres_thumbnail_url": @"http://hls.codeschool.com.s3.amazonaws.com/2a201cc5249eef3dda4f813d678de123_1.jpg",
        @"id": @4,
        @"more_url": @"https://www.codeschool.com/courses/core-ios-7",
        @"streaming_url": @"http://hls.codeschool.com.s3.amazonaws.com/c27342f3979688fadbc7663f90035390.m3u8",
        @"thumbnail_url": @"http://hls.codeschool.com.s3.amazonaws.com/2e11422f3cd52020414ba002463cc801_1.jpg",
        @"title": @"Updating from iOS 6",
    };
}

- (void)testDownloadSessionCreation
{
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:self.videoInfo];
    
    XCTAssert([videoVC.downloadSession isMemberOfClass:[[NSURLSession sharedSession] class]], @"Did not create an NSURLSession instance and assign it to the downloadSession property in CSVideoViewController's initWithVideoInfo: method");
    
    XCTAssertEqualObjects(videoVC.downloadSession.delegate, videoVC, @"Did not set the downloadSession's delegate to the CSVideoViewController instance");
}

-(void)testDownloadTaskCreation
{
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:self.videoInfo];
    
    [self triggerViewDidLoad:videoVC];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.lastDownloadTask = nil;
    
    [videoVC startDownload:nil];
    
    XCTAssert(appDel.lastDownloadTask, @"Did not create a download task in [CSVideoViewController startDownload:]");
    
    XCTAssert(appDel.lastDownloadTask.state == NSURLSessionTaskStateRunning, @"Did not start the download by sending the download task the 'resume' message");
    
    NSURLRequest *downloadRequest = appDel.lastDownloadTask.originalRequest;
    
    XCTAssert([[downloadRequest.URL absoluteString] isEqualToString:self.videoInfo[@"download_url"]], @"The download task in startDownload: should request the URL corresponding to the 'download_url' key in the video NSDictionary.");
    
    NSURLSession *session = [NSURLProtocol propertyForKey:@"TaskSession" inRequest:downloadRequest];
    
    XCTAssertEqualObjects(session.delegate, videoVC, @"Did not initiate the download task from the session in the CSVideoViewController class that has the controller instance set as the delegate");
}

-(void)testProgressUpdates
{
    CSVideoViewController<NSURLSessionDownloadDelegate> *videoVC = (CSVideoViewController<NSURLSessionDownloadDelegate> *)[[CSVideoViewController alloc] initWithVideoInfo:self.videoInfo];
    
    [self triggerViewDidLoad:videoVC];
    
    if ([videoVC respondsToSelector:@selector(URLSession:downloadTask:didWriteData:totalBytesWritten:totalBytesExpectedToWrite:)]) {
        
        videoVC.progressView.progress = 0.0f;
        
        [videoVC URLSession:[NSURLSession sharedSession] downloadTask:nil didWriteData:(int64_t)0 totalBytesWritten:(int64_t)50 totalBytesExpectedToWrite:(int64_t)100];
        
        XCTAssertEqualWithAccuracy(videoVC.progressView.progress, 0.0f, 0.01f, @"The progress delegate set the progressView.progress on the calling thread, instead of the main UI thread");
        
        [tester waitForTimeInterval:0.5f];
        
        XCTAssertEqualWithAccuracy(videoVC.progressView.progress, 0.50f, 0.01, @"The progress delegate method does not set the progressView.progress to the correct value based on the parameters. Make sure and use totalBytesWritten and totalBytesExpectedToWrite to calculate the progress");
        
    }else{
        XCTFail(@"Did not implement the delegate method in CSVideoViewController for getting progress updates");
    }

}

-(void)testDownloadCompletion
{
    CSVideoViewController<NSURLSessionDownloadDelegate> *videoVC = (CSVideoViewController<NSURLSessionDownloadDelegate> *)[[CSVideoViewController alloc] initWithVideoInfo:self.videoInfo];
    
    [self triggerViewDidLoad:videoVC];
    
    if ([videoVC respondsToSelector:@selector(URLSession:downloadTask:didFinishDownloadingToURL:)]) {
        CSVideoViewController *otherVideoVC = [[CSVideoViewController alloc] initWithVideoInfo:self.videoInfo];
        
        [self triggerViewDidLoad:otherVideoVC];
        
        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        appDel.lastDownloadTask = nil;
        
        [otherVideoVC startDownload:nil];
        
        NSURLSessionDownloadTask *task = (NSURLSessionDownloadTask *)appDel.lastDownloadTask;
        
        if (task) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *fixturesPath = [paths[0] stringByAppendingPathComponent:@"Fixtures"];
            
            [[NSFileManager defaultManager] createDirectoryAtPath:fixturesPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            NSString *fixturePath = [fixturesPath stringByAppendingPathComponent:task.originalRequest.URL.lastPathComponent];
            
            NSString *fixtureDataString = @"testing123";
            NSData *fixtureData = [fixtureDataString dataUsingEncoding:NSUTF8StringEncoding];
            
            BOOL result = [[NSFileManager defaultManager] createFileAtPath:fixturePath contents:fixtureData attributes:nil];
            
            if (result) {
                [videoVC URLSession:[NSURLSession sharedSession] downloadTask:task didFinishDownloadingToURL:[NSURL fileURLWithPath:fixturePath]];
                
                
                NSString *videosDirPath = [paths[0] stringByAppendingPathComponent:@"Videos"];
                
                NSString *videoPath = [videosDirPath stringByAppendingPathComponent:task.originalRequest.URL.lastPathComponent];
                
                XCTAssert([[NSFileManager defaultManager] fileExistsAtPath:videoPath], @"Did not copy the temp file into the app in the URLSession:downloadTask:didFinishDownloadingToURL: delegate method on CSVideoViewController");
                
                [tester waitForTimeInterval:0.5f];
                
                XCTAssert([videoVC.downloadButton.title isEqualToString:@"Watch Download"], @"Did not call CSVideoViewController's updateViewsForFinishedDownload method in the download task completion delegate method. Make sure to call it in the main thread too.");
            }else{
                NSLog(@"Well, for some reason we couldn't create the fixture file.");
            }
        }
    }else{
        XCTFail(@"Did not implement the URLSession:downloadTask:didFinishDownloadingToURL: method in the CSVideoViewController");
    }
}


- (void)triggerViewDidLoad:(UIViewController *)vc
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-value"
    vc.view;
#pragma clang diagnostic pop
}

- (NSString *)taskStateDescription:(NSURLSessionTask *)task
{
    switch (task.state) {
        case NSURLSessionTaskStateRunning:
            return @"NSURLSessionTaskStateRunning";
            break;
        case NSURLSessionTaskStateSuspended:
            return @"NSURLSessionTaskStateSuspended";
            break;
        case NSURLSessionTaskStateCanceling:
            return @"NSURLSessionTaskStateCanceling";
            break;
        case NSURLSessionTaskStateCompleted:
            return @"NSURLSessionTaskStateCompleted";
            break;
    }
}

@end
