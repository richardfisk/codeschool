//
//  NSArray+Additions.h
//  Level8-Downloading
//
//  Created by Eric Allam on 11/26/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Additions)
- (BOOL)includesStringEqualTo:(NSString *)string;
@end
