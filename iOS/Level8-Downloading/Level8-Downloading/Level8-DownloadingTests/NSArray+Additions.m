//
//  NSArray+Additions.m
//  Level8-Downloading
//
//  Created by Eric Allam on 11/26/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "NSArray+Additions.h"

@implementation NSArray (Additions)
- (BOOL)includesStringEqualTo:(NSString *)string;
{
    NSUInteger index = [self indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        
        if ([obj isKindOfClass:[NSString class]]) {
            return [string isEqualToString:obj];
        }else{
            return NO;
        }
        
        
    }];
    
    return index != NSNotFound;
}
@end
