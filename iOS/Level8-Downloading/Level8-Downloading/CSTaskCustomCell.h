//
//  CSCustomCellWithTaskCell.h
//  Level8-Downloading
//
//  Created by Eric Allam on 11/25/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSTaskCustomCell : UITableViewCell
@property (strong, nonatomic) NSURLSessionTask *task;
@end
