#import "CSPhotoDetailVC.h"

#import "Photo.h"

#import "CSEditPhotoNote.h"

@implementation CSPhotoDetailVC

- (id)init
{
    self = [super init];
    if(self) {
        self.title = [NSString stringWithFormat:@"Photo ID %ld",(long)self.photo.photoId];
        
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                    target:self
                                                                                    action:@selector(editButtonTapped:)];
        self.navigationItem.rightBarButtonItem = editButton;
    }
    return self;
}

- (void)loadView
{
    self.scrollView = [[UIScrollView alloc] init];
    
    self.photoImageView = [[UIImageView alloc] init];
    [self.scrollView addSubview:self.photoImageView];

    self.photoNameLabel = [[UILabel alloc] init];
    self.photoNameLabel.backgroundColor = [UIColor clearColor];
    self.photoNameLabel.textAlignment = NSTextAlignmentRight;
    
    UIFontDescriptor *helvetica24 = [UIFontDescriptor fontDescriptorWithName:@"HelveticaNeue" size:24.0f];
    UIFontDescriptor *boldBase = [helvetica24 fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    self.photoNameLabel.font = [UIFont fontWithDescriptor:boldBase size:24.0f];
    
    self.photoNameLabel.textColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.photoNameLabel];
    
    self.functionBar = [[UIToolbar alloc] init];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *linkButton = [[UIBarButtonItem alloc] initWithTitle:@"Send Via Email"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(linkButtonTapped:)];
    [self.functionBar setItems:@[flexibleSpace, linkButton]];
    [self.scrollView addSubview:self.functionBar];
    
    self.notesView = [[UITextView alloc] init];
    self.notesView.accessibilityLabel = @"Photo Notes";
    self.notesView.editable = NO;
    UIFontDescriptor *helvetica22 = [UIFontDescriptor fontDescriptorWithName:@"HelveticaNeue" size:22.0f];
    self.notesView.font = [UIFont fontWithDescriptor:helvetica22 size:22.0f];
    
    [self.scrollView addSubview:self.notesView];
    
    self.view = self.scrollView;
}

- (void)viewDidLoad
{
    self.view.tintColor = [UIColor redColor];
    self.photoImageView.image = [self.photo loadImage:self.photo.filename];
    
    self.photoNameLabel.text = self.photo.name;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.notesView.text = self.photo.notes;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.photoImageView.frame = CGRectMake(0,
                                           0,
                                           CGRectGetWidth(self.scrollView.frame),
                                           200);

    CGSize photoNameLabelSize = [self.photoNameLabel.text sizeWithAttributes:@{NSFontAttributeName: self.photoNameLabel.font, UIFontDescriptorTraitsAttribute: @(UIFontDescriptorTraitBold)}];
    
    self.photoNameLabel.frame = CGRectMake(CGRectGetWidth(self.photoImageView.frame) - photoNameLabelSize.width - 20,
                                           CGRectGetMaxY(self.photoImageView.frame) - 40,
                                           photoNameLabelSize.width,
                                           photoNameLabelSize.height);
    
    self.functionBar.frame = CGRectMake(0,
                                        CGRectGetMaxY(self.photoImageView.frame),
                                        CGRectGetWidth(self.view.frame),
                                        44);
    
    self.notesView.frame = CGRectMake(10,
                                      CGRectGetMaxY(self.functionBar.frame) + 10,
                                      CGRectGetWidth(self.scrollView.frame) - 10,
                                      140);
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // TODO: use the ensureLayoutForTextContainer and usedRectForTextContainer methods to create a CGRect that will contain the exact width/height of the textContainer used to display the text
    [self.notesView.layoutManager ensureLayoutForTextContainer:self.notesView.textContainer];
    // TODO: use that CGRect to update notesView's frame height, and also consider the top and bottom textContainerInsets in the height calculation.  Don't forget to take the ceilf() of that entire calculation to make sure only a round number is used to set the notesView frame.
    CGRect textContainerRect = [self.notesView.layoutManager usedRectForTextContainer:self.notesView.textContainer];
    
    
    
    CGFloat height = ceilf(textContainerRect.size.height + self.notesView.textContainerInset.bottom + self.notesView.textContainerInset.top);
    self.notesView.frame = CGRectMake(self.notesView.frame.origin.x, self.notesView.frame.origin.y, CGRectGetWidth(self.notesView.frame), height);
    NSLog(@"%@",self.notesView);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)editButtonTapped:(id)sender
{
    CSEditPhotoNote *csEditPhotoNote = [[CSEditPhotoNote alloc] init];
    csEditPhotoNote.photo = self.photo;

    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:self.photo.name
                                                                               style:UIBarButtonItemStyleBordered
                                                                              target:nil
                                                                              action:nil]];
    [self.navigationController pushViewController:csEditPhotoNote animated:YES];
}

- (void)linkButtonTapped:(id)sender
{
    NSString *subject = self.photo.name;
    NSString *body = self.photo.notes;
    NSArray *to = [NSArray arrayWithObject:@"nowhere@example.com"];
    
    MFMailComposeViewController *composeVC = [[MFMailComposeViewController alloc] init];
    composeVC.mailComposeDelegate = self;
    [composeVC setSubject:subject];
    [composeVC setMessageBody:body isHTML:NO];
    [composeVC setToRecipients:to];
    NSData *imageData = UIImagePNGRepresentation(self.photoImageView.image);
    [composeVC addAttachmentData:imageData mimeType:@"image/png" fileName:self.photo.filename];
    
    [self presentViewController:composeVC animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
