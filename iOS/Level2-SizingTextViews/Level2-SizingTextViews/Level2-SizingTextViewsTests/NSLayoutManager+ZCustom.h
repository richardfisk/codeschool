#import <UIKit/UIKit.h>

@interface NSLayoutManager (ZCustom)

- (void)custom_ensureLayoutForTextContainer:(NSTextContainer *)container;
- (CGRect)custom_usedRectForTextContainer:(NSTextContainer *)container;

@end
