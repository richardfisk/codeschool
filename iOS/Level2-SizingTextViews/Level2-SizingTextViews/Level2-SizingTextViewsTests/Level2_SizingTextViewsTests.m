#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "CSPhotoListVC.h"
#import "CSPhotoDetailVC.h"

#import "Photo.h"

@interface Level2_SizingTextViewsTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}

@end

@implementation Level2_SizingTextViewsTests

- (void)setUp
{
    [super setUp];
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [tester tapRowInTableViewWithAccessibilityLabel:@"Photo List Table" atIndexPath:indexPath];
}

- (void)tearDown
{
    [super tearDown];
    _appDel.ensureLayoutForTextContainerCalled = nil;
    _appDel.usedRectForTextContainerCalled = nil;
    
    [tester tapViewWithAccessibilityLabel:@"Photos"];
}

- (void)testTextViewSizing
{

    [tester waitForTappableViewWithAccessibilityLabel:@"Photo Notes"];

    XCTAssert([_appDel.ensureLayoutForTextContainerCalled integerValue] == 1, @"Did not call ensureLayoutForTextContainer in viewDidLayoutSubviews");
    
    XCTAssert([_appDel.usedRectForTextContainerCalled integerValue] == 1, @"Did not call usedRectForTextContainer in viewDidLayoutSubviews");
    
    UINavigationController *navVC = (UINavigationController *)[(UITabBarController *)_appDel.window.rootViewController viewControllers][0];
    
    CSPhotoDetailVC *photoDetailVC = (CSPhotoDetailVC *)[navVC topViewController];
    
    XCTAssertEqualWithAccuracy(CGRectGetHeight(photoDetailVC.notesView.frame), 121, 0.1, @"Did not correct resize the height of the notesView.");
    
    [tester tapViewWithAccessibilityLabel:@"Photos"];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [tester tapRowInTableViewWithAccessibilityLabel:@"Photo List Table" atIndexPath:indexPath];
    [tester waitForTappableViewWithAccessibilityLabel:@"Photo Notes"];
    
    navVC = (UINavigationController *)[(UITabBarController *)_appDel.window.rootViewController viewControllers][0];
    photoDetailVC = (CSPhotoDetailVC *)[navVC topViewController];
    
    XCTAssertEqualWithAccuracy(CGRectGetHeight(photoDetailVC.notesView.frame), 43, 0.1, @"Did not correct resize the height of the notesView.");
    
}

@end
