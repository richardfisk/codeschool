//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char ensureLayoutForTextContainerCalledKey;
static char usedRectForTextContainerCalledKey;

@implementation AppDelegate (TestAdditions)

- (void) setEnsureLayoutForTextContainerCalled:(NSNumber *)ensureLayoutForTextContainerCalled
{
    objc_setAssociatedObject(self, &ensureLayoutForTextContainerCalledKey, ensureLayoutForTextContainerCalled, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)ensureLayoutForTextContainerCalled
{
    return objc_getAssociatedObject(self, &ensureLayoutForTextContainerCalledKey);
}

- (void) setUsedRectForTextContainerCalled:(NSNumber *)usedRectForTextContainerCalled
{
    objc_setAssociatedObject(self, &usedRectForTextContainerCalledKey, usedRectForTextContainerCalled, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)usedRectForTextContainerCalled
{
    return objc_getAssociatedObject(self, &usedRectForTextContainerCalledKey);
}

@end
