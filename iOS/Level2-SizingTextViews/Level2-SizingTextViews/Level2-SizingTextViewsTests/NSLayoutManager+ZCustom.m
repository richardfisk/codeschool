#import "NSLayoutManager+ZCustom.h"
#import "CSSwizzler.h"
#import "AppDelegate+TestAdditions.h"

@implementation NSLayoutManager (ZCustom)

+ (void)load
{
    [CSSwizzler swizzleClass:[NSLayoutManager class]
               replaceMethod:@selector(ensureLayoutForTextContainer:)
                  withMethod:@selector(custom_ensureLayoutForTextContainer:)];
    
    [CSSwizzler swizzleClass:[NSLayoutManager class]
               replaceMethod:@selector(usedRectForTextContainer:)
                  withMethod:@selector(custom_usedRectForTextContainer:)];
}

- (void)custom_ensureLayoutForTextContainer:(NSTextContainer *)container
{
    AppDelegate *_appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _appDel.ensureLayoutForTextContainerCalled = @1;
    [self custom_ensureLayoutForTextContainer:container];
}

- (CGRect)custom_usedRectForTextContainer:(NSTextContainer *)container
{
    AppDelegate *_appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _appDel.usedRectForTextContainerCalled = @1;
    return [self custom_usedRectForTextContainer:container];
}

@end
