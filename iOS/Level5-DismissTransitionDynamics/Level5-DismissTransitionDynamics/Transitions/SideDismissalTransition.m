#import "SideDismissalTransition.h"

@implementation SideDismissalTransition


- (NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 1.0f;
}

- (void) animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    self.transitionContext = transitionContext;
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];

    // TODO: Create a UIDynamicAnimator instance using the transition context's containerView as the reference view
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:[transitionContext containerView]];
    
    // TODO: Create an instantaneous push behavior for fromVC.view that moves up and right by a factor of 100
    UIPushBehavior *rightPush = [[UIPushBehavior alloc] initWithItems:@[fromVC.view] mode:UIPushBehaviorModeInstantaneous];
    rightPush.magnitude = 1.0f;
    rightPush.pushDirection = CGVectorMake(100, -100);
    [self.animator addBehavior:rightPush];
    
    // TODO: Create a gravity behavior for fromVC.view pulls the view down (but not to the left or right) by a factor of 3
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[fromVC.view]];
    gravityBehavior.magnitude = 3.0f;
    [self.animator addBehavior:gravityBehavior];
    
    // TODO: Implement an action block on the push behavior.  Inside that action block, check if the containerView frame intersects with the fromVC view frame.  When they no longer intersect, complete the transition.
    rightPush.action = ^{
        BOOL isFromViewOnContainer = CGRectIntersectsRect([transitionContext containerView].frame, fromVC.view.frame);
        if (!isFromViewOnContainer)
        {
            [self.animator removeAllBehaviors];
            [transitionContext completeTransition:YES];
//            NSLog(@"0");
        }
//        NSLog(@"1");
    };
    
}

@end
