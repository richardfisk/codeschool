//
//  UIPushBehavior+Swizzles.h
//  Level5-MenuHideDynamics
//
//  Created by Jon Friskics on 10/9/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPushBehavior (Swizzles)

- (instancetype)custom_initWithItems:(NSArray *)items mode:(UIPushBehaviorMode)mode;
- (void)custom_setAction:(void (^)(void))action;

@end
