//
//  AppDelegate+TestAdditions.m
//  Level3-Answer-CreateAnimationController
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/objc-runtime.h>

static char presentedKey;
static char presentingViewControllerKey;
static char animatedKey;
static char beforeTransitionKey;
static char animatorInitCalledKey;
static char referenceFrameKey;
static char shouldIAddBehaviorsKey;
static char snapItemKey;
static char pushItem1Key;
static char pushItem2Key;
static char actionMethodSetKey;

@implementation AppDelegate (TestAdditions)

- (void) setBeforeTransition:(void (^)(void))beforeTransition
{
    objc_setAssociatedObject(self, &beforeTransitionKey, beforeTransition, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(void)) beforeTransition
{
    return objc_getAssociatedObject(self, &beforeTransitionKey);
}

- (void) setAnimated:(NSNumber *)animated
{
    objc_setAssociatedObject(self, &animatedKey, animated, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)animated
{
    return objc_getAssociatedObject(self, &animatedKey);
}

- (void) setPresentedViewController:(UIViewController *)presentedViewController
{
    objc_setAssociatedObject(self, &presentedKey, presentedViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentedViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentedKey);
}

- (void) setPresentingViewController:(UIViewController *)presentingViewController
{
    objc_setAssociatedObject(self, &presentingViewControllerKey, presentingViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIViewController *)presentingViewController
{
    return (UIViewController *)objc_getAssociatedObject(self, &presentingViewControllerKey);
}

- (void) setAnimatorInitCalled:(NSNumber *)animatorInitCalled
{
    objc_setAssociatedObject(self, &animatorInitCalledKey, animatorInitCalled, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSNumber *)animatorInitCalled
{
    return objc_getAssociatedObject(self, &animatorInitCalledKey);
}

- (void) setReferenceFrame:(NSString *)referenceFrame
{
    objc_setAssociatedObject(self, &referenceFrameKey, referenceFrame, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)referenceFrame
{
    return objc_getAssociatedObject(self, &referenceFrameKey);
}

- (void) setShouldIAddBehaviors:(NSNumber *)shouldIAddBehaviors
{
    objc_setAssociatedObject(self, &shouldIAddBehaviorsKey, shouldIAddBehaviors, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSNumber *)shouldIAddBehaviors
{
    return objc_getAssociatedObject(self, &shouldIAddBehaviorsKey);
}

- (void) setSnapItem:(NSArray *)snapItem
{
    objc_setAssociatedObject(self, &snapItemKey, snapItem, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSNumber *)snapItem
{
    return objc_getAssociatedObject(self, &snapItemKey);
}

- (void) setPushItem1:(NSArray *)pushItem1
{
    objc_setAssociatedObject(self, &pushItem1Key, pushItem1, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray *)pushItem1
{
    return objc_getAssociatedObject(self, &pushItem1Key);
}

- (void) setPushItem2:(NSArray *)pushItem2
{
    objc_setAssociatedObject(self, &pushItem2Key, pushItem2, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray *)pushItem2
{
    return objc_getAssociatedObject(self, &pushItem2Key);
}

- (void) setActionMethodSet:(NSNumber *)actionMethodSet
{
    objc_setAssociatedObject(self, &actionMethodSetKey, actionMethodSet, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSNumber *)actionMethodSet
{
    return objc_getAssociatedObject(self, &actionMethodSetKey);
}

@end
