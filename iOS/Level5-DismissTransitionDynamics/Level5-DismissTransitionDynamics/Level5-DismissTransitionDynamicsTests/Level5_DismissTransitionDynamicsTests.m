//
//  Level5_DismissTransitionDynamicsTests.m
//  Level5-DismissTransitionDynamicsTests
//
//  Created by Jon Friskics on 10/10/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSRecordingTestCase.h"
#import "AppDelegate+TestAdditions.h"
#import "StatsViewController.h"
#import "MockTransitionContext.h"
#import "BadgesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SideTransition.h"
#import "SideDismissalTransition.h"
#import "CSPropertyDefinition.h"

@interface Level5_DismissTransitionDynamicsTests : CSRecordingTestCase {
    AppDelegate *_appDel;
}

@end

@implementation Level5_DismissTransitionDynamicsTests

- (void)setUp
{
    [super setUp];
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _appDel.shouldIAddBehaviors = @NO;
    
    // This is how we are only running the tapViewWithAccessibilityLabel call once instead
    // of before every since test method
    //    if (!_appDel.presentedViewController) {
    //        [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    //    }
}

- (void)tearDown
{
    [super tearDown];
}
//
//- (void)testAnimatedTransition
//{
//    _appDel.shouldIAddBehaviors = @YES;
//    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
//    [tester waitForTimeInterval:1.5f];
//    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
//    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
//    [tester waitForTimeInterval:1.5f];
//
//    [tester tapViewWithAccessibilityLabel:@"dismiss"];
//
//    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:statsVC toController:badgesVC];
//
//    XCTAssert([_appDel.animatorInitCalled intValue] > 0, @"Did not create a UIDynamicAnimator instance");
//
//    SideDismissalTransition *sideDismissalTransition = [[SideDismissalTransition alloc] init];
//    [sideDismissalTransition animateTransition:context];
////    XCTAssert([sideDismissalTransition.animator.delegate class] == [sideDismissalTransition class], @"Did not set the animator delegate property");
//
//    [sideDismissalTransition.animator.behaviors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//        XCTAssert([obj isKindOfClass:[UIPushBehavior class]] || [obj isKindOfClass:[UIGravityBehavior class]], @"Did not create and add a push and gravity behavior");
//
//        if([obj isKindOfClass:[UIPushBehavior class]]) {
//            XCTAssertEqualWithAccuracy([obj pushDirection].dx, 100, 20, @"Did not set the push x direction to a number between 80 and 120");
//            XCTAssertEqualWithAccuracy([obj pushDirection].dy, -100, 20, @"Did not set the push y direction to a number between -80 and -120");
//        } else if([obj isKindOfClass:[UIGravityBehavior class]]) {
//            XCTAssertEqualWithAccuracy([obj gravityDirection].dx, 0, .1, @"Did not set the gravity x direction to zero");
//            XCTAssertEqualWithAccuracy([obj gravityDirection].dy, 3, 1, @"Did not set the gravity y direction to a number between 2 and 4");
//        }
//    }];
//}
//
- (void)testDidImplementDynamicAnimator
{
    _appDel.shouldIAddBehaviors = @YES;
    _appDel.actionMethodSet = @NO;
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    [tester waitForTimeInterval:1.5f];
    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    [tester waitForTimeInterval:1.5f];
    
    [statsVC goodJob:nil];
    [tester waitForTimeInterval:1.5f];
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:statsVC toController:badgesVC];
    id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate
                                                             animationControllerForDismissedController:statsVC];
    [transition animateTransition:context];
    
    XCTAssert([_appDel.animatorInitCalled intValue] > 0, @"Did not create a UIDynamicAnimator instance");
    
    [tester runBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
        KIFTestWaitCondition(context.completed, error, @"Did not call completeTransition:YES on the transitionContext inside the action block");
        
        return KIFTestStepResultSuccess;
    } timeout:2.0];
    
    [tester waitForTimeInterval:1.0f];
}

- (void)testPushBehaviors
{
    _appDel.shouldIAddBehaviors = @YES;
    _appDel.actionMethodSet = @NO;
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    [tester waitForTimeInterval:1.5f];
    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    [tester waitForTimeInterval:1.5f];
    
    [statsVC goodJob:nil];
    [tester waitForTimeInterval:1.5f];
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:statsVC toController:badgesVC];
    id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate
                                                             animationControllerForDismissedController:statsVC];
    [transition animateTransition:context];
    
    SideDismissalTransition *sideDismissalTransition = [[SideDismissalTransition alloc] init];
    [sideDismissalTransition animateTransition:context];
    
    __block UIPushBehavior *pushBehavior = nil;
    [sideDismissalTransition.animator.behaviors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[UIPushBehavior class]]) {
            pushBehavior = obj;
        }
    }];
    
    XCTAssertNotNil(pushBehavior, @"Did not add a UIPushBehavior");
    
    XCTAssertEqualWithAccuracy([pushBehavior pushDirection].dx, 100, 20, @"Did not set the push x direction to a number between 80 and 120");
    XCTAssertEqualWithAccuracy([pushBehavior pushDirection].dy, -100, 20, @"Did not set the push y direction to a number between -80 and -120");
    
    [tester runBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
        KIFTestWaitCondition(context.completed, error, @"Did not call completeTransition:YES on the transitionContext inside the action block");
        
        return KIFTestStepResultSuccess;
    } timeout:2.0];
    
    [tester waitForTimeInterval:1.0f];
}

- (void)testGravityBehaviors
{
    _appDel.shouldIAddBehaviors = @YES;
    _appDel.actionMethodSet = @NO;
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    [tester waitForTimeInterval:1.5f];
    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    [tester waitForTimeInterval:1.5f];
    
    [statsVC goodJob:nil];
    [tester waitForTimeInterval:1.5f];
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:statsVC toController:badgesVC];
    id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate
                                                             animationControllerForDismissedController:statsVC];
    [transition animateTransition:context];
    
    SideDismissalTransition *sideDismissalTransition = [[SideDismissalTransition alloc] init];
    [sideDismissalTransition animateTransition:context];
    
    __block UIGravityBehavior *gravityBehavior = nil;
    [sideDismissalTransition.animator.behaviors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[UIGravityBehavior class]]) {
            gravityBehavior = obj;
        }
    }];
    XCTAssertNotNil(gravityBehavior, @"Did not add a UIGravityBehavior");
    
    XCTAssertEqualWithAccuracy([gravityBehavior gravityDirection].dx, 0, .1, @"Did not set the gravity x direction to zero");
    XCTAssertEqualWithAccuracy([gravityBehavior gravityDirection].dy, 3, 1, @"Did not set the gravity y direction to a number between 2 and 4");
    
    [tester runBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
        KIFTestWaitCondition(context.completed, error, @"Did not call completeTransition:YES on the transitionContext inside the action block");
        
        return KIFTestStepResultSuccess;
    } timeout:2.0];
    
    [tester waitForTimeInterval:1.0f];
}

- (void)testTransitionCompletedInActionBlock
{
    _appDel.shouldIAddBehaviors = @YES;
    _appDel.actionMethodSet = @NO;
    BadgesViewController *badgesVC = (BadgesViewController *)[(UINavigationController *)_appDel.window.rootViewController topViewController];
    [tester waitForTimeInterval:1.5f];
    [tester tapViewWithAccessibilityLabel:@"Display Stats"];
    StatsViewController *statsVC = (StatsViewController *)_appDel.presentedViewController;
    [tester waitForTimeInterval:1.5f];
    
    [statsVC goodJob:nil];
    [tester waitForTimeInterval:1.5f];
    MockTransitionContext *context = [[MockTransitionContext alloc] initWithFromController:statsVC toController:badgesVC];
    id <UIViewControllerAnimatedTransitioning> transition = [statsVC.transitioningDelegate
                                                             animationControllerForDismissedController:statsVC];
    [transition animateTransition:context];
    XCTAssert(_appDel.actionMethodSet, @"Did not create an action method for one of the push behaviors");
    
    if(_appDel.actionMethodSet) {
        [tester runBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
            CGRect aFrame = [[[context viewControllerForKey:UITransitionContextFromViewControllerKey] view] frame];
            CGRect containerFrame = [[context containerView] frame];
            KIFTestWaitCondition(context.completed && aFrame.origin.x > CGRectGetMaxY(containerFrame) && aFrame.origin.y > CGRectGetMaxY(containerFrame), error, @"Did not call completeTransition:YES on the transitionContext inside the action block");
            
            return KIFTestStepResultSuccess;
        } timeout:2.0];
    }
    
    [tester waitForTimeInterval:1.0f];
}

- (CSPropertyDefinition *)animatorProperty
{
    CSPropertyDefinition *property;
    
    property = [CSPropertyDefinition firstPropertyWithType:@"UIDynamicAnimator" forClass:[SideDismissalTransition class]];
    
    return property;
}

@end
