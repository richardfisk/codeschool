//
//  UIPushBehavior+Swizzles.m
//  Level5-MenuHideDynamics
//
//  Created by Jon Friskics on 10/9/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "UIPushBehavior+Swizzles.h"
#import "AppDelegate+TestAdditions.h"
#import "CSSwizzler.h"

@implementation UIPushBehavior (Swizzles)

+ (void) load
{
    [CSSwizzler swizzleClass:[UIPushBehavior class]
               replaceMethod:@selector(initWithItems:mode:)
                  withMethod:@selector(custom_initWithItems:mode:)];
    [CSSwizzler swizzleClass:[UIPushBehavior class]
               replaceMethod:@selector(setAction:)
                  withMethod:@selector(custom_setAction:)];
}

- (instancetype)custom_initWithItems:(NSArray *)items mode:(UIPushBehaviorMode)mode
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(appDelegate.pushItem1.count > 0) {
        appDelegate.pushItem2 = @[items[0], @(mode)];
    } else {
        appDelegate.pushItem1 = @[items[0], @(mode)];
    }

    return [self custom_initWithItems:items mode:mode];
}

- (void)custom_setAction:(void (^)(void))action
{
    [self custom_setAction:^{
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        appDelegate.actionMethodSet = @YES;
        action();
    }];
}

@end
