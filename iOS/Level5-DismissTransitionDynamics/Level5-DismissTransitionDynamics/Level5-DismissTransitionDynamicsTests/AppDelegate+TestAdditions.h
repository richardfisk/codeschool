//
//  AppDelegate+TestAdditions.h
//  Level3-Answer-CreateAnimationController
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (TestAdditions)
@property (strong, nonatomic) UIViewController *presentedViewController;
@property (strong, nonatomic) NSNumber *animated;
@property (copy, nonatomic) void (^beforeTransition)(void);

@property (strong, nonatomic) NSNumber *animatorInitCalled;
@property (strong, nonatomic) NSString *referenceFrame;
@property (strong, nonatomic) NSNumber *shouldIAddBehaviors;
@property (strong, nonatomic) NSArray *snapItem;
@property (strong, nonatomic) NSArray *pushItem1;
@property (strong, nonatomic) NSArray *pushItem2;
@property (strong, nonatomic) UIViewController *presentingViewController;
@property (strong, nonatomic) NSNumber *actionMethodSet;

@end
