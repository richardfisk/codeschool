#import <UIKit/UIKit.h>

@interface BadgesViewController : UITableViewController <UIViewControllerTransitioningDelegate>
@property (strong, nonatomic) NSArray *badges;
- (void) displayStats:(id)sender;
@end
