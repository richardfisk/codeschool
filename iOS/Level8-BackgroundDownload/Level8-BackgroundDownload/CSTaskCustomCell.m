//
//  CSCustomCellWithTaskCell.m
//  Level8-BackgroundDownload
//
//  Created by Eric Allam on 11/25/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "CSTaskCustomCell.h"

@implementation CSTaskCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
