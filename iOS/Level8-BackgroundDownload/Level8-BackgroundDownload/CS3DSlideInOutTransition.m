#import "CS3DSlideInOutTransition.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@implementation CS3DSlideInOutTransition

- (id)init
{
    self = [super init];
    if (self) {
        self.presenting = YES;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.8f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    CATransform3D rotateBackTransform = CATransform3DIdentity;
    
    rotateBackTransform.m34 = -1.0/500;
    
    rotateBackTransform = CATransform3DRotate(rotateBackTransform, DEGREES_TO_RADIANS(15), 0, 1, 0);
    rotateBackTransform = CATransform3DTranslate(rotateBackTransform, 20, 0, -50);
    
    CATransform3D rotateFromTransform = CATransform3DIdentity;
    rotateFromTransform.m34 = -1.0/500;
    rotateFromTransform = CATransform3DRotate(rotateFromTransform, DEGREES_TO_RADIANS(15), 0, 1, 0);
    rotateFromTransform = CATransform3DTranslate(rotateFromTransform, 20, 0, -50);
    
    if (self.presenting) {
        CGRect originalFrame = fromVC.view.frame;
        fromVC.view.layer.anchorPoint = CGPointMake(0, 0.5);
        fromVC.view.frame = originalFrame;
        
        toVC.view.frame = CGRectOffset(originalFrame, 500, 0);

        [[transitionContext containerView] addSubview:toVC.view];
        
        toVC.view.layer.anchorPoint = CGPointMake(0, 0.5);
        toVC.view.frame = CGRectOffset(originalFrame, 500, 0);
        toVC.view.layer.transform = rotateFromTransform;
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext]
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
            fromVC.view.layer.transform = rotateBackTransform;
            toVC.view.frame = originalFrame;
            toVC.view.layer.transform = CATransform3DIdentity;
        } completion:^(BOOL finished) {
            fromVC.view.layer.transform = CATransform3DIdentity;
            fromVC.view.layer.anchorPoint = CGPointMake(0.5, 0.5);
            [transitionContext completeTransition:YES];
        }];
        
    }else{
        CGRect originalFrame = fromVC.view.frame;
        toVC.view.layer.transform = rotateBackTransform;
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext]
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^
        {
            toVC.view.layer.transform = CATransform3DIdentity;
            fromVC.view.frame = CGRectOffset(originalFrame, 600, 0);
            fromVC.view.layer.transform = rotateFromTransform;
            
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
    }
}

@end
