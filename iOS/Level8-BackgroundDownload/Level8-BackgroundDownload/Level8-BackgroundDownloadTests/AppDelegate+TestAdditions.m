//
//  AppDelegate+TestAdditions.m
//  Level3-AnimatedTransition
//
//  Created by Eric Allam on 9/14/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "AppDelegate+TestAdditions.h"
#import <objc/runtime.h>

static char lastDownloadTaskKey;
static char notificationObserversKey;
static char initializedSharedDownloaderKey;

@implementation AppDelegate (TestAdditions)

- (void)setInitializedSharedDownloader:(NSNumber *)initializedSharedDownloader
{
    objc_setAssociatedObject(self, &initializedSharedDownloaderKey, initializedSharedDownloader, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)initializedSharedDownloader
{
    return objc_getAssociatedObject(self, &initializedSharedDownloaderKey);
}

- (void)setNotificationObservers:(NSDictionary *)notificationObservers
{
    objc_setAssociatedObject(self, &notificationObserversKey, notificationObservers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSDictionary *)notificationObservers
{
    return objc_getAssociatedObject(self, &notificationObserversKey);
}

- (void)setLastDownloadTask:(NSURLSessionTask *)lastDownloadTask
{
    objc_setAssociatedObject(self, &lastDownloadTaskKey, lastDownloadTask, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSURLSessionTask *)lastDownloadTask
{
    return objc_getAssociatedObject(self, &lastDownloadTaskKey);
}

@end
