//
//  CSVideoDownloader+ZAdditions.m
//  Level8-BackgroundDownload
//
//  Created by Eric Allam on 28/11/2013.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "CSVideoDownloader+ZAdditions.h"
#import "AppDelegate+TestAdditions.h"
#import "CSSwizzler.h"

@implementation CSVideoDownloader (ZAdditions)

+ (void)load
{
    [CSSwizzler swizzleClass:self
          replaceClassMethod:@selector(sharedDownloader)
                  withMethod:@selector(custom_sharedDownloader)];
}

+ (instancetype) custom_sharedDownloader;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.initializedSharedDownloader = @(YES);
    
    return [self custom_sharedDownloader];
}

@end
