//
//  CSVideoDownloader+ZAdditions.h
//  Level8-BackgroundDownload
//
//  Created by Eric Allam on 28/11/2013.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "CSVideoDownloader.h"

@interface CSVideoDownloader (ZAdditions)
+ (instancetype) custom_sharedDownloader;
@end
