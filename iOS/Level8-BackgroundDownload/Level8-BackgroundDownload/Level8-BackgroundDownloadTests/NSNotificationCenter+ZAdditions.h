//
//  NSNotificationCenter+ZAdditions.h
//  Level8-BackgroundDownload
//
//  Created by Eric Allam on 28/11/2013.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNotificationCenter (ZAdditions)
- (void)custom_addObserver:(id)observer selector:(SEL)aSelector name:(NSString *)aName object:(id)anObject;
@end
