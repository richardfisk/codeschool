//
//  NSNotificationCenter+ZAdditions.m
//  Level8-BackgroundDownload
//
//  Created by Eric Allam on 28/11/2013.
//  Copyright (c) 2013 Code School. All rights reserved.
//

#import "NSNotificationCenter+ZAdditions.h"
#import "AppDelegate+TestAdditions.h"
#import "CSSwizzler.h"

@implementation NSNotificationCenter (ZAdditions)
+ (void)load
{
    [CSSwizzler swizzleClass:self
               replaceMethod:@selector(addObserver:selector:name:object:)
                  withMethod:@selector(custom_addObserver:selector:name:object:)];
}

- (void)custom_addObserver:(id)observer selector:(SEL)aSelector name:(NSString *)aName object:(id)anObject;
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableDictionary *mutableObservers = [NSMutableDictionary dictionaryWithDictionary:appDel.notificationObservers];
    
    mutableObservers[aName] = NSStringFromSelector(aSelector);
    
    appDel.notificationObservers = [NSDictionary dictionaryWithDictionary:mutableObservers];
    
    [self custom_addObserver:observer selector:aSelector name:aName object:anObject];
}
@end
