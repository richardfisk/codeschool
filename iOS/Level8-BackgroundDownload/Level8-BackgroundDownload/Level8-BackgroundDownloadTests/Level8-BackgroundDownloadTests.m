//
//  Level8-BackgroundDownloadTests.m
//  Level8-BackgroundDownloadTests
//
//  Created by Eric Allam on 10/22/13.
//  Copyright (c) 2013 Code School. All rights reserved.
//

// Test imports
#import "CSRecordingTestCase.h"
#import "CSPropertyDefinition.h"
#import "AppDelegate+TestAdditions.h"

// App imports
#import "CSVideoViewController.h"
#import "CSVideoDownloader.h"

@interface Level8_BackgroundDownloadingTests : CSRecordingTestCase
@property (strong, nonatomic) NSDictionary *videoInfo;
@end

@implementation Level8_BackgroundDownloadingTests

- (void)setUp
{
    [super setUp];
    
    self.videoInfo = @{
        @"badge_url": @"https://d1ffx7ull4987f.cloudfront.net/images/achievements/large_badge/307/level-1-on-core-ios-7-45046cadb933cc707038d10dccb28110.png",
        @"course": @"Core iOS 7",
        @"description": @"Learn the most common problems you'll run into when upgrading your app for iOS 7.",
        @"download_url": @"http://hls.codeschool.com.s3.amazonaws.com/2a201cc5249eef3dda4f813d678de123_1.jpg",
        @"duration": @"4:31",
        @"highres_thumbnail_url": @"http://hls.codeschool.com.s3.amazonaws.com/2a201cc5249eef3dda4f813d678de123_1.jpg",
        @"id": @4,
        @"more_url": @"https://www.codeschool.com/courses/core-ios-7",
        @"streaming_url": @"http://hls.codeschool.com.s3.amazonaws.com/c27342f3979688fadbc7663f90035390.m3u8",
        @"thumbnail_url": @"http://hls.codeschool.com.s3.amazonaws.com/2e11422f3cd52020414ba002463cc801_1.jpg",
        @"title": @"Updating from iOS 6",
    };
}

- (void)tearDown
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.notificationObservers = [NSDictionary dictionary];
}

#pragma mark - Testing dispatch_once

-(void)testSharedDownloader
{
    XCTAssertEqualObjects([CSVideoDownloader sharedDownloader], [CSVideoDownloader sharedDownloader], @"Did not use dispatch_once to make sure that sharedDownloader always returns the same CSVideoDownloader instance");
}

#pragma mark - Testing changes to CSVideoViewController

-(void)testStartDownload
{
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:self.videoInfo];
    
    [self triggerViewDidLoad:videoVC];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.lastDownloadTask = nil;
    
    [videoVC startDownload:nil];
    
    XCTAssert(appDel.lastDownloadTask, @"Did not create a download task in [CSVideoViewController startDownload:]");
    
    [tester waitForTimeInterval:0.01f];
    
    XCTAssert(appDel.lastDownloadTask.state == NSURLSessionTaskStateRunning, @"Did not start the download by sending the download task the 'resume' message");
    
    XCTAssert([appDel.lastDownloadTask isKindOfClass:NSClassFromString(@"__NSCFBackgroundDownloadTask")], @"The download task created in startDownload: is not a background download. Make sure to use the [CSVideoDownloader sharedDownloader] to create the task");
    
    [[NSNotificationCenter defaultCenter] removeObserver:videoVC];
}


-(void)testProgressNotification
{
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:self.videoInfo];
    
    [self triggerViewDidLoad:videoVC];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.lastDownloadTask = nil;
    
    [videoVC startDownload:nil];
    
    XCTAssert([appDel.notificationObservers[CSVideoProgressNotification] isEqualToString:NSStringFromSelector(sel_registerName("updateProgress:"))], @"Did not add an observer for the CSVideoProgressNotification notification in the startDownload method to call the updateProgress: method");
    
    if (appDel.lastDownloadTask) {
        // Make sure the notifications aren't fired by CSVideoDownloader
        [appDel.lastDownloadTask suspend];
        
        // Test for the progress notification working
        NSDictionary *userInfo = @{CSVideoProgressUserInfoKey: @(0.5f)};
        
        videoVC.progressView.progress = 0.0f;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CSVideoProgressNotification
                                                            object:appDel.lastDownloadTask
                                                          userInfo:userInfo];
        [tester waitForTimeInterval:0.1f];
        
        XCTAssertEqualWithAccuracy(videoVC.progressView.progress, 0.50f, 0.01, @"The posting of the CSVideoProgressNotification did not update the CSVideoViewController's progress view. Make sure you setup a notification observer in startDownload: that calls the updateProgress: method.");
    }else{
        XCTFail(@"Did not create a download task in startDownload:, so progress is kind of a moot point.");
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:videoVC];
}

-(void)testCompletionNotification
{
    CSVideoViewController *videoVC = [[CSVideoViewController alloc] initWithVideoInfo:self.videoInfo];
    
    [self triggerViewDidLoad:videoVC];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDel.lastDownloadTask = nil;
    
    [videoVC startDownload:nil];
    
    XCTAssert([appDel.notificationObservers[CSVideoCompletionNotification] isEqualToString:NSStringFromSelector(sel_registerName("finishDownload:"))], @"Did not add an observer for the CSVideoCompletionNotification notification in the startDownload method to call the finishDownload: method");
    
    if (appDel.lastDownloadTask) {
        // Make sure the notifications aren't fired by CSVideoDownloader
        [appDel.lastDownloadTask suspend];
        
        NSDictionary *userInfo = @{CSVideoURLUserInfoKey: self.videoInfo[@"download_url"]};
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CSVideoCompletionNotification object:appDel.lastDownloadTask userInfo:userInfo];
        
        [tester waitForTimeInterval:0.1f];
        
        XCTAssert([videoVC.downloadButton.title isEqualToString:@"Watch Download"], @"Did not call the updateViewsForFinishedDownload method when the CSVideoCompletionNotification is posted from the CSVideoDownloader.");
    }else{
        XCTFail(@"Did not create a download task in startDownload:, so the completion notification is kind of a moot point.");
    }
}

#pragma mark - Testing App Delegate

-(void)testImplementBackgroundAppDelegateMethod
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([appDel respondsToSelector:@selector(application:handleEventsForBackgroundURLSession:completionHandler:)]) {
        
        appDel.initializedSharedDownloader = @(NO);
        
        [appDel application:[UIApplication sharedApplication] handleEventsForBackgroundURLSession:@"com.CodeSchool.VideoDownloader" completionHandler:^{
            
            
        }];
        
        if (appDel.backgroundCompletionHandler) {
            // Test to make sure we've reconnected the session in CSVideoDownloader
            
            XCTAssertEqualObjects(appDel.initializedSharedDownloader, @(YES), @"Did not reconnect to the background daemon by called [CSVideoDownloader sharedDownloader]");
        }else{
            XCTFail(@"Did not set the AppDelegate's backgroundCompletionHandler to the completion handler block passed in.");
        }
        
        
        
    }else{
        XCTFail(@"Did not implement the application:handleEventsForBackgroundURLSession:completionHandler: method in the AppDelegate");
    }
}

#pragma mark - Testing VideoDownloader

- (void)testImplementedFinishEventsMethodAndCallCompletionHandler
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    __block BOOL calledCompletionHandler = NO;
    
    appDel.backgroundCompletionHandler = ^{
        calledCompletionHandler = YES;
    };
    
    CSVideoDownloader *downloader = [CSVideoDownloader sharedDownloader];
    NSLog(@"%@", downloader);
    if ([downloader respondsToSelector:@selector(URLSessionDidFinishEventsForBackgroundURLSession:)]) {

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
        NSURLSession *session = [downloader performSelector:@selector(session)];
        
        [downloader performSelector:@selector(URLSessionDidFinishEventsForBackgroundURLSession:) withObject:session];
#pragma clang diagnostic pop
        
        XCTAssert(calledCompletionHandler, @"The URLSessionDidFinishEventsForBackgroundURLSession: method does not call the completion handler attached to the AppDelegate");
        
        XCTAssert(!appDel.backgroundCompletionHandler, @"In the URLSessionDidFinishEventsForBackgroundURLSession:, after calling the background completion handler, you should set it to nil");
        
    }else{
        XCTFail(@"Did not implement the URLSessionDidFinishEventsForBackgroundURLSession: method in the CSVideoDownloader");
    }
}


- (void)triggerViewDidLoad:(UIViewController *)vc
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-value"
    vc.view;
#pragma clang diagnostic pop
}

- (NSString *)taskStateDescription:(NSURLSessionTask *)task
{
    switch (task.state) {
        case NSURLSessionTaskStateRunning:
            return @"NSURLSessionTaskStateRunning";
            break;
        case NSURLSessionTaskStateSuspended:
            return @"NSURLSessionTaskStateSuspended";
            break;
        case NSURLSessionTaskStateCanceling:
            return @"NSURLSessionTaskStateCanceling";
            break;
        case NSURLSessionTaskStateCompleted:
            return @"NSURLSessionTaskStateCompleted";
            break;
    }
}

@end
