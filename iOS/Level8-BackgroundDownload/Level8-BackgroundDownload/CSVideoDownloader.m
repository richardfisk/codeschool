#import "CSVideoDownloader.h"
#import "AppDelegate.h"

@interface CSVideoDownloader () <NSURLSessionDelegate, NSURLSessionDownloadDelegate>
@property (strong, nonatomic) NSURLSession *session;
@end

NSString *const CSVideoProgressNotification = @"com.CodeSchool.Video.ProgressNotification";
NSString *const CSVideoCompletionNotification = @"com.CodeSchool.Video.CompletionNotification";
NSString *const CSVideoProgressUserInfoKey = @"com.CodeSchool.Video.ProgressKey";
NSString *const CSVideoURLUserInfoKey = @"com.CodeSchool.Video.URLKey";

@implementation CSVideoDownloader

+ (instancetype) sharedDownloader;
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfiguration:@"com.CodeSchool.VideoDownloader"];

        self.session = [NSURLSession sessionWithConfiguration:config
                                                     delegate:self
                                                delegateQueue:nil];
    }
    return self;
}

#pragma mark - Public interface

- (NSURLSessionDownloadTask *)downloadTaskWithURL:(NSURL *)URL;
{
    return [self.session downloadTaskWithURL:URL];
}

- (NSString *)downloadedVideoPathForURL:(NSURL *)url
{
    NSString *filePath = [self buildDownloadPath:url];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        return filePath;
    }else{
        return nil;
    }
}

#pragma mark - NSURLSessionDownloadDelegate methods

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    CGFloat progress = (CGFloat)totalBytesWritten / totalBytesExpectedToWrite;
    
    NSDictionary *userInfo = @{CSVideoProgressUserInfoKey: @(progress)};
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CSVideoProgressNotification
                                                        object:downloadTask
                                                      userInfo:userInfo];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSString *filePath = [self buildDownloadPath:downloadTask.originalRequest.URL];
    
    [[NSFileManager defaultManager] copyItemAtPath:[location path] toPath:filePath error:nil];
    
    NSDictionary *userInfo = @{CSVideoURLUserInfoKey: [downloadTask.originalRequest.URL absoluteString]};
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CSVideoCompletionNotification
                                                        object:downloadTask
                                                      userInfo:userInfo];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    // Don't do anything here
}

#pragma mark - Background delegate method

// TODO: Implement the NSURLSession delegate method that is called when all background events are finished
//       and call the background completion handler
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(appDelegate.backgroundCompletionHandler)
    {
        appDelegate.backgroundCompletionHandler();
        appDelegate.backgroundCompletionHandler = nil;
    }
}

#pragma mark - Helper methods

- (NSString *)buildDownloadPath:(NSURL *)videoURL
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths[0] stringByAppendingPathComponent:@"Videos"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:docsPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSString *newFileName = videoURL.lastPathComponent;
    
    return [docsPath stringByAppendingPathComponent:newFileName];
}
@end
