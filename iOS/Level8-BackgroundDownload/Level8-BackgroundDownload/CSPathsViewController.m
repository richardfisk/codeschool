#import "CSPathsViewController.h"
#import "CSVideosViewController.h"
#import "CSTaskCustomCell.h"

@interface CSPathsViewController ()
@property (strong, nonatomic) NSArray *paths;
@end

static NSString * const CSPathsURL = @"http://codeschoolmockapi.herokuapp.com/api/app/v1/paths";
static NSString * const CSPathCellIdentifier = @"com.CodeSchool.PathCell";

@implementation CSPathsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.view.accessibilityLabel = @"Code School Paths";
        
        self.title = @"Code School";
        
        [self.tableView registerClass:[CSTaskCustomCell class] forCellReuseIdentifier:CSPathCellIdentifier];
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:config];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *pathsURL = [NSURL URLWithString:CSPathsURL];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *task = [session dataTaskWithURL:pathsURL
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        
        self.paths = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.tableView reloadData];
        }];
    }];
    
    [task resume];
    
    [session finishTasksAndInvalidate];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.paths.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSTaskCustomCell *cell = [self setupCellForIndexPath:indexPath];
    
    NSDictionary *path = self.paths[indexPath.row];
    
    cell.textLabel.text = path[@"title"];
    cell.imageView.image = [UIImage imageNamed:@"BadgePlaceholder"];
    
    NSURL *badgeURL = [NSURL URLWithString:path[@"badge_url"]];
    
    if (cell.task) {
        [cell.task cancel];
    }
    
    cell.task = [self.session dataTaskWithURL:badgeURL
                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        if (!error) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                cell.imageView.image = [UIImage imageWithData:data];
            }];
        }

    }];

    [cell.task resume];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *path = self.paths[indexPath.row];
    
    CSVideosViewController *videosVC = [[CSVideosViewController alloc] initWithPathID:path[@"id"]];
    
    [self.navigationController pushViewController:videosVC animated:YES];
}

- (CSTaskCustomCell *)setupCellForIndexPath:(NSIndexPath *)indexPath
{
    CSTaskCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CSPathCellIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
