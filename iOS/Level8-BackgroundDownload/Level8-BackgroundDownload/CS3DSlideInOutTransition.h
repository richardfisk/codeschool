#import <Foundation/Foundation.h>

@interface CS3DSlideInOutTransition : NSObject <UIViewControllerAnimatedTransitioning>

@property (assign, nonatomic) BOOL presenting;

@end
