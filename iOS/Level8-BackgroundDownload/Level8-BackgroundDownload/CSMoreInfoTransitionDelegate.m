#import "CSMoreInfoTransitionDelegate.h"
#import "CS3DSlideInOutTransition.h"

@interface CSMoreInfoTransitionDelegate ()

@property (strong, nonatomic) CS3DSlideInOutTransition *animatedTransition;

@end

@implementation CSMoreInfoTransitionDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    if (self.animatedTransition) {
        self.animatedTransition.presenting = YES;
    }else{
        self.animatedTransition = [CS3DSlideInOutTransition new];
    }
    
    return self.animatedTransition;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    self.animatedTransition.presenting = NO;
    
    return self.animatedTransition;
}

@end
